# Run the examples #
1. Ensure you have JDK 1.7 and Maven installed

2. Go to `lib` folder and run `make`.

3. Go back to the base folder, and ensure `mvn clean compile` works without errors.

4. The main method you will be running is edu.washington.cs.knowitall.main.Summa.java, run it with -Xmx 2G

5. At the moment it's set up to read in an example of Pope Benedict's resignation and Pope Francis' election, so you'll need to modify it to read in your own data. This should simply mean changing the file locations that it reads in.

6. Because parsing etc takes so long, we have the program set up to read in the parse trees and the extractions. So you'll need to create files that look like data/summaExamples/
Each cluster is its own directory, and each cluster contains three directories ("original", "extractions", "corenlp"). "original" contains the original documents -> your documents should have the same format as the documents in "original". "extractions" contains the Ollie extractions, and "corenlp" contains the files parsed by Stanford CoreNLP system.

7. You can create the CoreNLP files by running 
`java -cp "*" -Xmx3g edu.stanford.nlp.pipeline.StanfordCoreNLP -props annotators.txt -filelist <FILE WITH LIST OF FILENAMES> -outputDirectory <OUTDIRECTORY>`

annotators.txt looks like:
annotators = tokenize, cleanxml, ssplit, pos, lemma, ner, parse, dcoref
ssplit.newlineIsSentenceBreak = two

# Running from Scratch #
1. Get your data into the nlp_serde format
2. (Optional) Filter out irrelevant documents, e.g. `mvn scala:run -DmainClass=nlp_serde.apps.d2d.FilterDocuments -DaddArgs="data/d2d/docs.nlp.flr.json.gz|chibok|2014-04|2014-05|2014-06"`
3. Run `SummaFormatWriter` for generating `original` and `corenlp` extractions, e.g. `mvn scala:run -DmainClass=nlp_serde.apps.d2d.SummaFormatWriter -DaddArgs="kidnap"`
4. Run Ollie to generate `extractions`, e.g. `MAVEN_OPTS="-Xmx4g"; mvn clean compile exec:java -Dexec.mainClass="edu.washington.cs.knowitall.utilities.OllieWrapper" -Dexec.args="data/d2d/summa/kidnap/original data/d2d/summa/kidnap/extractions"`
5. Run Summa: `MAVEN_OPTS="-Xmx4g"; mvn clean compile exec:java -Dexec.mainClass="edu.washington.cs.knowitall.main.Summa"`, which produces a summa.txt in the base directory.
6. (Optional) To generate the HTML file, run: `org.sameersingh.ervisualizer.data.SummaTextToHTML.main(Array("kidnap"))`