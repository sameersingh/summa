package edu.washington.cs.knowitall.utilities;

import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.trees.Tree;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.summarization.Parameters;

public class Compressor {
	
	private static boolean containsTimeStamp(Tree tree) {
		for (Tree leaf : tree.getLeaves()) {
			if (Parameters.TIMES.contains(leaf.label().value())) {
				boolean okay = true;
				for (Tree child : tree.children()) {
					if (child.label().value().equals("SBAR")) {
						okay = false;
						break;
					}
				}
				if (okay) {
					return true;
				}
			}
		}
		return false;
	}
	
	private static boolean validWhichClause(List<Tree> children, int i) {
		String label = children.get(i).label().value();
		String nextLabel = children.get(i).children()[0].label().value();
		String firstWord = children.get(i).getLeaves().get(0).label().value();
		String secondLabel = ",";
		if (children.size() > i+1) 
			secondLabel = children.get(i+1).label().value();
		if (label.equals("SBAR") && nextLabel.equals("WHNP") && 
				(secondLabel.equals(",") || secondLabel.equals(".")) &&
				!firstWord.equals("that") && !firstWord.equals("what") && 
				!firstWord.equals("whatever") && !firstWord.equals("whoever") && 
				!firstWord.equals("whom")) {
			return true;
		}
		return false;
	}
	
	private static boolean validAdverbClause(List<Tree> children, int i) {
		String label = children.get(i).label().value();
		String secondLabel = ",";
		if (children.size() > i+1) {
			secondLabel = children.get(i+1).label().value();
		}
		if (label.equals("ADVP") &&
				(secondLabel.equals(",") || secondLabel.equals("."))) {
			return true;
		}
		return false;
	}
	
	private static int leafLoc(Tree root, Tree leaf) {
		List<Tree> rootLeaves = root.getLeaves();
		int leafLoc = -1;
		for (int j = 0; j < rootLeaves.size(); j++) {
			if (rootLeaves.get(j) == leaf) {
				leafLoc = j;
				return leafLoc;
			}
		}
		return leafLoc;
	}
	
	private static boolean validAppositiveClause(Tree root, List<Tree> children, int i) {
		String label = children.get(i).label().value();
		List<Tree> leaves = children.get(i).getLeaves();
		List<Tree> rootLeaves = root.getLeaves();
		int leafLoc = leafLoc(root, leaves.get(leaves.size()-1));

		if (rootLeaves.size() > leafLoc+1) {
			String secondLabel = rootLeaves.get(leafLoc+1).label().value();
			if (label.equals("NP") && (secondLabel.equals(","))) {// || secondLabel.equals("."))) {
				if ((rootLeaves.size() <= leafLoc+2 || (!rootLeaves.get(leafLoc+2).label().value().equals(",") && !rootLeaves.get(leafLoc+2).label().value().equals("CC")))
					&& (rootLeaves.size() <= leafLoc+3 || (!rootLeaves.get(leafLoc+3).label().value().equals(",") && !rootLeaves.get(leafLoc+3).label().value().equals("CC")))) {
					return true;
				}
			} else if (rootLeaves.size() > leafLoc+2) {
				String thirdLabel = rootLeaves.get(leafLoc+2).label().value();
				if (label.equals("RB") && secondLabel.equals("NP") && thirdLabel.equals(",")) {
					if ((rootLeaves.size() <= leafLoc+3 || (!rootLeaves.get(leafLoc+3).label().value().equals(",") && !rootLeaves.get(leafLoc+3).label().value().equals("CC")))
							&& (rootLeaves.size() <= leafLoc+4 || (!rootLeaves.get(leafLoc+4).label().value().equals(",") && !rootLeaves.get(leafLoc+4).label().value().equals("CC")))) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private static String getLeafPhrase(Tree tree) {
		String phrase = "";
		for (Tree leaf : tree.getLeaves()) {
			phrase += leaf.label().value()+" ";
		}
		return phrase.trim();
	}
	
	private static boolean validSaidClause(List<Tree> children, int i) {
		if (children.size() < i+2) {
			return false;
		}
		String label = children.get(i).label().value();
		String leafPhrase = getLeafPhrase(children.get(i));
		String nextLabel = children.get(i+1).label().value();
		String nextLeafPhrase = getLeafPhrase(children.get(i+1));
		if (label.equals("S")) {
			if ((leafPhrase.equals("officials say") || leafPhrase.equals("officials said")) && 
					(nextLabel.equals(",") || nextLabel.equals("."))) {
				return true;
			}
		}
		if (children.size() < i+3) {
			return false;
		}
		String thirdLabel = children.get(i+2).label().value();
		String thirdLeafPhrase = getLeafPhrase(children.get(i+2));
		if (label.equals(",") && nextLabel.equals("NP") && thirdLabel.equals("VP") && nextLeafPhrase.contains("officials") 
				&& thirdLeafPhrase.equals("said") && children.size()==i+3) {
			return true;
		}
		if (label.equals("NP") && nextLabel.equals("VP") && leafPhrase.contains("officials") 
				&& nextLeafPhrase.equals("said") && (thirdLabel.equals(",") || thirdLabel.equals("."))) {
			return true;
		}
		return false;
	}
	
	private static boolean validAsClause(List<Tree> children, int i) {
		String label = children.get(i).label().value();
		String firstWord = children.get(i).getLeaves().get(0).label().value().toLowerCase();
		String nextLabel = children.get(i+1).label().value();
		String nextFirstWord = children.get(i+1).getLeaves().get(0).label().value().toLowerCase();
		if (label.equals("SBAR") && (firstWord.equals("as") || firstWord.equals("though")) && nextLabel.equals(",")) {
			return true;
		}
		if (label.equals(",") && nextLabel.equals("SBAR") && (nextFirstWord.equals("as") || nextFirstWord.equals("though"))) {
			return true;
		}
		return false;
	}
	
	private static boolean validAndClause(Tree root, List<Tree> children, int i) {
		String label1 = children.get(i).label().value();
		String label2 = children.get(i+1).label().value();
		int leafLoc = leafLoc(root, children.get(i+1).getLeaves().get(children.get(i+1).getLeaves().size()-1));
		if (label1.equals("CC") && (label2.equals("S") || label2.equals("VP") && leafLoc == root.getLeaves().size()-1)) {
			return true;
		}
		if (i+2 < children.size()) {
			String label3 = children.get(i+2).label().value();
			leafLoc = leafLoc(root, children.get(i+2).getLeaves().get(children.get(i+2).getLeaves().size()-1));
			if (label1.equals(",") && label2.equals("CC") && (label3.equals("S") || label3.equals("VP") && leafLoc == root.getLeaves().size()-1)) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean validPrepClause(Tree root, List<Tree> children, int i) {
		String label1 = children.get(i).label().value();
		String label2 = children.get(i+1).label().value();
		int leafLoc = leafLoc(root, children.get(i).getLeaves().get(0));
		if (i == 0 && leafLoc == 0 && label1.equals("PP") && label2.equals(",")) {
			return true;
		}
		leafLoc = leafLoc(root, children.get(i+1).getLeaves().get(children.get(i+1).getLeaves().size()-1));
		if (label1.equals(",") && label2.equals("PP") && leafLoc == root.getLeaves().size()-1) {
			return true;
		}
		return false;
	}
	
	private static boolean validToClause(Tree root, List<Tree> children, int i) {
		String label1 = children.get(i).label().value();
		String label2 = children.get(i+1).label().value();
		if (label1.equals("VB") && label2.equals("SBAR") && children.get(i).parent(root).parent(root).getChild(0).label().value().equals("TO")) {
			return true;
		}
		return false;
	}
	
	private static ArrayList<Integer> compressTreeRecursive(Tree root, Tree tree, int level, boolean pretty) {
		ArrayList<Integer> indices = new ArrayList<Integer>();
		if (tree.isLeaf()) {
			indices.add(leafLoc(root, tree));
			return indices;
		}
		
		String firstWord = tree.getLeaves().get(0).label().value().toLowerCase();
		String label = tree.label().value();
		String nextLabel = tree.children()[0].label().value();
		if (!pretty && label.equals("S") && nextLabel.equals("VP") && firstWord.equals("to")) {
			return indices;
		}
		if (label.equals("PRN")) {
			return indices;
		}
		if (label.equals("ADVP") && nextLabel.equals("RB") && firstWord.endsWith("ly")) {
			return indices;
		}
		if (label.equals("SBAR") && (firstWord.equals("as") || (!pretty && (firstWord.equals("that") || firstWord.equals("what") ||
				firstWord.equals("except") || firstWord.equals("after") || firstWord.equals("where") || firstWord.equals("who") ||
				firstWord.equals("because") || firstWord.equals("even") || firstWord.equals("before") || firstWord.equals("when") ||
				firstWord.equals("how"))))) {
			return indices;
		}
		if (pretty && label.equals("PP") && nextLabel.equals("VBG")) {
			return indices;
		}
		if (!pretty && label.equals("PP") && firstWord.equals("because")) {
			return indices;
		}
		// on Thursday
		if (label.equals("PP") && nextLabel.equals("IN") && tree.children().length == 2 && 
				Parameters.TIMES.contains(tree.children()[1].getLeaves().get(0).label().value())) {
			return indices;
		}
		if (label.equals("PP") && tree.children().length > 1 &&
				tree.children()[1].label().value().equals("NP-TMP") && containsTimeStamp(tree)) {
			return indices;
		}	
		if (label.equals("NP-TMP") && containsTimeStamp(tree)) {
			return indices;
		}

		if (label.equals("NP") && tree.children().length > 1 && tree.children()[1].label().value().equals("VP") && tree.children()[1].children()[0].label().value().equals("VBN")) {
			indices.add(0);
			return indices;
		}
		List<Tree> children = tree.getChildrenAsList();
		ArrayList<Integer> skipList = new ArrayList<Integer>();
		//if (children.size() > 2) {
			for (int i = 0; i < children.size()-1; i++) {
				String labelChild = children.get(i).label().value();
				String nextLabelChild = children.get(i+1).label().value();
				if (validSaidClause(children, i+1)) {
					skipList.add(i);
					skipList.add(i+1);
					if (i+2 < children.size()) { 
						String nextLeafPhrase = getLeafPhrase(children.get(i+2));
						if (nextLeafPhrase.equals("said") || nextLeafPhrase.equals(",")) {
							skipList.add(i+2);
							if (i+3 < children.size() && children.get(i+3).label().value().equals(",")) {
								skipList.add(i+3);
							}
						} 
					}
				} else if (validToClause(root, children, i) && !pretty) {
					skipList.add(i+1);
				} else if (validPrepClause(root, children, i)) {
					skipList.add(i);
					skipList.add(i+1);
				} else if (validAsClause(children, i)) {
					skipList.add(i);
					skipList.add(i+1);
					if (i+2 < children.size() && children.get(i+2).label().value().equals(",")) {
						skipList.add(i+2);
					}
				} else if (validAndClause(tree, children, i)) {
					skipList.add(i);
					skipList.add(i+1);
					if (i+2 < children.size() && children.get(i).label().value().equals(",")) {
						skipList.add(i+2);
					}
				} else if (!(i == 1 && (children.get(i-1).label().value().equals("PP") ||
						children.get(i-1).label().value().equals("ADVP"))) &&
							labelChild.equals(",") &&
							(validWhichClause(children, i+1) ||
							validAdverbClause(children, i+1) ||
							(validAppositiveClause(root, children, i+1)) ||
							nextLabelChild.equals("S") ||
							//children.get(i+1).label().value().equals("CC") ||
							nextLabelChild.equals("PP"))) {
					skipList.add(i);
					skipList.add(i+1);
					if (i+2 < children.size() && children.get(i+2).label().value().equals(",")) {
						skipList.add(i+2);
					}
				} 
			}
		//} 
		for (int i = 0; i < children.size(); i++) {
			if (!skipList.contains(i)) {
				Tree child = children.get(i);
				ArrayList<Integer> tempIndices = compressTreeRecursive(root, child, level+1, pretty);
				indices.addAll(tempIndices);
			}
		}

		return indices;
	}
	
	public static ArrayList<Integer> compressIndices(Sentence sentence, boolean pretty) {
		Tree root = sentence.getParseTree();
		ArrayList<Integer> compressedIndices = compressTreeRecursive(root, root, 0, pretty);
		return compressedIndices;
	}
	
	public static boolean withinClause(Sentence sentence, int index, boolean debug) {
		Tree root = sentence.getParseTree();
		Tree leaf = root.getLeaves().get(index);
		List<Tree> path = root.dominationPath(leaf);
		for (Tree tree : path) {
			String leafWord = tree.getLeaves().get(0).label().value();
			if (leafWord.equals("who") || leafWord.equals("after") || leafWord.equals("for") || leafWord.equals("of") || leafWord.equals("that") || leafWord.equals("which")) {
				return true;
			}
		}
		return false;
	}
	
}
