package edu.washington.cs.knowitall.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import edu.washington.cs.knowitall.datastructures.Sentence;

import weka.classifiers.functions.LinearRegression;
import weka.core.Instances;

public abstract class Classifier {

    protected LinearRegression classifier;
    protected String header;
    
    protected Collection<Sentence> sentences;
	protected HashMap<String, Double> labelMap;
	
	public Classifier() {
		labelMap = new HashMap<String, Double>();
	}
	
	public void setup(Collection<Sentence> sentences) {
		this.sentences = sentences;
	}
	
	protected void setupTraining(String trainingFilename) {
		// Set up the classifier from the training data.
		InputStreamReader trainingReader = null;
		classifier = new LinearRegression();
        try {
        	trainingReader = new InputStreamReader(new FileInputStream(trainingFilename));
        	Instances trainingInstances = setupInstances(trainingReader);
        	classifier.buildClassifier(trainingInstances);
        }
    	catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
    	}
		        
	}
    
	public double labelSentence(Sentence sentence) {
		String key = Sentence.getKey(sentence.getDocId(), sentence.getSentenceId());
		if (!labelMap.containsKey(key)) {
			return -1;
		}
		double label = labelMap.get(key);
		return label;
	}
	
	public double labelSentence(String key) {
		if (!labelMap.containsKey(key)) {
			return -1;
		}
		double label = labelMap.get(key);
		return label;
	}
	
	public double classifyInstance(String features) {
		StringReader testReader = new StringReader(features+"\n");
		Instances testingInstances = setupInstances(testReader);
		try {
			classifier.classifyInstance(testingInstances.firstInstance());
			return classifier.distributionForInstance(testingInstances.firstInstance())[0];
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.5;
	}
	
	/**
	 * Set up the instances from the reader
	 * @param instanceReader the source of the instances
	 * @return the instances object
	 */
	public Instances setupInstances(Reader instanceReader) {
		Instances instances = null;
		try {
			instances = new Instances(instanceReader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		instances.setClassIndex(instances.numAttributes() - 1);
		try {
			instanceReader.close();
		} catch (IOException e) {
			System.err.println("could not close reader");
			e.printStackTrace();
			System.exit(1);
		}
		return instances;
	}

	/**
	 * Scale the features so they're all 0.0-1.0.
	 * @param features
	 * @return
	 */
	public static ArrayList<ArrayList<Double>> scaleFeatures(ArrayList<ArrayList<Double>> features) {
		for (int j = 0; j < features.get(0).size(); j++) {
			double highest = 0.0;
			double lowest = 1000000;
			for (int i = 0; i < features.size()-1; i++) {
				if (features.get(i).get(j) > highest) {
					highest = features.get(i).get(j);
				}
				if (features.get(i).get(j) < lowest) {
					lowest = features.get(i).get(j);
				}
			}

			if (highest-lowest > 0) {
				for (int i = 0; i < features.size(); i++) {
					double original = features.get(i).get(j);
					double scaled = (original-lowest)/(highest-lowest);
					features.get(i).set(j, scaled);
				}
			}
		}
		
		return features;
	}
	
}
