package edu.washington.cs.knowitall.utilities;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import edu.washington.cs.knowitall.datastructures.Pair;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.summarization.Parameters;

public class Utilities {

    public static boolean copy(File src, File dest) {
    	if (src.isDirectory()) {
        	if (!dest.exists()) {
    		   dest.mkdir();
    		} else {
    			return false;
    		}
    		String files[] = src.list();
 
    		for (String file : files) {
    			//construct the src and dest file structure
    			File srcFile = new File(src, file);
    			File destFile = new File(dest, file);
    			copy(srcFile,destFile);
    		}
    		return true;
        } else {
        	if (dest.exists()) {
        		return false;
        	}
        	
			try {
				InputStream in = new FileInputStream(src);
	        	OutputStream out = new FileOutputStream(dest); 
	        	byte[] buffer = new byte[1024];
	        	 
	    	    int length;
	    	    //copy the file content in bytes 
	    	    while ((length = in.read(buffer)) > 0){
	    	  	   out.write(buffer, 0, length);
	    	    }
	 
	    	    in.close();
	    	    out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	    return true;
    	}
    }
    
    public static boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(null!=files){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());
    }
    
    public static void moveDirectory(File src, File dest) {
    	copy(src, dest);
    	deleteDirectory(src);
    }
	
	public static int getDayOfWeekIndex(String dateStr) {
		dateStr = dateStr.toLowerCase();
		if (dateStr.equals("monday") || dateStr.equals("mon") || dateStr.equals("mon.")) {
			return Calendar.MONDAY;
		} else if (dateStr.equals("tuesday") || dateStr.equals("tues") || dateStr.equals("tues.")) {
			return Calendar.TUESDAY;
		} else if (dateStr.equals("wednesday") || dateStr.equals("wed") || dateStr.equals("wed.")) {
			return Calendar.WEDNESDAY;
		} else if (dateStr.equals("thursday") || dateStr.equals("thurs") || dateStr.equals("thurs.")) {
			return Calendar.THURSDAY;
		} else if (dateStr.equals("friday") || dateStr.equals("fri") || dateStr.equals("fri.")) {
			return Calendar.FRIDAY;
		} else if (dateStr.equals("saturday") || dateStr.equals("sat") || dateStr.equals("sat.")) {
			return Calendar.SATURDAY;
		} else if (dateStr.equals("sunday") || dateStr.equals("sun") || dateStr.equals("sun.")) {
			return Calendar.SUNDAY;
		}
		return -1;
	}
	
	public static int getMonthIndex(String dateStr) {
		dateStr = dateStr.toLowerCase();
		if (dateStr.equals("january") || dateStr.matches("jan\\.?")) {
			return Calendar.JANUARY;
		} else if (dateStr.equals("february") || dateStr.matches("feb\\.?")) {
			return Calendar.FEBRUARY;
		} else if (dateStr.equals("march") || dateStr.matches("mar\\.?")) {
			return Calendar.MARCH;
		} else if (dateStr.equals("april") || dateStr.matches("apr\\.?")) {
			return Calendar.APRIL;
		} else if (dateStr.equals("may") || dateStr.matches("may\\.?")) {
			return Calendar.MAY;
		} else if (dateStr.equals("june") || dateStr.matches("jun\\.?")) {
			return Calendar.JUNE;
		} else if (dateStr.equals("july") || dateStr.matches("jul\\.?")) {
			return Calendar.JULY;
		} else if (dateStr.equals("august") || dateStr.matches("aug\\.?")) {
			return Calendar.AUGUST;
		} else if (dateStr.equals("september") || dateStr.matches("sep[t]?\\.?")) {
			return Calendar.SEPTEMBER;
		} else if (dateStr.equals("october") || dateStr.matches("oct\\.?")) {
			return Calendar.OCTOBER;
		} else if (dateStr.equals("november") || dateStr.matches("nov\\.?")) {
			return Calendar.NOVEMBER;
		} else if (dateStr.equals("december") || dateStr.matches("dec\\.?")) {
			return Calendar.DECEMBER;
		}
		return -1;
	}
	
	public static boolean isDayOfWeek(String dateStr) {
		String[] week = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
		for (String weekStr : week) {
			if (dateStr.contains(weekStr)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean matchesSpecificDate(String dateStr) {
		if (dateStr.matches("[(January),(Jan),(February),(Feb),(March),(Mar),"+
								"(April),(Apr),(May),(June),(Jun),(July),(Jul),"+
								"(August),(Aug),(September),(Sep),(Sept),(October),(Oct),"+
								"(November),(Nov),(December),(Dec)]+\\.? \\d\\d?")) {
			return true;
		}
		return false;
	}

	public static boolean matchesSpecificDateYear(String dateStr) {
		if (dateStr.matches("[(January),(Jan),(February),(Feb),(March),(Mar),"+
								"(April),(Apr),(May),(June),(Jun),(July),(Jul),"+
								"(August),(Aug),(September),(Sep),(Sept),(October),(Oct),"+
								"(November),(Nov),(December),(Dec)]+\\.? \\d+, \\d\\d+")) {
			return true;
		}
		return false;
	}
	
	public static Calendar getDate(Calendar referenceDate, String dateStr) {
		if (isDayOfWeek(dateStr)) {
			int referenceDayOfWeek = referenceDate.get(Calendar.DAY_OF_WEEK);
			int stringDayOfWeek = getDayOfWeekIndex(dateStr);
			if (stringDayOfWeek == referenceDayOfWeek) {
				return referenceDate;
			} else {
				int offset;
				if (referenceDayOfWeek > stringDayOfWeek) {
					offset = referenceDayOfWeek - stringDayOfWeek;
				} else {
					offset = referenceDayOfWeek + (7-stringDayOfWeek);
				}
				return new GregorianCalendar(referenceDate.get(Calendar.YEAR), 
						referenceDate.get(Calendar.MONTH), 
						referenceDate.get(Calendar.DAY_OF_MONTH)-offset);
			}
		} else if (matchesSpecificDate(dateStr)) {
			int month = getMonthIndex(dateStr.split(" ")[0]);
			int day = Integer.parseInt(dateStr.split(" ")[1]);
			return new GregorianCalendar(referenceDate.get(Calendar.YEAR), month, day);
		} else if (matchesSpecificDateYear(dateStr)) {
			int month = getMonthIndex(dateStr.split(" ")[0]);
			int day = Integer.parseInt(dateStr.split(" ")[1]);
			int year = Integer.parseInt(dateStr.split(", ")[1]);
			return new GregorianCalendar(year, month, day);
		} else  {
			return null;
		}
	}
	
	public static List<Sentence> sortSentencesByDate(List<Sentence> sentences) {
		Collections.sort(sentences, new Comparator<Sentence>(){
			@Override
			public int compare(Sentence sentence1, Sentence sentence2) {
				if (sentence1.getSentenceDate().getSecond().getTimeInMillis() < sentence2.getSentenceDate().getSecond().getTimeInMillis()) {
					return -1;
				} else if (sentence1.getSentenceDate().getSecond().getTimeInMillis() > sentence2.getSentenceDate().getSecond().getTimeInMillis()) {
					return 1;
				}
	            return 0;
			}
		});
		return sentences;
	}
	
	public static String[] partition(String string, String pattern) {
		String[] elements = string.split(pattern);
		System.out.println("**"+string+"***"+elements.length);
		String first = elements[0];
		String second = "";
		for (int i = 1; i < elements.length-1; i++) {
			second += elements[i]+pattern;
		}
		if (elements.length > 1) {
			second += elements[elements.length-1];
		}
		String[] partition = {first,second};
		return partition;
	}
	
	public static void copyFile( File from, File to ) {
		String content = readInFileAsOneString(from);
		writeToFile(to.getAbsolutePath(), content, false);
	}
			
	public static ArrayList<String> recursiveGetFilenames(String rootdir, boolean emptyFiles) {
		ArrayList<String> filenames = new ArrayList<String>();
		File folder = new File(rootdir);
		File[] listOfFiles = folder.listFiles(); 
		 
		for (int i = 0; i < listOfFiles.length; i++) {
			File curFile = listOfFiles[i];
			if (curFile.isFile()) {
				if (!curFile.getName().startsWith(".") && ((!emptyFiles && curFile.length() > 0) || (emptyFiles && curFile.length() == 0))) {
					filenames.add(listOfFiles[i].getAbsolutePath());
				}
			} else {
				filenames.addAll(recursiveGetFilenames(curFile.getAbsolutePath(), emptyFiles));
			}
		}
		return filenames;
	}
	
    public static void log(String string) {
    	BufferedWriter bw = null;

    	try {
    		DateFormat dateFormat1 = new SimpleDateFormat("yyyy.MM.dd");
    		DateFormat dateFormat2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    		//get current date time with Date()
    		Date date = new Date();
    		String directory = "logs";
    		if (!new File(directory).exists()) {
    			new File(directory).mkdir();
    		}
    		bw = new BufferedWriter(new FileWriter(directory+"/summarizationServer."+dateFormat1.format(date), true));
    		bw.write(dateFormat2.format(date)+"\t"+string);
    		bw.newLine();
    		bw.flush();
    	} catch (IOException ioe) {
    		ioe.printStackTrace();
    	} finally {                       // always close the file                                               
    		if (bw != null) try {
    			bw.close();
    		} catch (IOException ioe2) {
    			// just ignore it                                                                            
    		}
    	} // end try/catch/finally                                                                               
    }

	private static <E> List<List<E>> removeOne(List<E> summary, int start) {
		List<List<E>> combinations = new ArrayList<List<E>>();
		for (int i = start; i < summary.size(); i++) {
			List<E> combination = new ArrayList<E>(summary);
			combination.remove(i);
			combinations.add(combination);
		}
		return combinations;
	}
	
	public static <E> List<List<E>> getCombinations(List<E> originalList, int removeCount, int start) {
		HashSet<List<E>> combinations = new HashSet<List<E>>();
		List<List<E>> lastCombinations = new ArrayList<List<E>>();
		lastCombinations.add(originalList);
		for (int i = 0; i < removeCount; i++) {
			List<List<E>> newCombinations = new ArrayList<List<E>>();
			for (List<E> lastCombination : lastCombinations) {
				List<List<E>> tempCombinations = removeOne(lastCombination, start);
				newCombinations.addAll(tempCombinations);
				combinations.addAll(tempCombinations);
			}
			lastCombinations = newCombinations;
		}
		List<List<E>> combinationsList = new ArrayList<List<E>>(combinations);
		return combinationsList;
	}
	
	public static <E> E chooseProbabilistic( HashMap<E,Double> scoring) {
		double total = 0.0;
		for (double score : scoring.values()) {
			total += score;
		}
		for (E key : scoring.keySet()) {
			scoring.put(key, scoring.get(key)/total);
		}
		
		double choice = Math.random();
		total = 0;
		for (E key : scoring.keySet()) {
			total += scoring.get(key);
			if (total >= choice) {
				return key;
			}
		}
		return null;
	}
	
	public static String[] readInFile(File file) {
		Scanner in = null;
		try {
			in = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.err.println("Could not find file:"+file.getAbsolutePath());
			e.printStackTrace();
			System.exit(0);
		}
		ArrayList<String> lines = new ArrayList<String>();
		if (in!=null) {
			while (in.hasNextLine()) {
				String line = in.nextLine();
				lines.add(line);
			}
			in.close();
		}
		String[] linesArray = new String[lines.size()];
		linesArray = lines.toArray(linesArray);
		return linesArray;
	}

    public static String[] readInFileSafe(File file) {
        ArrayList<String> lines = new ArrayList<String>();
        BufferedReader br = null;
        try {
            InputStreamReader fr = new InputStreamReader(new FileInputStream(file), "UTF-8");
            br = new BufferedReader(fr);
            String line = null;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            System.err.println("Could not find file:"+file.getAbsolutePath());
            e.printStackTrace();
            System.exit(0);
        }
        String[] linesArray = new String[lines.size()];
        linesArray = lines.toArray(linesArray);
        return linesArray;
    }

    public static String readInFileAsOneStringSafe(File file) {
        String input = "";
        BufferedReader br = null;
        try {
            InputStreamReader fr = new InputStreamReader(new FileInputStream(file), "UTF-8");
            br = new BufferedReader(fr);
            String line = null;
            while ((line = br.readLine()) != null) {
                input += line + "\n";
            }
        } catch (IOException e) {
            System.err.println("Could not find file:"+file.getAbsolutePath());
            e.printStackTrace();
            System.exit(0);
        }
        return input;
    }

	public static String readInFileAsOneString(File file) {
		Scanner in = null;
		try {
			in = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.err.println("Could not find file:"+file.getAbsolutePath());
			e.printStackTrace();
			System.exit(0);
		}
		String input = "";
		if (in!=null) {
			while (in.hasNextLine()) {
				String line = in.nextLine();
				input += line+"\n";
			}
			in.close();
		}
		return input;
	}
	
	public static boolean arrayContainsLower(String[] lemmas, String string) {
		string = string.toLowerCase();
		for (String lemma : lemmas) {
			if (lemma.toLowerCase().equals(string)) {
				return true;
			}
		}
		return false;
	}
	
	public static <T> String join(List<T> tokens, String delimiter) {
		return join(tokens, delimiter, 0, tokens.size());
	}
	
	public static <T> String join(List<T> tokens, String delimiter, int start, int end) {
		String string = "";
		for (int i = start; i < end-1; i++) {
			string += tokens.get(i) + delimiter;
		}
		if (end > 0) {
			string += tokens.get(end-1);
		}
		return string;
	}
	
	public static String join(String[] tokens, String delimiter) {
		return join(tokens, delimiter, 0, tokens.length);
	}
	
	public static String join(String[] tokens, String delimiter, int start, int end) {
		String string = "";
		for (int i = start; i < end-1; i++) {
			string += tokens[i] + delimiter;
		}
		if (end > 0) {
			string += tokens[end-1];
		}
		return string;
	}
	
	public static Pair<Integer, Integer> locatePhrase(List<String> longTokens, String[] shortTokens) {
		String[] longTokenArray = new String[longTokens.size()];
		int i = 0;
		for (String token : longTokens) {
			longTokenArray[i] = token;
			i++;
		}
		return locatePhrase(longTokenArray, shortTokens);
	}
	
	public static Pair<Integer, Integer> locatePhrase(String[] longTokens, String[] shortTokens) {
		if (longTokens.length == 0 || shortTokens.length == 0) {
			return new Pair<Integer, Integer>(-1, -1);
		}
		int start = -1;
		int shortIndex = 0;
		int longIndex = 0;
		int shortLength = shortTokens.length;
		while (longIndex < longTokens.length && shortIndex < shortTokens.length) {
			//System.out.println(longIndex+" "+longTokens[longIndex]+" "+shortIndex+" "+shortTokens[shortIndex]);
			if (longTokens[longIndex].equals(shortTokens[shortIndex])) {
				if (shortIndex == 0) {
					start = longIndex;
				}
				shortIndex+=1;
			} else if (shortIndex < shortTokens.length-1 &&
						longTokens[longIndex].equals(shortTokens[shortIndex]+shortTokens[shortIndex+1])) {
				if (shortIndex == 0) {
					start = longIndex;
				}
				shortIndex+=2;
				shortLength = shortLength-1;
			} else if (longIndex < longTokens.length-1 && 
						shortTokens[shortIndex].equals(longTokens[longIndex]+longTokens[longIndex+1])) {
				if (shortIndex == 0) {
					start = longIndex;
				}
				longIndex+=1;
				shortIndex+=1;
				shortLength = shortLength+1;
			} else {
				if (start != -1) {
					longIndex = start;
				}
				start = -1;
				shortIndex = 0;
			}
			longIndex+=1;
		}
		if (shortIndex == shortTokens.length){ 
			if ((start+shortLength) < 1) {
				System.err.println("problem in locatePhrase:"+(start+shortLength));
				System.err.println("long:"+join(longTokens, " "));
				System.err.println("short:"+join(shortTokens, " "));
				return new Pair<Integer, Integer>(-1, -1);
			}
			return new Pair<Integer, Integer>(start, start+shortLength);
		} else {
			return new Pair<Integer, Integer>(-1, -1);
		}
	}
	
	
	public static boolean notProceededByVerb(List<String> lemmaList, List<String> posList, String string1) {
		Pair<Integer, Integer> startStop = locatePhrase(lemmaList, string1.split(" "));
		int start = startStop.getFirst();
		if (start > -1) {
			for (int i = 0; i < start; i++) {
				if (posList.get(i).startsWith("V")) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	public static int overlap(String[] tokens1, String[] tokens2) {
		int overlap = 0;
		for (String token1 : tokens1) {
			for (String token2 : tokens2) {
				if (token1.equals(token2)) {
					overlap++;
				}
			}
		}
		return overlap;
	}
	
	public static int overlap(List<String> tokens1, List<String> tokens2) {
		int overlap = 0;
		for (String token1 : tokens1) {
			for (String token2 : tokens2) {
				if (token1.equals(token2)) {
					overlap++;
				}
			}
		}
		return overlap;
	}
	
	public static boolean firstLetterCapitalized(String string) {
		return Character.isUpperCase(string.charAt(0));
	}
	
	public static boolean stringContains(List<String> lemmaArray, String string1) {
		String string2 = Utilities.join(lemmaArray, " ");
		if (stringContains(string2, string1)) {
			return true;
		}
		return false;
	}
	
	public static boolean stringContains(String string1, String string2) {	
		if (string1.equals(string2) || 
				string1.startsWith(string2+" ") || 
				string1.endsWith(" "+string2) || 
				string1.contains(" "+string2+" ")) {
			return true;
		}
		return false;
	}
	
	public static String printDate(Calendar date) {
		String dateStr = date.get(Calendar.YEAR)+"-"+(date.get(Calendar.MONTH)+1)+"-"+date.get(Calendar.DAY_OF_MONTH)
				+" "+date.get(Calendar.HOUR_OF_DAY)+":"+date.get(Calendar.MINUTE)+":"+date.get(Calendar.SECOND);
		return dateStr;
	}
	
	public static String printDateShort(Calendar date) {
		String dateStr = date.get(Calendar.YEAR)+"-"+(date.get(Calendar.MONTH)+1)+"-"+date.get(Calendar.DAY_OF_MONTH);
		return dateStr;
	}
	
	public static int longestConsecutiveOverlap(List<String> tokens1,
			List<String> tokens2) {
		HashMap<String,Integer> tokens2Map = new HashMap<String,Integer>();
		for (int i = tokens2.size()-1; i > -1; i--) {
			tokens2Map.put(tokens2.get(i), i);
		}
		int largestOverlap = 0;
		for (int i = 0; i < tokens1.size()-largestOverlap; i++) {
			if (tokens2Map.containsKey(tokens1.get(i))) {
				int j  = tokens2Map.get(tokens1.get(i));
				int overlap = 1;
				int index1 = i+1;
				int index2 = j+1;
				while (index1 < tokens1.size() && index2 < tokens2.size() && tokens1.get(index1).equals(tokens2.get(index2))) {
					overlap++;
					index1++;
					index2++;
				}
				if (overlap > largestOverlap) {
					largestOverlap = overlap;
				}
			}
		}
		return largestOverlap;
	}
	
	public static double percentOverlap(List<String> tokens1,
			List<String> tokens2) {
		int overlap = 0;
		double totalWords = 0;
		for (int i = 0; i < tokens1.size(); i++) {
			if (!Parameters.STOP_WORDS.contains(tokens1.get(i))) {
				for (int j = 0; j < tokens2.size(); j++) {
					if (tokens1.get(i).equals(tokens2.get(j))) {
						overlap++;
						break;
					}
				}
				totalWords++;
			}
		}
		return overlap/(totalWords);
	}
	
	public static List<String> split(String string, String delimiter) {
		String[] elements = string.split(delimiter);
		List<String> list = new ArrayList<String>();
		for (String element : elements) {
			list.add(element);
		}
		return list;
	}
	
	public static Calendar parseDate(String dateString) {
		String[] elements = dateString.split(" ");
		if (elements.length == 2 || elements.length == 1) {
			String[] firstElements = elements[0].split("-");
			if (firstElements.length == 3) {
				int year = Integer.parseInt(firstElements[0]);
				int month = Integer.parseInt(firstElements[1]);
				int day = Integer.parseInt(firstElements[2]);
				
				if (elements.length == 2) {
					String[] secondElements = elements[1].split(":");
					if (secondElements.length == 3) {
						int hour = Integer.parseInt(secondElements[0]);
						int minutes = Integer.parseInt(secondElements[1]);
						int seconds = Integer.parseInt(secondElements[2]);
						GregorianCalendar calendar = new GregorianCalendar();
						calendar.set(year, month, day, hour, minutes, seconds);
						return calendar;
					} else if (secondElements.length == 2) {
						int hour = Integer.parseInt(secondElements[0]);
						int minutes = Integer.parseInt(secondElements[1]);
						GregorianCalendar calendar = new GregorianCalendar();
						calendar.set(year, month, day, hour, minutes);
						return calendar;
					}
				} else {
					GregorianCalendar calendar = new GregorianCalendar();
					calendar.set(year, month, day, 0, 0, 0);
					return calendar;
				}
			}
		} 
		return null;
	}
	
	public static void writeToFile(String filename, String content, boolean append) {
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile(), append);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static HashMap<String, List<Sentence>> sortSentencesByDoc(Collection<Sentence> sentences) {
		HashMap<String, List<Sentence>> sentenceMap = new HashMap<String, List<Sentence>>();
		for (Sentence sentence : sentences) {
			if (!sentenceMap.containsKey(sentence.getDocId())) {
				sentenceMap.put(sentence.getDocId(),  new ArrayList<Sentence>());
			}
			sentenceMap.get(sentence.getDocId()).add(sentence);
		}
		for (List<Sentence> list : sentenceMap.values()) {
			Collections.sort(list, new Comparator<Sentence>(){
				@Override
				public int compare(Sentence sentence1, Sentence sentence2) {
					if (sentence1.getDocId().equals(sentence2.getDocId())) {
						if (sentence1.getSentenceId() < sentence2.getSentenceId()) {
							return -1;
						} else if (sentence1.getSentenceId() > sentence2.getSentenceId()) {
							return 1;
						}
					}
		            return 0;
				}
			});
		}
		return sentenceMap;
	}
	
	public static HashMap<Long, List<Sentence>> sortSentencesByDateAndDocument(Collection<Sentence> sentences, boolean rounded, boolean article) {
		HashMap<Long, List<Sentence>> sentenceMap = new HashMap<Long, List<Sentence>>();
		for (Sentence sentence : sentences) {
			long time;
			if (rounded) {
				if (article) {
					time = sentence.getRoundedArticleDate().getTimeInMillis();
				} else {
					time = sentence.getRoundedSentenceDate().getSecond().getTimeInMillis();
				}
			} else {
				if (article) {
					time = sentence.getArticleDate().getTimeInMillis();
				} else {
					time = sentence.getSentenceDate().getSecond().getTimeInMillis();
				}
			}
			if (!sentenceMap.containsKey(time)) {
				sentenceMap.put(time,  new ArrayList<Sentence>());
			}
			sentenceMap.get(time).add(sentence);
		}
		for (List<Sentence> list : sentenceMap.values()) {
			Collections.sort(list, new Comparator<Sentence>(){
				@Override
				public int compare(Sentence sentence1, Sentence sentence2) {
					if (sentence1.getDocId().equals(sentence2.getDocId())) {
						if (sentence1.getSentenceId() < sentence2.getSentenceId()) {
							return -1;
						} else if (sentence1.getSentenceId() > sentence2.getSentenceId()) {
							return 1;
						}
					}
		            return 0;
				}
			});
		}
		return sentenceMap;
	}

	public static boolean containsString(String string1, String string2) {
		if (string1.equals(string2)) {
			return true;
		}
		if (string1.startsWith(string2+" ")) {
			return true;
		}
		if (string1.endsWith(" "+string2)) {
			return true;
		}
		if (string1.contains(" "+string2+" ")) {
			return true;
		}
		return false;
	}
	
	
	private static <E> Set<Set<E>> getSetsRecursive(List<E> list, int size, Set<E> curSet) {
		if (curSet == null) {
			curSet = new HashSet<E>();
		}
		Set<Set<E>> sets = new HashSet<Set<E>>();
		for (int i = 0; i < list.size(); i++) {
			if (!curSet.contains(list.get(i))) {
				HashSet<E> tempSet = new HashSet<E>(curSet);
				tempSet.add(list.get(i));
				if (tempSet.size() < size) {
					sets.addAll(getSetsRecursive(list, size, tempSet));
				} else {
					sets.add(tempSet);
				}
			}
		}
		return sets;
	}
	
	public static Set<Set<Calendar>> getSets(ArrayList<Calendar> dateList, int i) {
		return getSetsRecursive(dateList, i, null);
	}
	
	public static void main(String args[]) {
		System.out.println(matchesSpecificDate("June 21"));
		System.out.println(matchesSpecificDate("October 1"));
		System.out.println(matchesSpecificDate("Sept. 21"));
		System.out.println(matchesSpecificDate("Sept. 21, 20012"));
		System.out.println(matchesSpecificDateYear("Sept. 21"));
		System.out.println(matchesSpecificDateYear("Sept. 21, 20012"));
	}
	
	
	public static int calculateLength(Collection<Sentence> sentences) {
		int length = 0;
		for (Sentence sentence : sentences) {
			if (sentence != null) 
				length += sentence.getBytes();
		}
		return length;
	}

	public static String join(double[] array, String delimiter) {
		String result = "";
		for (double element : array) {
			result += element+delimiter;
		}
		return result;
	}
	

	public static HashMap<String, ArrayList<Sentence>> sortByDocument(Collection<Sentence> sentences) {
		HashMap<String, ArrayList<Sentence>> sortedSentences = new HashMap<String, ArrayList<Sentence>>();
		for (Sentence sentence : sentences) {
			String docId = sentence.getDocId();
			if (!sortedSentences.containsKey(docId)) {
				sortedSentences.put(docId, new ArrayList<Sentence>());
			}
			sortedSentences.get(docId).add(sentence);
		}
		for (ArrayList<Sentence> sentenceGroup : sortedSentences.values()) {
			Collections.sort(sentenceGroup, new Comparator<Sentence>(){
				@Override
				public int compare(Sentence sentence1, Sentence sentence2) {
					int sentenceId1 = sentence1.getSentenceId();
		            int sentenceId2 = sentence2.getSentenceId();
					if (sentenceId1 < sentenceId2) {
		            	return -1;
		            } else if (sentenceId1 > sentenceId2) {
		            	return 1;
		            }
		            return 0;
				}
			});
		}
		return sortedSentences;
	}
	

}
