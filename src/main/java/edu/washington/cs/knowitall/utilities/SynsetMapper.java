package edu.washington.cs.knowitall.utilities;

import java.util.HashMap;

import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.smu.tspell.wordnet.WordSense;

public class SynsetMapper {
	private WordNetDatabase database;
	private HashMap<String, Synset[]> storedSynsetsVerb = new HashMap<String, Synset[]>();
	private HashMap<String, Synset[]> storedSynsetsNoun = new HashMap<String, Synset[]>();
	private HashMap<Synset, NounSynset[]> storedSynsetsHyponyms = new HashMap<Synset, NounSynset[]>();
	private HashMap<Synset, NounSynset[]> storedSynsetsHypernyms = new HashMap<Synset, NounSynset[]>();
	
	
	public static final String WORD_NET_DATABASE = System.getenv().get("WORDNET_DICT");
	
	public SynsetMapper() {
		if (WORD_NET_DATABASE == null) {
            System.setProperty("wordnet.database.dir", "lib/WordNet-3.0/dict");
			// System.err.println("Please specify the location of the WordNet database");
			// System.exit(1);
		} else System.setProperty("wordnet.database.dir", WORD_NET_DATABASE);
		database = WordNetDatabase.getFileInstance();
	}
	
	public Synset[] getSynsetsVerb(String verb) {
		if (storedSynsetsVerb.containsKey(verb)) {
			return storedSynsetsVerb.get(verb);
		} else {
			Synset[] synsets = database.getSynsets(verb, SynsetType.VERB);
			storedSynsetsVerb.put(verb, synsets);
			return synsets;
		}
	}
	
	public Synset[] getSynsetsNoun(String noun) {
		if (storedSynsetsNoun.containsKey(noun)) {
			return storedSynsetsNoun.get(noun);
		} else {
			Synset[] synsets = database.getSynsets(noun, SynsetType.NOUN);
			storedSynsetsNoun.put(noun, synsets);
			return synsets;
		}
	}
	
	
	public Synset[] getDerivationalyRelatedForms(String verb) {
		Synset[] verbSynsets = database.getSynsets(verb, SynsetType.VERB);
		for (Synset verbSynset : verbSynsets) {
			WordSense[] wordSenses = verbSynset.getDerivationallyRelatedForms(verb);
			Synset[] synsets = new Synset[wordSenses.length];
			int i = 0;
			for (WordSense wordSense : wordSenses) {
				synsets[i] = wordSense.getSynset();
				i++;
			}
			if (synsets.length > 0) {
				return synsets;
			}
		}
		
		return new Synset[0];
	}

	public NounSynset[] getHyponyms(NounSynset synset1) {
		if (storedSynsetsHyponyms.containsKey(synset1)) {
			return storedSynsetsHyponyms.get(synset1);
		} else {
			NounSynset[] synsets = synset1.getHyponyms();
			storedSynsetsHyponyms.put(synset1, synsets);
			return synsets;
		}
	}
	
	public NounSynset[] getHypernyms(NounSynset synset1) {
		if (storedSynsetsHypernyms.containsKey(synset1)) {
			return storedSynsetsHypernyms.get(synset1);
		} else {
			NounSynset[] synsets = synset1.getHypernyms();
			storedSynsetsHypernyms.put(synset1, synsets);
			return synsets;
		}
	}
}
