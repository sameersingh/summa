package edu.washington.cs.knowitall.utilities;

import edu.stanford.nlp.trees.Tree;
import edu.washington.cs.knowitall.datastructures.Sentence;

public class ParseTreeHandler {

	public static Tree locateString(Tree parseTree, String string) {
		string = string.toLowerCase();
		String[] tokens = string.split(" ");
		for (int i = 0; i < parseTree.getLeaves().size()-tokens.length+1; i++) {
			Tree leaf = parseTree.getLeaves().get(i);
			String label = "";
			for (int j = i; j < i+tokens.length; j++) {
				label += parseTree.getLeaves().get(j).label().value()+" ";
			}
			label = label.toLowerCase().trim();
			if (label.equals(string)) {
				return leaf;
			}
		}
		return null;
	}
	
	public static void printTreeRecursive(Tree tree, int level) {
		for (Tree child : tree.getChildrenAsList()) {
			for (int i = 0; i < level; i++) System.out.print("\t");
			System.out.println(child);
			printTreeRecursive(child, level+1);
		}
	}

	public static boolean withinFirstClause(Tree parseTree, String transition, boolean debug) {
		if (debug) printTreeRecursive(parseTree, 0);
		Tree leafNode = locateString(parseTree, transition);
		if (leafNode != null) {
			if (parseTree.children().length > 0 && parseTree.children()[0].label().value().equals("S")) {
				Tree rootTree = parseTree.children()[0];
				for (Tree child : rootTree.children()) {
					if (child.getLeaves().contains(leafNode)) {
						return true;
					}
					if (child.label().value().equals("S")) {
						return false;
					}
				}
			}
		}
		return false;
	}

	public static boolean beforeConj(Tree parseTree, String transition,
			boolean debug) {
		Tree leafNode = locateString(parseTree, transition);
		if (leafNode != null) {
			for (Tree leaf : parseTree.getLeaves()) {
				if (leaf.label().value().equals("and") || leaf.label().value().equals("but") || leaf.label().value().equals("who")) {
					return false;
				}
				if (leaf == leafNode) {
					return true;
				}
			}
		}
		return true;
	}

	public static String getSubj(Sentence sentence) {
		Tree parseTree = sentence.getParseTree();
		if (parseTree.children().length > 0 && parseTree.children()[0].label().value().startsWith("S")) {
			Tree rootTree = parseTree.children()[0];
			for (Tree child : rootTree.children()) {
				if (child.label().value().equals("NP")) {
					String subj = "";
					for (Tree leaf : child.getLeaves()) {
						subj += leaf.label().value()+" ";
					}
					subj = subj.trim();
					return subj;
				}
			}
		}
		return "";
	}
}