package edu.washington.cs.knowitall.utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;


import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefClusterIdAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.DocDateAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.time.TimeAnnotations.TimexAnnotation;
import edu.stanford.nlp.time.Timex;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;
import edu.washington.cs.knowitall.datastructures.Mention;
import edu.washington.cs.knowitall.datastructures.Pair;
import edu.washington.cs.knowitall.datastructures.Sentence;

public class StanfordNLPWrapper {

    protected StanfordCoreNLP pipeline;
    protected StanfordCoreNLP pipelineTokenizePosLemma;
    protected StanfordCoreNLP pipelineTokenizePos;
    
    public StanfordNLPWrapper() {
        // Create StanfordCoreNLP object properties, with POS tagging
        // (required for lemmatization), and lemmatization
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, cleanxml, ssplit, pos, lemma, ner, parse, dcoref");
        this.pipeline = new StanfordCoreNLP(props);

        Properties propsLemma = new Properties();
        propsLemma.put("annotators", "tokenize, ssplit, pos, lemma");
        this.pipelineTokenizePosLemma = new StanfordCoreNLP(propsLemma);
        
        Properties propsPos = new Properties();
        propsPos.put("annotators", "tokenize, ssplit, pos");
        this.pipelineTokenizePos = new StanfordCoreNLP(propsPos);
    }
    
    public List<CoreMap> getSentences(String documentText) {
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        
        // run all Annotators on this text
        this.pipeline.annotate(document);

        // Return sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        return sentences;
    }
    
    public List<CoreMap> getSentencesPosOnly(String documentText) {
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        
        // run all Annotators on this text
        this.pipelineTokenizePos.annotate(document);

        // Return sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        return sentences;
    }
    
    public List<CoreMap> getSentencesPosLemmaOnly(String documentText) {
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        
        // run all Annotators on this text
        this.pipelineTokenizePosLemma.annotate(document);

        // Return sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        return sentences;
    }
    
    public List<CoreMap> getSentences(String documentText, String date) {
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        document.set(DocDateAnnotation.class, date);

        // run all Annotators on this text
        this.pipeline.annotate(document);

        // Return sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        return sentences;
    }
    
    public List<String> lemmatize(String documentText) {
        List<String> lemmas = new LinkedList<String>();

        // Iterate over all of the sentences found
        List<CoreMap> sentences = getSentences(documentText);
        for(CoreMap sentence: sentences) {
            // Iterate over all tokens in a sentence
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
                lemmas.add(token.get(LemmaAnnotation.class));
            }
        }

        return lemmas;
    }
    
    public List<String> getPosTags(String documentText) {
        List<String> pos = new LinkedList<String>();

        // Iterate over all of the sentences found
        List<CoreMap> sentences = getSentencesPosOnly(documentText);
        for(CoreMap sentence: sentences) {
            // Iterate over all tokens in a sentence
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
            	pos.add(token.get(PartOfSpeechAnnotation.class));
            }
        }
        return pos;
    }
    
    public List<Pair<String,String>> getTokensAndPosTags(String documentText) {
        List<Pair<String,String>> tagsAndTokens = new LinkedList<Pair<String,String>>();

        // Iterate over all of the sentences found
        List<CoreMap> sentences = getSentencesPosOnly(documentText);
        for(CoreMap sentence: sentences) {
            // Iterate over all tokens in a sentence
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
            	tagsAndTokens.add(new Pair<String,String>(token.originalText(),
            			token.get(PartOfSpeechAnnotation.class)));
            }
        }
        return tagsAndTokens;
    }
    
    public List<String> getTokens(String documentText) {
        List<String> tokens = new LinkedList<String>();

        // Iterate over all of the sentences found
        List<CoreMap> sentences = getSentencesPosOnly(documentText);
        for(CoreMap sentence: sentences) {
            // Iterate over all tokens in a sentence
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                // Retrieve and add the lemma for each word into the
                // list of lemmas
            	tokens.add(token.originalText());
            }
        }
        return tokens;
    }
    
    public List<CoreMap> annotateSentence(String documentText) {
    	// create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        
        // run all Annotators on this text
        this.pipelineTokenizePosLemma.annotate(document);

        // Return sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        return sentences;
    }
    
    public List<CoreMap> annotateSentenceFull(String documentText) {
    	// create an empty Annotation just with the given text
        Annotation document = new Annotation(documentText);
        
        // run all Annotators on this text
        this.pipeline.annotate(document);

        // Return sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        return sentences;
    }
    
    public static String getSentenceText(CoreMap sentence) {
    	return Utilities.join(getSentenceTokens(sentence), " ");
    }
    
    public static List<String> getSentenceTokens(CoreMap sentence) {
    	List<String> sentenceTokens = new ArrayList<String>();
	    // Iterate over all tokens in a sentence
        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
            // Retrieve and add the token for each word.
        	sentenceTokens.add(token.originalText());
        }
    	return sentenceTokens;
    }
    
    public static List<String> getSentencePosTags(CoreMap sentence) {
    	List<String> sentenceTokens = new ArrayList<String>();
	    // Iterate over all tokens in a sentence
        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
            // Retrieve and add the pos tag for each word.
        	sentenceTokens.add(token.get(PartOfSpeechAnnotation.class));
        }
    	return sentenceTokens;
    }

    public static List<String> getSentenceLemmas(CoreMap sentence) {
    	List<String> sentenceLemmas = new ArrayList<String>();
	    // Iterate over all tokens in a sentence
        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
            // Retrieve and add the lemma for each word.
            sentenceLemmas.add(token.get(LemmaAnnotation.class));
        }
        return sentenceLemmas;
    }
    
    public static List<String> getSentenceNer(CoreMap sentence) {
    	List<String> sentenceNer = new ArrayList<String>();
	    // Iterate over all tokens in a sentence
        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
            // Retrieve and add the ner for each word.
        	sentenceNer.add(token.get(NamedEntityTagAnnotation.class));
        }
        return sentenceNer;
    }
    
    public static List<Pair<Integer,Calendar>> getSentenceDates(CoreMap sentence) {
    	List<Pair<Integer,Calendar>> dates = new ArrayList<Pair<Integer,Calendar>>();
	    // Iterate over all tokens in a sentence
    	int index = 0;
        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
            // Retrieve and add the calendar for each word.

        	if (token.get(TimexAnnotation.class) != null) {
        		Timex timex = token.get(TimexAnnotation.class);
            	if (timex.value() !=null) {
            		String value = timex.value();
            		if (!value.contains("X") && Pattern.matches("\\d+.*", value) && !value.equals("PRESENT_REF") && 
            				!value.equals("PAST_REF") && !value.equals("FUTURE_REF") && !value.equals("TMO")) {
	                	if (timex.timexType() != "DURATION") { 
	                		if (timex.getRange() != null) {
		            			dates.add(new Pair<Integer,Calendar>(index,timex.getRange().first()));
		            		} else {
		            			dates.add(new Pair<Integer,Calendar>(index,timex.getDate()));
		            		}
	            		}
            		} 
            	} 
        	}
            index++;
        }
        return dates;
    }
    
    public static Tree getParseTree(CoreMap sentence) {
    	Tree tree = sentence.get(TreeAnnotation.class);
    	return tree;
    }
    
    public Tree getParseTree(String sentenceText) {
    	 // create an empty Annotation just with the given text
        Annotation document = new Annotation(sentenceText);
        
        // run all Annotators on this text
        this.pipeline.annotate(document);

        // Return sentences found
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
    	
    	Tree tree = sentences.get(0).get(TreeAnnotation.class);
    	return tree;
    }
    
    public static Pair<Integer,Integer> getSpan(Tree originalTree, Tree subTree) {
    	List<Tree> leaves = originalTree.getLeaves();
    	List<Tree> children = subTree.getLeaves();
    	int start = -1;
    	int end = -1;
    	int index = 0;
    	for (Tree leaf : leaves) {
    		if (leaf == children.get(0)) {
    			start = index;
    		}
    		if (leaf == children.get(children.size()-1)) {
    			end = index+1;
    		}
    		index++;
    	}
    	return new Pair<Integer,Integer>(start,end);
    }
    
    public static List<Pair<Integer,Integer>> getNounPhrasesRecursive(Tree originalTree, Tree tree, int depth, boolean debug) {
    	ArrayList<Pair<Integer,Integer>> nounPhrases = new ArrayList<Pair<Integer,Integer>>();
    	for (Tree child : tree.getChildrenAsList()) {
    		if (child.depth() <= 2 && child.label().value().startsWith("N")) {
    			nounPhrases.add(getSpan(originalTree, child));
    		} else {
    			nounPhrases.addAll(getNounPhrasesRecursive(originalTree, child, depth+1, debug));
    		}
    	}
    	return nounPhrases;
    }
    
    public static List<Pair<Integer,Integer>> getNounPhrases(CoreMap sentence) {
    	boolean debug = false;
    	Tree tree = getParseTree(sentence);
    	List<Pair<Integer,Integer>> nounPhrases = getNounPhrasesRecursive(tree, tree, 0, debug);
    	return nounPhrases;
    }
    
    public static HashMap<Integer, ArrayList<Mention>> getCoreferences(CoreMap coreMap, Sentence sentence) {
    	HashMap<Integer, ArrayList<Mention>> coreferences = new HashMap<Integer, ArrayList<Mention>>();
    	int id = -1;
    	int start = -1;
    	int end = -1;
    	int last = -1;
    	for (int i = 0; i < coreMap.get(TokensAnnotation.class).size(); i++) {
    		CoreLabel token = coreMap.get(TokensAnnotation.class).get(i);
    		if (token.get(CorefClusterIdAnnotation.class) != null) {
	    		id = token.get(CorefClusterIdAnnotation.class);
	    		if (last == id) {
	    			end = i+1;
	    		} else {
	    			if (start != -1 && end != -1) {
		    			if (!coreferences.containsKey(id)) {
	    					coreferences.put(id, new ArrayList<Mention>());
	    				}
	    				coreferences.get(id).add(new Mention(sentence, start, end));
	    			}
    				start = i;
    				end = i+1;
	    		}
    		} else {
    			if (start != -1 && end != -1) {
    				if (!coreferences.containsKey(id)) {
    					coreferences.put(id, new ArrayList<Mention>());
    				}
    				coreferences.get(id).add(new Mention(sentence, start, end));
    			}
    			start = -1;
    			end = -1;
    		}
    	}
    	
    	return coreferences;
    }
    
    public static List<String> getLemmas(List<CoreMap> sentences) {
    	List<String> lemmas = new ArrayList<String>();
    	for (CoreMap sentence : sentences) {
    		lemmas.addAll(getSentenceLemmas(sentence));
    	}
    	return lemmas;
    }
   
}