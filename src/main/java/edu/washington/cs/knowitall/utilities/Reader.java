package edu.washington.cs.knowitall.utilities;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import edu.stanford.nlp.trees.PennTreeReader;
import edu.stanford.nlp.trees.Tree;
import edu.washington.cs.knowitall.datastructures.Coreference;
import edu.washington.cs.knowitall.datastructures.Extraction;
import edu.washington.cs.knowitall.datastructures.Mention;
import edu.washington.cs.knowitall.datastructures.Pair;
import edu.washington.cs.knowitall.datastructures.Sentence;

public class Reader {
	
	private final static String TOKEN = "word";
	private final static String LEMMA = "lemma";
	private final static String POS = "POS";
	private final static String NER = "NER";
	private final static String DATE = "NormalizedNER";
	private final static String START = "start";
	private final static String END = "end";
	private final static String SENTENCE = "sentence";
	private final static String COREF = "coreference";
	private final static String MENTION = "mention";
	
	private final static int MORNING = 7;
	private final static int NIGHT = 20;
	
	private final static int SPRING = 3;
	private final static int SUMMER = 5;
	private final static int FALL = 8;
	private final static int WINTER = 10;
	
	private final static int DEFAULT_MONTH = 0;
	private final static int DEFAULT_DAY = 1;
	private final static int DEFAULT_HOUR = 12;
	private final static int DEFAULT_MINUTE = 0;
	private final static int DEFAULT_SECOND = 0;
	private final static int DEFAULT_MILLISECOND = 0;
	
	private final static String STANFORD_NLP_DIRECTORY = "corenlp/";
	private final static String ORIGINAL_DIRECTORY = "original/";
	private final static String OLLIE_DIRECTORY = "/extractions/";
	
	public final static String DATE_MARKER_START = "<DATE_TIME>";
	public final static String DATE_MARKER_END = "</DATE_TIME>";
	public final static String DATE_MARKER_START2 = "<DATETIME>";
	public final static String DATE_MARKER_END2 = "</DATETIME>";
	
	public static HashMap<String, Sentence> readInSentenceCluster(String clusterFilename) {
		HashMap<String, Sentence> sentenceMap = new HashMap<String, Sentence>();
		
		File clusterDirectory = new File(clusterFilename+"/"+STANFORD_NLP_DIRECTORY);
		String[] children = clusterDirectory.list();
		for (int i = 0; i < children.length; i++) {
			String filename = children[i];
			if (!filename.startsWith(".") && !(new File(clusterDirectory.getAbsolutePath()+"/"+filename)).isDirectory()) {
				HashMap<String, Sentence> sentenceMapTemp = readInSentences(clusterDirectory.getAbsolutePath()+"/"+filename);
				sentenceMap.putAll(sentenceMapTemp);
			}
		}
		int key = 0;
		for (Sentence sentence : sentenceMap.values()) {
			sentence.setRedundancyKey(key);
			key++;
		}
		
		return sentenceMap;
	}
	
	private static Calendar readInDate(String filename) {
		String[] lines = Utilities.readInFileSafe(new File(filename));
		String fullFile = Utilities.readInFileAsOneStringSafe(new File(filename));
		String dateLine = "";
		if (fullFile.contains(DATE_MARKER_START2) && fullFile.contains(DATE_MARKER_END2)) {
			fullFile = fullFile.replaceAll("\n",  "");
			dateLine = fullFile.split(DATE_MARKER_START2)[1].trim();
			dateLine = dateLine.split(DATE_MARKER_END2)[0].trim();
		}

		if (dateLine.equals("")) {
			for (String line : lines) {
				if (line.contains(DATE_MARKER_START)) {
					dateLine = line.split(DATE_MARKER_START)[1].trim();
					dateLine = dateLine.split(DATE_MARKER_END)[0].trim();
					break;
				}
			}
		}
		
		if (Pattern.matches("\\d+/\\d+/\\d+ \\d+:\\d+:\\d+", dateLine)) {
			String[] dayTokens = dateLine.split(" ")[0].split("/");
			String[] timeTokens = dateLine.split(" ")[1].split(":");
			int month = Integer.parseInt(dayTokens[0])-1;
			int day = Integer.parseInt(dayTokens[1]);
			int year = Integer.parseInt(dayTokens[2]);
			int hour = Integer.parseInt(timeTokens[0]);
			int minutes = Integer.parseInt(timeTokens[1]);
			int seconds = Integer.parseInt(timeTokens[2]);
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.set(year, month, day, hour, minutes, seconds);
			return calendar;
		} else if (Pattern.matches("\\d+-\\d+-\\d+ \\d+:\\d+", dateLine)) {
			String[] dayTokens = dateLine.split(" ")[0].split("-");
			String[] timeTokens = dateLine.split(" ")[1].split(":");
			int year = Integer.parseInt(dayTokens[0]);
			int month = Integer.parseInt(dayTokens[1])-1;
			int day = Integer.parseInt(dayTokens[2]);
			int hour = Integer.parseInt(timeTokens[0]);
			int minutes = Integer.parseInt(timeTokens[1]);
			int seconds = 0;
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.set(year, month, day, hour, minutes, seconds);
			return calendar;
		} else if (Pattern.matches("\\d+-\\d+-\\d+ \\d+:\\d+:\\d+", dateLine)) {
				String[] dayTokens = dateLine.split(" ")[0].split("-");
				String[] timeTokens = dateLine.split(" ")[1].split(":");
				int year = Integer.parseInt(dayTokens[0]);
				int month = Integer.parseInt(dayTokens[1])-1;
				int day = Integer.parseInt(dayTokens[2]);
				int hour = Integer.parseInt(timeTokens[0]);
				int minutes = Integer.parseInt(timeTokens[1]);
				int seconds = Integer.parseInt(timeTokens[2]);
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.set(year, month, day, hour, minutes, seconds);
				return calendar;
		} else if (Pattern.matches("\\d+/\\d+/\\d+", dateLine)) {
			String[] dayTokens = dateLine.split("/");
			int month = Integer.parseInt(dayTokens[0])-1;
			int day = Integer.parseInt(dayTokens[1]);
			int year = Integer.parseInt(dayTokens[2]);
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.set(year, month, day, DEFAULT_HOUR, DEFAULT_MINUTE, DEFAULT_SECOND);
			return calendar;
		}
		System.out.println("Date string not found in " + filename + " (" + dateLine + ")");
		String dateString = filename.split("/")[filename.split("/").length-1].split("\\.")[0];
		int day = Integer.parseInt(dateString.substring(dateString.length()-2));
		int month = Integer.parseInt(dateString.substring(dateString.length()-4, dateString.length()-2))-1;
		int year = Integer.parseInt(dateString.substring(dateString.length()-8, dateString.length()-4));
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month, day, DEFAULT_HOUR, DEFAULT_MINUTE, DEFAULT_SECOND);
		return calendar;
	}
	
	private static String getTitle(String filename) {
		String[] tokens = filename.split(STANFORD_NLP_DIRECTORY);
		String originalFile = tokens[0]+ORIGINAL_DIRECTORY+tokens[1];
		String lines = Utilities.readInFileAsOneString(new File(originalFile));
		String title = "";
		if (lines.contains("<TITLE>") && lines.contains("</TITLE>")) {
			title += lines.split("<TITLE>")[1].split("</TITLE>")[0].trim()+" ";
		}
		return title.replaceAll("\n",  " ");
	}
	
	private static HashMap<String, Sentence> readInSentences(String filename) {
		HashMap<String, Sentence> sentenceMap = new HashMap<String, Sentence>();
		String title = getTitle(filename);
		if (title.startsWith("COMMENTARY:") || title.startsWith("EDITORIAL:")) {
			return sentenceMap;
		}
		String docId = getDocId(filename);
		String dateFilename = filename.split(STANFORD_NLP_DIRECTORY,2)[0]+ORIGINAL_DIRECTORY+filename.split("/")[filename.split("/").length-1];
		Calendar articleDate = readInDate(dateFilename);

		articleDate.set(Calendar.MILLISECOND, 0);
		String[] lines = Utilities.readInFile(new File(filename));
		
		// Identify sentence ids, tokens, lemmas, pos, ner
		ArrayList<String> sentenceLines = new ArrayList<String>();
		for (String line : lines) {
			line = line.trim().replaceAll("`", "'");
			if (line.startsWith("</"+SENTENCE)) {
				Sentence sentence = readInSentence(docId, articleDate, title, sentenceLines);
				if (sentence != null && sentence.getSentenceId() > -1 && sentence.getLength() > 1) {
					String key = Sentence.getKey(docId, sentence.getSentenceId());
					sentence.setCluster(filename.split("/")[filename.split("/").length-3]);
					sentenceMap.put(key, sentence);
					sentence.getSentenceDate();
				}
				sentenceLines.clear();
			} else {
				sentenceLines.add(line);
			}
		}
		
		ArrayList<Coreference> coreferences = processCoreferences(filename, sentenceMap);
		for (Coreference coreference : coreferences) {
			for (Mention mention : coreference.getMentions()) {
				docId = mention.getDocId();
				String key = mention.getKey();
				if (sentenceMap.containsKey(key)) {
					Sentence sentence = sentenceMap.get(key);
					sentence.addCoreference(coreference);
				}
			}
		}
		
		return sentenceMap;
	}
	
	private static ArrayList<Coreference> processCoreferences(String filename, HashMap<String, Sentence> sentenceMap) {
		String docId = getDocId(filename);
		String[] lines = Utilities.readInFile(new File(filename));
		
		ArrayList<Coreference> coreferences = new ArrayList<Coreference>();
		
		boolean incoreferenceSection = false;
		
		int start = -1;
		int end = -1;
		int sentenceId = -1;
		ArrayList<Mention> mentions = new ArrayList<Mention>();
		
		for (String line : lines) {
			if (incoreferenceSection) {
				if (line.contains("</"+COREF)) {
					Coreference coreference = new Coreference(docId, mentions);
					if (coreference.getMentions().size() > 1) {
						coreferences.add(coreference);
					}
					mentions = new ArrayList<Mention>();
				} else {
					if (line.contains("</"+MENTION)) {
						Sentence sentence = sentenceMap.get(Sentence.getKey(docId, sentenceId));
						if (sentence != null) {
							if (sentenceId == 0 && sentence.wasModified()) {
								if (start > 0) {
									start--;
								}
								end--;
							}
							Mention mention = new Mention(sentence, start, end);
							mentions.add(mention);
						}
					} else if (line.contains("<"+START)) {
						start = Integer.parseInt(removeXML(START, line))-1;
					} else if (line.contains("<"+END)) {
						end = Integer.parseInt(removeXML(END, line))-1;
					} else if (line.contains("<"+SENTENCE)) {
						sentenceId = Integer.parseInt(removeXML(SENTENCE, line))-2;
					}
				}
			} else {
				if (line.contains("<"+COREF)) {
					incoreferenceSection = true;
				}
			} 
		}
		return coreferences;
	}
	
	private static Sentence readInSentence(String docId, Calendar articleDate, String title, List<String> data) {
		int sentenceId = -1;
		ArrayList<String> tokenList = new ArrayList<String>();
		ArrayList<String> lemmaList = new ArrayList<String>();
		ArrayList<String> posList = new ArrayList<String>();
		ArrayList<String> nerList = new ArrayList<String>();
		ArrayList<Pair<Integer, Calendar>> sentenceDates = new ArrayList<Pair<Integer, Calendar>>();
		ArrayList<Pair<Integer,Integer>> nounPhrases = null;
		Tree parseTree = null;
		
		// Identify sentence ids, tokens, lemmas, pos, ner
		for (String line : data) {
			line = line.replaceAll("`", "'");
			if (line.startsWith("<sentence id=")) {
				line = line.replace("<sentence id=\"", "");
	            line = line.replace("\">", "");
	            sentenceId = Integer.parseInt(line)-2;
			} else if (line.startsWith("<"+TOKEN)) {
				tokenList.add(removeXML(TOKEN, line));
			} else if (line.startsWith("<"+LEMMA)) {
				lemmaList.add(removeXML(LEMMA, line));
			} else if (line.startsWith("<"+POS)) {
	            posList.add(removeXML(POS, line));
			} else if (line.startsWith("<"+NER)) {
	            nerList.add(removeXML(NER, line));
	        } else if (line.startsWith("<"+DATE) && nerList.get(nerList.size()-1).equals("DATE") && articleDate != null) {
	        	String date = removeXML(DATE, line);
	        	Calendar sentenceDate = parseStanfordDate(articleDate, date);
	        	if (sentenceDate != null) {
	        		sentenceDates.add(new Pair<Integer, Calendar>(tokenList.size()-1,sentenceDate));
	        	}
	        } else if (line.startsWith("<parse")) {
	        	parseTree = processParse(line);
	        	nounPhrases = processNounPhrases(line);
	        } 
		}
		boolean modified = false;
		if (sentenceId == -1) {
			return null;
		}
		
		Sentence sentence = new Sentence(docId,sentenceId,title,parseTree,tokenList,lemmaList,posList,nerList,articleDate,sentenceDates,nounPhrases);
		
		sentence.setModified(modified);
		return sentence;
	}
	
	public static List<Extraction> readInExtractionCluster(String clusterFilename, HashMap<String,Sentence> sentences) {
		List<Extraction> extractions = new ArrayList<Extraction>();
		File clusterDirectory = new File(clusterFilename+OLLIE_DIRECTORY);
		String[] children = clusterDirectory.list();
		for (int i = 0; i < children.length; i++) {
			String filename = children[i];
			if (!filename.startsWith(".")) {
				List<Extraction> extractionsTemp = readInExtractions(sentences, clusterDirectory.getAbsolutePath()+"/"+filename);
				extractions.addAll(extractionsTemp);
			}
		}
		return extractions;
	}
	
	private static List<Extraction> readInExtractions(HashMap<String,Sentence> sentences, String filename) {
		ArrayList<Extraction> extractions = new ArrayList<Extraction>();
		String docId = filename.split("/")[filename.split("/").length-1];
		if (!sentences.containsKey(Sentence.getKey(docId, 0)) && 
				!sentences.containsKey(Sentence.getKey(docId, 1))) {
			return extractions;
		}
		String[] lines = Utilities.readInFile(new File(filename));
		int count = 0;
		for (String line : lines) {
			line = line.replaceAll("`", "'");
			String[] elements = line.split("\t");
			if (elements.length == 9 && count > 0) {
				String arg1 = elements[1].trim();
				String relation = elements[2].trim();
				String arg2 = elements[3].trim();
				String arg3 = elements[4].trim();
				String arg4 = elements[5].trim();
				String sentenceStr = elements[6].trim();
				Double confidence = Double.parseDouble(elements[0].trim());
				Sentence sentence = locateSentence(sentences.values(), sentenceStr, docId);
				if (sentence != null && !relation.trim().equals("") && !arg1.equals("_NULL_") && !arg2.equals("_NULL_")) {
					if (Extraction.validExtraction(sentence, arg1, relation, arg2)) {
						Extraction extraction = new Extraction(sentence, arg1, relation, arg2, confidence);
						extractions.add(extraction);
						sentence.addExtraction(extraction);
						if (!arg3.equals("None")) {
							Pair<Integer, Integer> location = extraction.locatePhrase(arg3);
							if (location.getFirst() > -1) {
								extraction.addArgument(location.getFirst(), location.getSecond());
							}
						}
						if (!arg4.equals("None")) {
							Pair<Integer, Integer> location = extraction.locatePhrase(arg4);
							if (location.getFirst() > -1) {
								extraction.addArgument(location.getFirst(), location.getSecond());
							}
						}
					} 
				}	
			} else if (elements.length != 9 && elements.length != 7) {
				String arg1 = elements[1];
				String relation = elements[2];
				String arg2 = elements[3];
				String sentenceStr = elements[5];
				Double confidence = 1.0;//Double.parseDouble(elements[4]);
				Sentence sentence = locateSentence(sentences.values(), sentenceStr, docId);
				if (sentence != null && !arg1.equals("_NULL_") && !arg2.equals("_NULL_")) {
					if (Extraction.validExtraction(sentence, arg1, relation, arg2)) {
						Extraction extraction = new Extraction(sentence, arg1, relation, arg2, confidence);
						extractions.add(extraction);
						sentence.addExtraction(extraction);
					} 
				}
			}
			count++;
		}
		return extractions;
	}
	
	private static Sentence locateSentence(Collection<Sentence> sentences, String string, String docId) {
		String compressedString = Utilities.join(string.split(" "), "").replaceAll("``","").replaceAll("''","");
		for (Sentence sentence : sentences) {
			if (sentence.getDocId().equals(docId)) {
				String sentenceStr = Utilities.join(sentence.getTokenList(), "");
				if (compressedString.equals(sentenceStr) || (sentenceStr.contains("_") && compressedString.equals(sentenceStr.split("_")[1]))) {
					return sentence;
				}
			}
		}
		return null;
	}
	
	public static Tree processParse(String line) {
		PennTreeReader treeReader = new PennTreeReader(new StringReader(line));
		try {
			return treeReader.readTree();
		} catch (IOException e) {
			System.err.println("Could not read tree from line: "+line);
			e.printStackTrace();
			return null;
		}
	}
	
	private static ArrayList<Pair<Integer,Integer>> processNounPhrases(String line) {
		ArrayList<Pair<Integer,Integer>> nounPhrases = new ArrayList<Pair<Integer,Integer>>();
		Pattern splitPattern = Pattern.compile("\\(\\S+ \\S+\\)\\)");

		String originalLine = removeXML("parse", line);
		String revisedLine = originalLine;
		String last = "";
		while (Pattern.matches(".*\\(\\S+ \\S+\\)\\).*", revisedLine)) {
			String first = splitPattern.split(revisedLine)[0];
			first = first+revisedLine.substring(first.length()).split("\\)")[0]+"))";
			revisedLine = revisedLine.substring(first.length());
			String nounphrase = findStartOfParentheses(last+first);
			if (nounphrase.startsWith("(NP")) {
				int length = getTokenLength(nounphrase);
				int end = getTokenLength(last+first);
				int start = end-length;
				nounPhrases.add(new Pair<Integer,Integer>(start,end));
			}
			last += first;
		}

		return nounPhrases;
	}
	
	private static int getTokenLength(String tree) {
		String clean = removeParseTreeStuff(tree);
		String[] tokens = clean.split(" ");
		return tokens.length;
	}
	
	private static String removeParseTreeStuff(String tree) {
		String string = "";
		String[] tokens = tree.split(" ");
		for (String token : tokens) {
			if (token.endsWith(")")) {
				string += token.replace(")", "")+" ";
			}
		}
		return string;
	}
	
	private static String findStartOfParentheses(String root) {
		String string = "";
		int count = 0;
		for (int i = root.length()-1; i > 0; i--) {
			if (root.charAt(i) == ')') {
				count--;
			} else if (root.charAt(i) == '(') {
				count++;
			}
			string = root.charAt(i)+string;
			if (count == 0) {
				break;
			}
		}
		return string;
	}
	
	private static Calendar parseStanfordDate(Calendar articleDate, String date) {
		Calendar calendar = new GregorianCalendar();
		calendar.set(articleDate.get(Calendar.YEAR), DEFAULT_MONTH, DEFAULT_DAY, DEFAULT_HOUR, DEFAULT_MINUTE, DEFAULT_SECOND);
		calendar.set(Calendar.MILLISECOND, DEFAULT_MILLISECOND);

		// YEAR
		boolean foundDay = false;
		boolean foundMonth = false;
		boolean foundYear = false;
		if (Pattern.matches("\\d\\d\\d\\d.*", date)) {
			int year = Integer.parseInt(date.substring(0, 4));
			calendar.set(Calendar.YEAR, year);
			foundYear = true;
		} else if (Pattern.matches("\\d\\dXX.*", date)) {
			String yearStr = date.substring(0,2)+"01";
			int year = Integer.parseInt(yearStr);
			calendar.set(Calendar.YEAR, year);
			foundYear = true;
		} else if (Pattern.matches("\\d\\d\\dX.*", date)) {
			String yearStr = date.substring(0,3)+"1";
			int year = Integer.parseInt(yearStr);
			calendar.set(Calendar.YEAR, year);
			foundYear = true;
		} 
		// MONTH
		if (Pattern.matches("....-\\d\\d.*", date)) {
			// Month is apparently 0 indexed.
			int month = Integer.parseInt(date.substring(5, 7))-1;
			calendar.set(Calendar.MONTH, month);
			foundMonth = true;
		} else if (Pattern.matches("....-W\\d\\d.*", date)) {
			int week = Integer.parseInt(date.substring(6,8));
			calendar.set(Calendar.WEEK_OF_YEAR, week);
		} else if (Pattern.matches("....-FA.*", date)) {
			int month = FALL;
			calendar.set(Calendar.MONTH, month);
		} else if (Pattern.matches("....-SU.*", date)) {
			int month = SUMMER;
			calendar.set(Calendar.MONTH, month);
		} else if (Pattern.matches("....-SP.*", date)) {
			int month = SPRING;
			calendar.set(Calendar.MONTH, month);
		} else if (Pattern.matches("....-WI.*", date)) {
			int month = WINTER;
			calendar.set(Calendar.MONTH, month);
		}
		// DAY OF MONTH
		if (Pattern.matches("....-\\d\\d-\\d\\d.*", date)) {
			int day = Integer.parseInt(date.substring(8, 10));
			calendar.set(Calendar.DAY_OF_MONTH, day);
			foundDay = true;
		}
		// HOUR
		if (Pattern.matches(".*NI", date)) {
			calendar.set(Calendar.HOUR_OF_DAY, NIGHT);
		} else if (Pattern.matches(".*MO", date)) {
			calendar.set(Calendar.HOUR_OF_DAY, MORNING);
		} 
		// OTHER DATES
		if (date.matches("PRESENT_REF")) {
			calendar = articleDate;
			foundDay = true;
			foundMonth = true;
			foundYear = true;
		}
		else if (date.matches("\\d\\d\\d\\d/\\d\\d\\d\\d.*")) {
			int year = Integer.parseInt(date.substring(0, 4));
			calendar.set(year, DEFAULT_MONTH, DEFAULT_DAY);
		} else if (date.matches("P\\dY")) {
			int year = articleDate.get(Calendar.YEAR)-Integer.parseInt(date.substring(1,2));
			calendar.set(year, DEFAULT_MONTH, DEFAULT_DAY);
		}
		if (foundDay && foundMonth && foundYear) {
			return calendar;
		}
		return null;
	}
	
	private static String removeXML(String code, String line) {
		line = line.replace("<"+code+">", "");
        line = line.replace("</"+code+">", "");
        return line.trim();
	}
	
	private static String getDocId(String filename) {
		String docId = filename.split("/")[filename.split("/").length-1].split(".xml")[0];
		return docId;
	}
	
}
