package edu.washington.cs.knowitall.utilities;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.washington.cs.knowitall.datastructures.Document;
import edu.washington.cs.knowitall.datastructures.Sentence;

public class TfidfCalculator {
	private HashMap<String,Double> tfScores = new HashMap<String,Double>();
	private HashMap<String,Double> idfScores = new HashMap<String,Double>();
	private HashMap<String,Integer> dfScores = new HashMap<String,Integer>();
	public int tfCount = 0;
	public int tfAlreadyCount = 0;
	public int idfCount = 0;
	public int idfAlreadyCount = 0;
	private double size = 0;
	private String idfFilename = "data/idf.txt";
	
	public TfidfCalculator() {
		setup(idfFilename);
	}
	
	private void setup(String filename) {
		String[] lines = Utilities.readInFile(new File(filename));
		for (String line : lines) {
			String[] tokens = line.split("\t");
			if (tokens.length == 2) {
				String term = tokens[0];
				double idf = Double.parseDouble(line.split("\t")[1]);
				idfScores.put(term, idf);
			} else if (line.startsWith("documentSize::")) {
				size = Double.parseDouble(line.split("::")[1]);
			}
		}
	}
	
	public void setup(Collection<Sentence> sentences) {
		HashMap<String,ArrayList<Sentence>> sentenceMap = new HashMap<String,ArrayList<Sentence>>();
		for (Sentence sentence : sentences) {
			if (!sentenceMap.containsKey(sentence.getDocId())) {
				sentenceMap.put(sentence.getDocId(),  new ArrayList<Sentence>());
			}
			sentenceMap.get(sentence.getDocId()).add(sentence);
		}
		HashSet<String> docSet = new HashSet<String>();
		for (ArrayList<Sentence> document : sentenceMap.values()) {
			HashSet<String> alreadySeen = new HashSet<String>();
			for (Sentence sentence : document) {
				List<String> terms = sentence.getLemmaList();
				for (String term : terms) {
					if (!alreadySeen.contains(term)) {
						calculateDF(sentence, term);
						alreadySeen.add(term);
						docSet.add(sentence.getDocId());
					}
				}
			}
		}
		for (String term : dfScores.keySet()) {
			idfScores.put(term, Math.log((docSet.size()+0.0)/dfScores.get(term)));
		}
	}
	
	public void setup(List<Document> documents) {
		for (int i = 0; i < documents.size(); i++) {
			HashSet<String> alreadySeen = new HashSet<String>();
			Set<String> terms = documents.get(i).getTermSet();
			for (String term : terms) {
				if (!alreadySeen.contains(term)) {
					calculateDF(documents.get(i), term);
					alreadySeen.add(term);
				}
			}
		}
		for (String term : dfScores.keySet()) {
			idfScores.put(term, Math.log((documents.size()+0.0)/dfScores.get(term)));
		}
	}
	
	public double calculateTF(Sentence sentence1, String term1) {
		String key = sentence1.getKey()+":"+term1;
		if (tfScores.containsKey(key)) {
			tfAlreadyCount++;
			return tfScores.get(key);
		}
		double tf = 0.0;
		for (String term2 : sentence1.getLemmaList()) {
			if (term1.equals(term2)) {
				tf++;
			}
		}
		tfCount++;
		tfScores.put(key, tf);
		return tf;
	}
	
	public double calculateTF(String[] terms, String term1) {
		double tf = 0.0;
		for (String term2 : terms) {
			if (term1.equals(term2)) {
				tf++;
			}
		}
		return tf;
	}
	
	private double calculateTF(Document doc, String term1) {
		if (tfScores.containsKey(doc.getDocId()+"::"+term1)) {
			return tfScores.get(doc.getDocId()+"::"+term1);
		}
		double tf = 0.0;
		for (String term2 : doc.getTermList()) {
			if (term1.equals(term2)) {
				tf++;
			}
		}
		tfScores.put(doc.getDocId()+"::"+term1, tf);
		return tf;
	}

	private void calculateDF(Document document, String term) {
		int value = 1;
		if (dfScores.containsKey(term)) {
			value += dfScores.get(term);
		}
		dfScores.put(term, value);
	}
	
	private void calculateDF(Sentence sentence, String term) {
		int value = 1;
		if (dfScores.containsKey(term)) {
			value += dfScores.get(term);
		}
		dfScores.put(term, value);
	}
	
	private double calculateIDF(String term) {
		if (idfScores.containsKey(term)) {
			return idfScores.get(term);
		}
		idfScores.put(term, Math.log(size/1.0));
		return Math.log(size/1.0);
	}
	
	public double calculateCosineSimilarity(Document doc1, Document doc2, boolean debug) {
		double numerator = 0.0;
		double denominator1 = 0.0;
		for (String term_i : doc1.getTermList()) {
			double tf1 = calculateTF(doc1, term_i);
			double tf2 = calculateTF(doc2, term_i);
			double idf = 1.0;
			numerator += tf1*tf2*idf*idf;
			denominator1 += tf1*tf1*idf*idf;
		}
		if (denominator1 == 0) {
			return 0.0;
		}
		double denominator2 = 0.0;
		for (String term_i : doc2.getTermList()) {
			double tf = calculateTF(doc2, term_i);
			double idf = 1.0;
			denominator2 += tf*tf*idf*idf;
		}
		if (denominator2 == 0) {
			return 0.0;
		}
		double cosineSimilarity = numerator/(Math.sqrt(denominator1) * Math.sqrt(denominator2));
		return cosineSimilarity;
	}
	
	
	private double calculateCosineSimilarity(String[] doc1, String[] doc2, boolean debug) {
		double numerator = 0.0;
		double denominator1 = 0.0;
		for (String term_i : doc1) {
			double tf1 = calculateTF(doc1, term_i);
			double tf2 = calculateTF(doc2, term_i);
			double idf = calculateIDF(term_i);
			double temp1 = tf1*tf2*idf*idf;
			double temp2 = tf1*tf1*idf*idf;
			
			numerator += temp1;
			denominator1 += temp2;
		}
		if (denominator1 == 0) {
			return 0.0;
		}
		double denominator2 = 0.0;
		for (String term_i : doc2) {
			double tf = calculateTF(doc2, term_i);
			double idf = calculateIDF(term_i);
			double temp1 = tf*tf*idf*idf;
			denominator2 += temp1;
		}
		if (denominator2 == 0) {
			return 0.0;
		}
		double cosineSimilarity = numerator/(Math.sqrt(denominator1) * Math.sqrt(denominator2));
		return cosineSimilarity;
	}
	
	public double calculateCosineSimilarity(List<String> list1, List<String> list2, boolean debug) {
		int i = 0;
		HashSet<String> set1 = new HashSet<String>();
		set1.addAll(list1);
		String[] doc1 = new String[set1.size()];
		for (String element : set1) {
			doc1[i] = element;
			i++;
		}
		i = 0;
		HashSet<String> set2 = new HashSet<String>();
		set2.addAll(list2);
		String[] doc2 = new String[set2.size()];
		for (String element : set2) {
			doc2[i] = element;
			i++;
		}
		return calculateCosineSimilarity(doc1, doc2, debug);
	}
	
}
