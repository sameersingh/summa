package edu.washington.cs.knowitall.utilities;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import scala.collection.Iterable;
import scala.collection.Iterator;

import edu.knowitall.ollie.Ollie;
import edu.knowitall.ollie.OllieExtraction;
import edu.knowitall.ollie.OllieExtractionInstance;
import edu.knowitall.tool.parse.MaltParser;
import edu.knowitall.tool.parse.graph.DependencyGraph;

public class OllieWrapper {
	private Ollie ollie;
	private MaltParser maltParser;
	private static String MALT_PARSER_FILENAME = System.getenv().get("MALT_MCO");
	
	public OllieWrapper() {
		scala.Option<File> nulloption = scala.Option.apply(null);
        if (MALT_PARSER_FILENAME == null) {
            maltParser = new MaltParser(new File("lib/engmalt.linear-1.7.mco"));
        } else maltParser = new MaltParser(new File(MALT_PARSER_FILENAME));
		ollie = new Ollie();
	}
	
	/**
	 * Gets Ollie extractions from a single sentence.
	 * @param sentence
	 * @return the set of ollie extractions
	 */
	public Iterable<OllieExtractionInstance> extract(String sentence) {
        try {
            DependencyGraph graph = ((MaltParser) maltParser).dependencyGraph(sentence);
            Iterable<OllieExtractionInstance> extrs = ollie.extract(graph);
            return extrs;
        } catch(Exception e) {
            e.printStackTrace();
            return scala.collection.immutable.List.empty();
        }
	}
	
	public static void main(String args[]) throws IOException {
		//Initialize.
		OllieWrapper ollieWrapper = new OllieWrapper();

        if(args.length == 0) {
            // Extract from a single sentence.
            String sentence = "President Obama will meet with Congressional leaders on Friday, and House Republicans summoned lawmakers back for a Sunday session, in a last-ditch effort to avert a fiscal crisis brought on by automatic tax increases and spending cuts scheduled to hit next week.";
            Iterable<OllieExtractionInstance> extrs = ollieWrapper.extract(sentence);

            // Print the extractions.
            Iterator<OllieExtractionInstance> iterator = extrs.iterator();
            while (iterator.hasNext()) {
                OllieExtraction extr = iterator.next().extr();
                System.out.println(extr.arg1().text() + "\t" + extr.rel().text() + "\t" + extr.arg2().text());
            }
        } else {
            String dir = args[0];
            String outputDir = args[1];
            assert(new File(dir).isDirectory());
            assert(new File(outputDir).isDirectory());
            String[] files = new File(dir).list();
            for(int i = 0; i < files.length; i++) {
                System.out.println(files[i]);
                File input = new File(dir + "/" + files[i]);
                String fileString = Utilities.readInFileAsOneStringSafe(input);
                fileString = fileString.replaceAll("<DOC>", "");
                fileString = fileString.replaceAll("</DOC>", "");
                fileString = fileString.replaceAll("<TEXT>", "");
                fileString = fileString.replaceAll("</TEXT>", "");
                fileString = fileString.replaceAll("<DATETIME>.*</DATETIME>", "");
                fileString = fileString.replaceAll("<TITLE>.*</TITLE>", "").trim();
                String[] lines = fileString.split("\n\n");
                System.out.println("Lines: " + lines.length);
                File output = new File(outputDir + "/" + files[i].replaceAll(".xml", ""));
                PrintWriter writer = new PrintWriter(output);
                for(int l=0; l<lines.length; l++) {
                    Iterable<OllieExtractionInstance> extrs = ollieWrapper.extract(lines[l]);
                    // Print the extractions.
                    Iterator<OllieExtractionInstance> iterator = extrs.iterator();
                    while (iterator.hasNext()) {
                        OllieExtraction extr = iterator.next().extr();
                        System.out.println(extr.arg1().text() + "\t" + extr.rel().text() + "\t" + extr.arg2().text());
                        writer.println(l + "\t" + extr.arg1().text() + "\t" + extr.rel().text() + "\t" + extr.arg2().text()  + "\t" + extr.openparseConfidence() + "\t" + lines[l].replaceAll("\n", " "));
                    }
                }
                writer.flush();
                writer.close();
            }
        }
	}	
}
