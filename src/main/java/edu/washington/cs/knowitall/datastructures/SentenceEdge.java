package edu.washington.cs.knowitall.datastructures;

import java.util.HashSet;

public class SentenceEdge {
	private Sentence sourceSentence;
	private Sentence destSentence;
	private double weight;
	private String reason;
	private EDGE_TYPE type;
	private HashSet<String> synonyms;
	
	public enum EDGE_TYPE {
	    PERSON,NOUN,NOUNVERB,TRANSITION,INFERTRANSITION,NECNOUNVERB;
	}
	
	public SentenceEdge(Sentence sourceSentence, Sentence destSentence, double weight, String reason, EDGE_TYPE type, HashSet<String> synonyms) {
		this.sourceSentence = sourceSentence;
		this.destSentence = destSentence;
		this.weight = weight;
		this.reason = reason;
		this.type = type;
		this.setSynonyms(synonyms);
	}
	
	public Sentence getSource() {
		return sourceSentence;
	}
	
	public Sentence getDestination() {
		return destSentence;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public static String getKey(Sentence sourceSentence, Sentence destSentence) {
    	String destKey = "";
    	String sourceKey = "";
    	if (destSentence == null) {
    		destKey = "null.-1";
    	} else {
    		destKey = destSentence.getKey();
    	}
    	if (sourceSentence == null) {
    		sourceKey = "null.-1";
    	} else {
    		sourceKey = sourceSentence.getKey();
    	}
    	return sourceKey+"::"+destKey;
	}

	public String getReason() {
		return reason;
	}

	public EDGE_TYPE getType() {
		return type;
	}

	public HashSet<String> getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(HashSet<String> synonyms) {
		this.synonyms = synonyms;
	}
}
