package edu.washington.cs.knowitall.datastructures;

import java.util.List;

public class Mention {
	private Sentence sentence;
	private int start;
	private int end;
	
	public Mention(Sentence sentence, int start, int end) {
		this.sentence = sentence;
		this.setStart(start);
		this.setEnd(end);
	}

	public String getKey() {
		return Sentence.getKey(getDocId(), getSentenceId());
	}
	
	public String getDocId() {
		return sentence.getDocId();
	}

	public int getSentenceId() {
		return sentence.getSentenceId();
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}
	
	public String getTokens() {
		return sentence.getTokens(start, end);
	}

	public String toString() {
		return getSentenceId()+" "+sentence.getTokens(start, end);
	}

	public Sentence getSentence() {
		return sentence;
	}
	
	public boolean isPronoun() {
		if (end-start > 1) {
			return false;
		}
		List<String> pos = getSentence().getPosList();
		if (pos.get(start).startsWith("PRP")) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isPerson() {
		List<String> ner = getSentence().getNerList();
		for (int i = start; i < end; i++) {
			if (ner.get(i).equals("PERSON")) {
				return true;
			}
		}
		return false;
	}
}
