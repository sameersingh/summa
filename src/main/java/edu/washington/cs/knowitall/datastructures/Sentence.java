package edu.washington.cs.knowitall.datastructures;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import edu.washington.cs.knowitall.summarization.ActionResponseGenerator;
import edu.washington.cs.knowitall.summarization.Parameters;
import edu.washington.cs.knowitall.utilities.Compressor;
import edu.washington.cs.knowitall.utilities.ParseTreeHandler;
import edu.washington.cs.knowitall.utilities.StanfordNLPWrapper;
import edu.washington.cs.knowitall.utilities.Utilities;

public class Sentence {
	private String docId;
	private int sentenceId;
	private String title;
	
	private List<String> tokenList;
	private List<String> lemmaList;
	private List<String> lemmaListNoStopWords;
	private List<String> posList;
	private List<String> nerList;
	private List<String> tokenListNoNums;
	
	private ArrayList<Integer> activeIndices;
	private List<String> verbList;
	private List<String> activeVerbList;
	private ArrayList<Pair<Integer, String>> nouns;
	private ArrayList<Pair<Integer, String>> activeNouns;
	private ArrayList<Pair<Integer, String>> properNouns;
	private ArrayList<Pair<Integer, String>> activeProperNouns;
	private  List<Pair<Integer,Integer>> nounPhrases;
	private HashSet<String> people;
	private ArrayList<Coreference> coreferences = new ArrayList<Coreference>();
	private Tree parseTree;
	
	private Calendar articleDate;
	private Pair<Integer,Calendar> sentenceDate;
	private Pair<Integer,Calendar> roundedSentenceDate;
	private List<Pair<Integer, Calendar>> sentenceDates;
	
	private int bytes;
	
	private ArrayList<Extraction> extractions = new ArrayList<Extraction>();
	
	private String key = "";
	private String cluster = "";
	private boolean modified;
	
	private int redundancyKey = -1;
	
	private boolean valid = true;
	
	public void setRedundancyKey(int redundancyKey) {
		this.redundancyKey = redundancyKey;
	}
	
	public int getRedundancyKey() {
		return redundancyKey;
	}
	
	public Sentence(String docId, int sentenceId, String title, Tree parseTree,
			List<String> tokenList, List<String> lemmaList, List<String> posList, List<String> nerList,
			Calendar articleDate, List<Pair<Integer, Calendar>> sentenceDates, List<Pair<Integer,Integer>> nounPhrases) {
		this.docId = docId;
		this.sentenceId = sentenceId;
		this.title = title;
		this.setParseTree(parseTree);
		
		this.tokenList = tokenList;
		this.lemmaList = lemmaList;
		this.posList = posList;
		this.nerList = nerList;
		
		this.activeIndices = Compressor.compressIndices(this, false);
		this.verbList = findVerbs(false);
		this.activeVerbList = findVerbs(true);
		this.nounPhrases = nounPhrases;		
		this.nouns = findNouns(false,false);
		this.activeNouns = findNouns(false,true);
		this.properNouns = findNouns(true,false);
		this.activeProperNouns = findNouns(true,true);
		
		this.articleDate = articleDate;
		this.sentenceDates = sentenceDates;
	
		bytes = Utilities.join(getTokenList(), " ").getBytes().length;
		this.key = Sentence.getKey(getDocId(), getSentenceId());
	}
	
	public Sentence(String docId, int sentenceId, String title,
			List<String> tokenList, List<String> lemmaList, List<String> posList) {
		this.docId = docId;
		this.sentenceId = sentenceId;
		this.title = title;
		
		this.tokenList = tokenList;
		this.lemmaList = lemmaList;
		this.posList = posList;
		
		this.activeIndices = Compressor.compressIndices(this, false);
		this.verbList = findVerbs(false);
		this.activeVerbList = verbList;
		this.nouns = findNouns(false,false);
		this.activeNouns = nouns;
		this.properNouns = findNouns(true,false);
		this.activeProperNouns = properNouns;
		
		bytes = Utilities.join(getTokenList(), " ").getBytes().length;
		this.key = Sentence.getKey(getDocId(), getSentenceId());
	}
	
	public Sentence(String docId, int sentenceId, String title, Calendar articleDate, List<Pair<Integer,Integer>> nounPhrases, CoreMap coreMap) {
		this.docId = docId;
		this.sentenceId = sentenceId;
		this.title = title;
		this.parseTree = StanfordNLPWrapper.getParseTree(coreMap);
		
		this.tokenList = StanfordNLPWrapper.getSentenceTokens(coreMap);
		this.posList = StanfordNLPWrapper.getSentencePosTags(coreMap);
		this.lemmaList = StanfordNLPWrapper.getSentenceLemmas(coreMap);
		this.nerList = StanfordNLPWrapper.getSentenceNer(coreMap);

		this.activeIndices = Compressor.compressIndices(this, false);
		this.verbList = findVerbs(false);
		this.activeVerbList = findVerbs(true);
		this.nounPhrases = nounPhrases;
		this.nouns = findNouns(false,false);
		this.activeNouns = findNouns(false,true);
		this.properNouns = findNouns(true,false);
		this.activeProperNouns = findNouns(true,true);
		
		this.articleDate = articleDate;
		this.sentenceDates = StanfordNLPWrapper.getSentenceDates(coreMap);
		
		bytes = Utilities.join(getTokenList(), " ").getBytes().length;
		this.key = Sentence.getKey(getDocId(), getSentenceId());
	}
	
	public Sentence() {
	}

	/**** Basic information ****/
	public static String getKey(String docId, int sentenceId) {
		return docId+"::"+sentenceId;
	}

	public String getKey() {
		return key;
	}
	
	public String getDocId() {
		return docId;
	}

	public int getSentenceId() {
		return sentenceId;
	}
	
	public String getTitle() {
		return title;
	}

	public String getSentenceStr() {
		return Utilities.join(getTokenList(), " ");
	}
	
	public String toString() {
		return getDocId()+"\t"+getSentenceId()+"\t"+getSentenceStr();
	}

	public int getLength() {
		return tokenList.size();
	}

	public int getBytes() {
		return bytes;
	}
		
	public static String getKey(String docId, double sentenceId) {
		return docId+"::"+sentenceId;
	}
	
	/**** Parse information information ****/
	public List<String> getTokenList() {
		return tokenList;
	}
	
	public String getToken(int index) {
		return tokenList.get(index);
	}
	
	public String getTokens(int start, int end) {
		return Utilities.join(tokenList, " ", start, end);
	}
	
	public List<String> getLemmaList() {
		return lemmaList;
	}
	public List<String> getLemmaListNoStopWords() {
		if (lemmaListNoStopWords != null) {
			return lemmaListNoStopWords;
		}

		List<String> list = new ArrayList<String>();
		for (int i = 0; i < getLength(); i++) {
			if (!getPos(i).equals("DET") && !getPos(i).equals("RB") && !getPos(i).equals("PP") && 
					!getPos(i).equals("IN") && !getPos(i).equals("RP") && !Parameters.STOP_WORDS.contains(getLemma(i))) {
				list.add(getLemma(i));
			}
		}
		lemmaListNoStopWords = list;
		return lemmaListNoStopWords;
	}

	public String getLemma(int index) {
		return lemmaList.get(index);
	}
	
	public List<String> getPosList() {
		return posList;
	}

	public String getPos(int index) {
		return posList.get(index);
	}

	public List<String> getNerList() {
		return nerList;
	}

	public String getNer(int index) {
		return nerList.get(index);
	}
	
	/**** Date information ****/
	public Calendar getArticleDate() {
		return articleDate;
	}

	public Calendar getArticleDateOrig() {
		return articleDate;
	}

	public List<Pair<Integer, Calendar>> getSentenceDates() {
		return sentenceDates;
	}
	
	/**** Noun information ****/
	public ArrayList<Pair<Integer, String>> getNouns() {
		return nouns;
	}
	
	/**** Noun information ****/
	public ArrayList<Pair<Integer, String>> getActiveNouns() {
		return activeNouns;
	}
	
	
	public ArrayList<Pair<Integer, String>> getProperNouns() {
		return properNouns;
	}
	
	public ArrayList<Pair<Integer, String>> getActiveProperNouns() {
		return activeProperNouns;
	}
	
	
	public boolean active(int index) {
		if (getTokenList().contains("because")) {
			if (!activeIndices.contains(index)) {
				return false;
			}
			for (int i = 0; i < index; i++) {
				if (getToken(i).equals("because")) {
					return false;
				}
			}
			return true;
		} else {
			return activeIndices.contains(index);
		}
	}
	
	public ArrayList<Pair<Integer, String>> findNouns(boolean properNouns, boolean active) {
		ArrayList<Pair<Integer,String>> nouns = new ArrayList<Pair<Integer,String>>();
		int i = 0;
		while (i < tokenList.size()) {
			String noun = "";
			while (i < posList.size() && ((properNouns && posList.get(i).startsWith("NNP")) || 
					(!properNouns && posList.get(i).startsWith("N") && !posList.get(i).startsWith("NNP")))) {
				noun += lemmaList.get(i)+" ";
				if (endOfNounPhrase(i)) {
					break;
				} else {
					i+=1;
				}
			}

			if (!noun.equals("")) {
				if (i > tokenList.size()-1 || !posList.get(i).startsWith("N")) {
					i--;
				}
				if (!active || active(i)) {
					nouns.add(new Pair<Integer, String>(i,noun.trim()));
				}
			}
			i++;
		}
		return nouns;
	}
	
	public List<Pair<Integer,Integer>> getNounPhrases() {
		return nounPhrases;
	}
	
	public boolean endOfNounPhrase(int index) {
		if (nounPhrases == null) {
			if (posList.get(index).startsWith("N")) {
				return true;
			} else {
				return false;
			}
		}
		for (Pair<Integer,Integer> phrase : nounPhrases) {
			if (index == phrase.getSecond()-1) {
				return true;
			}
		}
		return false;
	}
	
	/**** Verb information ****/
	private List<String> findVerbs(boolean active) {
		ArrayList<String> verbs = new ArrayList<String>();
		for (int i = 0; i < posList.size(); i++) {
			String pos = posList.get(i);
			String lemma = lemmaList.get(i);
			//TODO REMOVED THIS
			if (pos.startsWith("V") && (!active || active(i))) {// && !Parameters.STOP_VERBS.contains(lemma)) {
				verbs.add(lemma);
			}
		}
		return verbs;
	}
	
	public List<String> getVerbs() {
		return verbList;
	}
	
	public List<String> getActiveVerbs() {
		return activeVerbList;
	}

	/**** sentence level analysis****/
	public boolean containsProperNouns() {
		if (getProperNouns().size() > 0) {
			return true;
		}
		return false;
	}

	public boolean containsNouns() {
		if (getNouns().size() > 0) {
			return true;
		}
		return false;
	}
	
	public boolean containsPronouns() {
		for (String pos : posList) {
			if (pos.equals("PRP")) {
				return true;
			}
		}
		return false;
	}

	public boolean containsQuotes() {
		String sentenceStr = getSentenceStr();
		if (sentenceStr.contains("\"") || sentenceStr.contains("''") || sentenceStr.contains("' '")) {
			return true;
		}
		return false;
	}

	public boolean containsNumbers() {
		for (String pos : posList) {
			if (pos.equals("CD")) {
				return true;
			}
		}
		return false;
	}

	public boolean containsMoney() {
		for (String pos : posList) {
			if (pos.equals("$")) {
				return true;
			}
		}
		return false;
	}
	
	/**** people in sentence ****/
	public HashSet<String> getPeopleStrings() {
		HashSet<String> people = new HashSet<String>();
		for (int i = 0; i < posList.size(); i++) {
			if (posList.get(i).equals("NNP") && nerList.get(i).equals("PERSON") && endOfNounPhrase(i)) {
				people.add(tokenList.get(i).toLowerCase());
			}
		}
		return people;
	}
	
	public HashSet<String> getPeopleStringsFull() {
		if (people != null) {
			return people;
		}
		people = new HashSet<String>();
		String curPerson = "";
		for (int i = 0; i < posList.size(); i++) {
			if (nerList.get(i).equals("PERSON")) {
				if (i > 0 && curPerson.equals("") && Parameters.TITLES.contains(getToken(i-1))) {
					curPerson += getToken(i-1)+" ";
				}
				curPerson += getToken(i)+" ";
			} else {
				if (!curPerson.equals("")) {
					break;
				}
			}
			if (!curPerson.equals("") && (i == getLength()-1 || !getNer(i+1).equals("PERSON"))) {
				people.add(curPerson.trim());
				curPerson = "";
			}
		}
		return people;
	}
	
	/**** underspecified ****/
	public HashSet<Integer> underspecifiedNounsDate() {
		HashSet<Integer> underspecifiedNouns = new HashSet<Integer>();
		for (int i = 1; i < posList.size(); i++) {
			if (underspecifiedDate(i)) {
				underspecifiedNouns.add(i);
			}
		}
		return underspecifiedNouns;
	}

	public boolean underspecifiedDate(int index) {
		if (index > posList.size()-1 || index == 0) {
			return false;
		}

		String pos = posList.get(index);
		String lemma = lemmaList.get(index);
		String ner = nerList.get(index);
		
		int prevIndex = index-1;
		while (prevIndex > 0 && (posList.get(prevIndex).startsWith("NN") || posList.get(prevIndex).startsWith("JJ"))) {
			prevIndex--;
		}
		String prevLemma = lemmaList.get(prevIndex);
		String prevNer = nerList.get(prevIndex);
		
		int nextIndex = index+1;
		if (nextIndex < getLength() && (posList.get(nextIndex).equals("NNS") || posList.get(nextIndex).equals("NN"))) {
			return false;
		}
		
		if ((pos.equals("NN") || pos.equals("NNS")) && !ner.equals("DATE") && 
				(prevNer.equals("DATE") || (prevLemma.equals("'s") && prevIndex > 0 && nerList.get(prevIndex-1).equals("DATE")))) {
			if (!lemma.equals("morning") && !lemma.equals("evening") && !lemma.equals("afternoon") && !lemma.equals("night")) {
				if (prevIndex > 0 && prevLemma.equals("'s")) {
					prevLemma = tokenList.get(prevIndex-1);
				}
				if (!prevLemma.equals("today") && !prevLemma.equals("now") && 
					!prevLemma.equals("past") && !prevLemma.equals("future")) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public HashSet<Integer> underspecifiedNounsStrict() {
		HashSet<Integer> underspecifiedNouns = new HashSet<Integer>();
		for (int i = 0; i < posList.size(); i++) {
			if (underspecifiedStrict(i)) {
				underspecifiedNouns.add(i);
			}
		}
		return underspecifiedNouns;
	}

	public boolean underspecifiedVeryStrict(int index) {
		if (!underspecifiedStrict(index)) {
			return false;
		}
		boolean underspecified = false;
		if (Parameters.NON_AMBIGUOUS.contains(lemmaList.get(index))) {
			underspecified = false;
		} else {
			if (index < getLength()-1) {
				String nextPos = posList.get(index+1);
				String nextToken = tokenList.get(index+1);
				if (nextPos.equals("VBZ") || nextPos.equals("VBP") || nextPos.equals("VBN") || 
					nextPos.equals("VBD") || nextPos.equals("POS") || nextPos.equals("MD") ||
					(nextPos.equals("IN") && (nextToken.equals("by") || nextToken.equals("with") || 
					nextToken.equals("than") || nextToken.equals("until")))) {
					underspecified = true;
				}
			}
		}
		return underspecified;
	}
	
	public boolean underspecifiedStrict(int index) {
		if (index > posList.size()-2 || getNer(index).equals("DATE") || getNer(index).equals("DURATION")) {
			return false;
		}
		String pos = posList.get(index);
		String lemma = lemmaList.get(index);
		// example - *This* was the best situation possible.
		// example - He never knew about *that*.
		if (lemma.equals("this") || lemma.equals("these") || lemma.equals("that") || lemma.equals("other")) {
			if (index > 0 && posList.get(index-1).startsWith("N")) {
				return false;
			}
			if (posList.get(index+1).startsWith("V") || posList.get(index+1).startsWith("TO") || posList.get(index+1).length() == 1) {
				if (lemma.equals("other")) {
					for (int i = 0;i < index; i++) {
						if (posList.get(i).startsWith("N")) {
							return false;
						}
					}
				}
				return true;
			} else {
				return false;
			}
		}
		// These were for the previous case, if it didn't pass, then we return.
		if (index == 0 || getSentenceId() == 0) {
			return false;
		}
		
		int nextIndex = index+1;
		String nextPos = posList.get(nextIndex);
		String nextLemma = lemmaList.get(nextIndex);

		int prevIndex = index-1;
		String prevLemma = "";
		while(prevIndex > -1 && (posList.get(prevIndex).equals("NNS") || posList.get(prevIndex).equals("NN"))) {
			prevIndex--;
		}
		if (prevIndex > -1) {
			prevLemma = lemmaList.get(prevIndex);
		}
		
		// Now looking only for nouns.
		if (!(pos.equals("NN") || pos.equals("NNS")) || nextPos.startsWith("NN")) {
			return false;
		}
		
		boolean possible = false;
		
		// example - The *bombing* occurred last Friday.
		if (prevLemma.equals("the") || prevLemma.equals("this") || prevLemma.equals("those") || prevLemma.equals("these")) {
			possible = true;
			
		// example - The first *bombing* occurred last Friday.
		// example - The two *men* were seen running from the scene.
		} else if ((prevLemma.equals("first") || prevLemma.equals("second") || prevLemma.equals("third") || prevLemma.equals("two") || prevLemma.equals("three")) 
				&& prevIndex > 0 && lemmaList.get(prevIndex-1).equals("the")) {
			possible = true;
		}
		
		if (possible) {
			if (!nextPos.equals("PP") && !nextLemma.equals("of") && !nextPos.equals("TO") && !nextLemma.equals("which") && !nextLemma.equals("that")) {
				// Check if the same word appears earlier in the sentence
				for (int i = index-1; i > -1; i--) {
					if (lemmaList.get(i).equals(lemma)) {
						return false;
					}
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	public HashSet<Integer> underspecifiedNounsLoose(ActionResponseGenerator actionResponseMapping, boolean debug) {
		HashSet<Integer> underspecifiedNouns = new HashSet<Integer>();
		for (int i = 1; i < posList.size(); i++) {

			if (underspecifiedLoose(actionResponseMapping, i, debug)) {
				underspecifiedNouns.add(i);
			}
			else if (underspecifiedDate(i)) {
				underspecifiedNouns.add(i);
			} 
		}
		return underspecifiedNouns;
	}

	public boolean underspecifiedLoose(ActionResponseGenerator actionResponseMapping, int index, boolean debug) {
		if (index > posList.size()-1 || index == 0 || getNer(index).equals("DATE") || getNer(index).equals("DURATION")) {
			return false;
		}
		if (!endOfNounPhrase(index)) {
			return false;
		}
		String pos = posList.get(index);
		int prevIndex = index-1;
		String prevLemma = lemmaList.get(prevIndex);
		String prevPos = posList.get(prevIndex);
		while (prevIndex > 0 && (prevPos.startsWith("N") || prevPos.equals("CD") || prevLemma.equals("of") || //|| prevPos.startsWith("J") 
				(prevPos.equals("VBN") && lemmaList.get(prevIndex-1).equals("the")))) {
			prevIndex--;
			prevLemma = lemmaList.get(prevIndex);
			prevPos = posList.get(prevIndex);
			if (prevPos.startsWith("NNP")) {
				break;
			}
		}
		if ((pos.equals("NN") || pos.equals("NNS")) && (prevPos.startsWith("JJ") || getSentenceId() == 0)) {
			return true;
		}
		if ((pos.equals("NN") || pos.equals("NNS")) && (prevLemma.equals("the") || prevLemma.equals("this") || prevLemma.equals("those") || prevPos.startsWith("NNP"))) {
			return true;
		}
		return false;
	}
	
	public boolean underspecifiedVeryLoose(int index) {
		String pos = posList.get(index);
		
		if ((pos.equals("NN") || pos.equals("NNS")) && Compressor.withinClause(this, index, false)) {
			return true;
		}
		return false;
	}
	
	public boolean dateUnderspecified(int prevIndex, int index, String pos, String token) {
		if ((posList.get(index).startsWith("N") && prevIndex >= 1 && lemmaList.get(index-2).equals("the") && nerList.get(prevIndex).equals("DATE")) || 
				(posList.get(index).startsWith("N") && prevIndex >= 2 && lemmaList.get(index-3).equals("the") && nerList.get(prevIndex).equals("DATE")  && nerList.get(prevIndex-1).equals("DATE"))) {
			return true;
		}
		return false;
	}
	
	public boolean determinantUnderspecified(int prevIndex, int index, String pos, String token) {
		if (getSentenceId() == 0) {
			return false;
		}
		String prevLemma = lemmaList.get(prevIndex);
		if ((prevLemma.equals("the") || prevLemma.equals("other") || prevLemma.equals("such") || prevLemma.equals("these") || prevLemma.equals("those") || (prevLemma.equals("that") && pos.startsWith("N"))) && 
				(!(pos.startsWith("JJ") || pos.equals("CD") || pos.equals("''") || pos.startsWith("V") || pos.startsWith("NNP") || pos.startsWith("RB") || token.matches(".*\\d.*")) &&
				(prevIndex >= posList.size()-2 || !(posList.get(index+1).equals("IN") || posList.get(index+1).equals("VBN") || posList.get(index+1).equals("PRP") || posList.get(index+1).startsWith("NN"))) &&
				(prevIndex >= posList.size()-4 || !(posList.get(index+1).equals("CC") && posList.get(index+2).startsWith("NN") && posList.get(index+3).equals("IN"))) ) ||
				(prevIndex > 0 && posList.get(prevIndex).equals("POS") && Parameters.TIMES.contains(tokenList.get(prevIndex-1)))){
			return true;
		}
		return false;
	}
	
	public boolean possessiveUnderspecified(int prevIndex, int index, String pos, String token) {
		if (prevIndex >= 1 && posList.get(prevIndex).equals("POS") && (nerList.get(prevIndex-1).equals("DATE"))) {// || posList.get(prevIndex-1).startsWith("NNP"))) {
			return true;
		}
		return false;
	}
	
	/**** extraction information ****/
	public void addExtraction(Extraction extraction) {
		extractions.add(extraction);
	}
	
	public List<Extraction> getExtractions() {
		return extractions;
	}
	
	/**** coreference information ****/
	public void addCoreference(Coreference coreference) {
		if (!coreferences.contains(coreference)) {
			if (sentenceId == 0) {
				for (Mention mention : coreference.getMentions()) {
					if (mention.getSentenceId() == getSentenceId()) {
						if (mention.getStart() > 0) {
							mention.setStart(mention.getStart()-1);
						}
						mention.setEnd(mention.getEnd()-1);
					}
				}
			}
			coreferences.add(coreference);
		}
	}
	
	public ArrayList<Coreference> getCoreferences() {
		return coreferences;
	}

	/**** other ****/
	public boolean definitivelyAfter(Sentence sentence2) {
		List<Pair<Integer, Calendar>> sentenceDates2 = sentence2.getSentenceDates();
		for (Pair<Integer, Calendar> pair1 : sentenceDates) {
			Calendar date1 = pair1.getSecond();
			for (Pair<Integer, Calendar> pair2 : sentenceDates2) {
				Calendar date2 = pair2.getSecond();
				if (!date1.after(date2)) {
					return false;
				}
			}
		}
		if (!articleDate.after(sentence2.getArticleDate())) {
			return false;
		}
		return true;
	}
	
	public boolean before(Sentence sentence2, boolean debug) {
		List<Pair<Integer, Calendar>> sentenceDates2 = sentence2.getSentenceDates();
		for (Pair<Integer, Calendar> pair1 : sentenceDates) {
			Calendar date1 = pair1.getSecond();
			for (Pair<Integer, Calendar> pair2 : sentenceDates2) {
				Calendar date2 = pair2.getSecond();
				if (!date1.before(date2)) {
					return false;
				}
			}
		}
		if (!articleDate.before(sentence2.getArticleDate())) {
			return false;
		}
		return true;
	}

	public String transitionSentence(boolean debug) {
		for (String transition : Parameters.TRANSITONS) {
			if (Utilities.containsString(getSentenceStr().toLowerCase(),transition)) {
				if ((!transition.equals("in addition") || !Utilities.containsString(getSentenceStr().toLowerCase(),"in addition to")) &&
					(!transition.equals("instead") || !Utilities.containsString(getSentenceStr().toLowerCase(),"instead of")) &&
					(!transition.equals("another") || !Utilities.containsString(getSentenceStr().toLowerCase(),"one another")) &&
					(!transition.equals("as well") || !Utilities.containsString(getSentenceStr().toLowerCase(),"as well as"))) {
					if (transition.equals("again") || transition.equals("also") || transition.equals("then") || 
							transition.equals("later") || transition.equals("nonetheless") || transition.equals("however") || 
							transition.equals("moreover")) {
						if (ParseTreeHandler.beforeConj(getParseTree(), transition, debug)) {
							return transition;
						}
					} else {
						return transition;
					}
				}
			}
			
		}
		for (String transition : Parameters.TRANSITONS_START) {
			if (getSentenceStr().toLowerCase().startsWith(transition+" ")) {
				return transition;
			}
		}
		return "";
	}

	public ArrayList<Integer> unknownPerson() {
		boolean person = false;
		ArrayList<Integer> pronouns = new ArrayList<Integer>();
		for (int i = 0; i < lemmaList.size(); i++) {
			String lemma = lemmaList.get(i);
			String ner = nerList.get(i);
			if (lemma.equals("they") || lemma.equals("them") || lemma.equals("their") || lemma.equals("he") || lemma.equals("his") || lemma.equals("him") || lemma.equals("she") || lemma.equals("her") || lemma.equals("hers")) {
				pronouns.add(i);
			} else if (ner.equals("PERSON")) {
				person = true;
			}
		}
		if (pronouns.size() > 0 && person) {
			return new ArrayList<Integer>();
		}
		ArrayList<Integer> unknownPronouns = new ArrayList<Integer>();
		if (pronouns.size() > 0) {
			for (int pronounLoc : pronouns) {
				boolean locatedThis = false;
				for (Coreference coreference : coreferences) {
					if (coreference.contains(getSentenceId(), pronounLoc, pronounLoc+1) && coreference.matchingNonPronoun(getSentenceId(), pronounLoc, pronounLoc+1)) {
						locatedThis = true;
						break;
					}
				}
				if (!locatedThis) {
					unknownPronouns.add(pronounLoc);
				}
			}
		}
		return unknownPronouns;
	}
	
	public boolean containsPhrase(String name) {
		if (Utilities.stringContains(tokenList, name)) {
			return true;
		}
		return false;
	}
	
	public ArrayList<Integer> pronounsNotInSentence() {
		ArrayList<Integer> pronouns = new ArrayList<Integer>();
		ArrayList<Integer> persons = new ArrayList<Integer>();
		ArrayList<Integer> unfoundPronouns = new ArrayList<Integer>();
		if (getSentenceId() < 2) {
			return unfoundPronouns;
		}
		for (int i = 0; i < lemmaList.size(); i++) {
			String lemma = lemmaList.get(i);
			String ner = nerList.get(i);
			if (lemma.equals("he") || lemma.equals("his") || lemma.equals("him") || lemma.equals("she") || lemma.equals("her") || lemma.equals("hers")) {
				pronouns.add(i);
			} else if (ner.equals("PERSON")) {
				persons.add(i);
			}
			if (lemma.equals("they") || lemma.equals("them") || lemma.equals("their")) {
				boolean okay = false;
				List<Coreference> corefs = getCoreferences(i, i);
				for (Coreference coref : corefs) {
					List<Mention> mentions = coref.getMention(this);
					for (Mention mention : mentions) {
						if (mention.getSentence() == this) {
							if (!mention.getTokens().toLowerCase().equals("they") && !mention.getTokens().toLowerCase().equals("their") && !mention.getTokens().toLowerCase().equals("them")) {
								okay = true;
								break;
							}
						}
					}
				}
				if (!okay) {
					unfoundPronouns.add(i);
				}
			}
		}
		if (unfoundPronouns.size() > 0) {
			return unfoundPronouns;
		}

		if (pronouns.size() > 0 && persons.size() > 0) {
			for (int pronoun : pronouns) {
				boolean located = false;
				List<Coreference> coreferences = getCoreferences(pronouns.get(0), pronouns.get(0));
				for (Coreference coreference : coreferences) {
					for (Mention mention : coreference.getMention(this)) {
						if ((mention.getStart() > pronoun || mention.getEnd() <= pronoun) && !getPos(mention.getStart()).startsWith("PRP")) {
							located = true;
							break;
						}
					}
				}
				if (!located) {
					unfoundPronouns.add(pronoun);
				}
			}
			if (unfoundPronouns.size() > 0 ) {
				if (unfoundPronouns.get(0)==0) {
					return unfoundPronouns;
				} else {
					boolean personFirst = false;
					boolean commaBetween = false;
					for (int person : persons) {
						if (person < unfoundPronouns.get(0)) {
							personFirst = true;
							break;
						} else {
							for (int i = unfoundPronouns.get(0)+1; i < person; i++) {
								if (getToken(i).equals(",")) {
									commaBetween = true;
								}
							}
						}
					}
					String prevPos = getPos(unfoundPronouns.get(0)-1);
					if ((prevPos.equals("IN") || prevPos.startsWith("W") || prevPos.equals("VBG")) && !personFirst && commaBetween) {
						return new ArrayList<Integer>(); 
					} else {
						return unfoundPronouns;
					}
				}
			} else {
				return new ArrayList<Integer>();
			}
		}
		return pronouns;
	}
	
	public List<Coreference> getCoreferences(int start, int end) {
		ArrayList<Coreference> relevantCoreferences = new ArrayList<Coreference>();
		for (Coreference coreference : getCoreferences()) {
			for (Mention mention : coreference.getMention(this)) {
				if (mention.getStart() == start && mention.getEnd() == end) {
					relevantCoreferences.add(coreference);
					break;
				}
			}
		}
		return relevantCoreferences;
	}

	public boolean similar(String sentence2, double similarPercent) {
		if (equals(sentence2)) {
			return true;
		}
		String tempSentence1 = Utilities.join(tokenList, " ").trim();
		String[] tokenArray1 = tempSentence1.split(" ");
		String[] tokenArray2 = sentence2.split(" ");

		if (overlap(tokenArray1, tokenArray2)>=similarPercent && overlap(tokenArray2, tokenArray1)>=similarPercent) {
			return true;
		}
		return false;
	}
	
	public double overlap(String[] tokens1, String[] tokens2) {
		int overlap = 0;
		int total = 0;
		for (int i = 0; i < tokens1.length; i++) {
			String token1 = tokens1[i].replaceAll(",","").replaceAll("\\(","").replaceAll("\\)","").replaceAll("\\.", "").replaceAll("-LRB-","").replaceAll("-RRB-","").trim();
			if (!token1.equals("")) {
				for (int j = 0; j < tokens2.length; j++) {
					String token2 = tokens2[j].replaceAll(",","").replaceAll("\\(","").replaceAll("\\)","").replaceAll("\\.", "").replaceAll("-LRB-","").replaceAll("-RBR-","").trim();
					if (token1.equals(token2)) {
						overlap++;
						break;
					}
				}
				total++;
			}
		}
		return overlap/(total+0.0);
	}

	public boolean containsDate() {
		for (String lemma : lemmaList) {
			lemma = lemma.toLowerCase();
			if (lemma.equals("monday") || lemma.equals("tuesday") || lemma.equals("wednesday") || lemma.equals("thursday") || lemma.equals("friday") || lemma.equals("saturday") || lemma.equals("sunday")) {
				return true;
			}
		}
		return false;
	}

	public void setArticleDate(Calendar articleDate) {
		this.articleDate = articleDate;
	}
	
	public void setSentenceDate(Pair<Integer,Calendar> date) {
		this.sentenceDate = date;
	}
	
	private static String recursiveRootVerb(Tree tree, int level) {
		for (Tree child : tree.getChildrenAsList()) {
			recursiveRootVerb(child, level+1);
		}
		return "";
	}
	
	public String getRootVerb() {
		if (parseTree == null) {
			return "";
		}
		recursiveRootVerb(parseTree, 0);
		return "";
	}
	
	private static boolean labelMatch(Tree tree, String label) {
		if (tree.label().value().equals(label)) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean getSentenceDateSub(int index, String string, boolean debug) {
		if (parseTree == null) {
			return true;
		}
		if (parseTree.getLeaves().get(0).label().value().matches("\\d+-\\d+-\\d+")) {
			index++;
		}
		Tree tree = parseTree.getLeaves().get(index);
		List<Tree> path = parseTree.dominationPath(tree);
		boolean containsSbar = false;
		boolean inNp = false;
		boolean inVp = false;
		boolean advpClause = false;
		int npGroups = 0;
		int vpGroups = 0;
		for (int i = path.size()-1; i > -1; i--) {
			Tree pathTree = path.get(i);
			String label = pathTree.label().value();
			if (label.startsWith("NP")) {
				if (!inNp) {
					npGroups++;
				}
				inNp = true;
			} else {
				inNp = false;
			}
			if (label.startsWith("VP")) {
				if (!inVp) {
					vpGroups++;
				}
				inVp = true;
			} else {
				inVp = false;
			}
			if (label.equals("SBAR")) {
				containsSbar = true;
			} else if (label.equals("ADVP") && pathTree.getLeaves().get(0).label().value().equals("since")) {
				advpClause = true;
			} 
		}
		if (npGroups > 1 && (!containsSbar && !advpClause)) {
			if (path.size() == 7 && labelMatch(path.get(0), "ROOT") && 
					labelMatch(path.get(1), "S") && 
					labelMatch(path.get(2), "NP") && 
					labelMatch(path.get(3), "PP") && 
					labelMatch(path.get(4), "NP")) {
				return true;
			}
			if (path.size() == 8 && labelMatch(path.get(0), "ROOT") && 
					labelMatch(path.get(1), "S") && 
					labelMatch(path.get(2), "PP") && 
					labelMatch(path.get(3), "NP") && 
					labelMatch(path.get(4), "VP") && 
					labelMatch(path.get(5), "NP-TMP")) {
				return true;
			}
			if (path.size() == 10 && labelMatch(path.get(0), "ROOT") && 
					labelMatch(path.get(1), "S") && 
					labelMatch(path.get(2), "VP") && 
					labelMatch(path.get(3), "VP") && 
					labelMatch(path.get(4), "PP") && 
					labelMatch(path.get(5), "NP") &&
					labelMatch(path.get(6), "PP") && 
					labelMatch(path.get(7), "NP") ) {
				return true;
			}
			
		}
	
		if (containsSbar || npGroups > 2 || advpClause || (npGroups == 2 && vpGroups > 1)) {
			return false;
		} else {
			Tree vpTree = getVP(tree);
			if (vpTree != null) {
				String firstLabel = vpTree.getLeaves().get(0).label().value();
				// If it's future tense, we return false: to, will, begins, is
				if (firstLabel.equals("to") || firstLabel.equals("will") || firstLabel.equals("begins") || firstLabel.equals("is")) {
					return false;
				}
			}
			return true;
		}
	}
	
	private Tree getVP(Tree tree) {
		int i = 1;
		while (i < parseTree.depth(tree)) {
			Tree ancestor = tree.ancestor(i, parseTree);
			if (ancestor.label().value().startsWith("V")) {
				if (i < parseTree.depth(tree) && tree.ancestor(i+1, parseTree).label().value().startsWith("V")) {
				} else {
					return ancestor;
				}
			} else if (ancestor.label().value().equals("S")) {
				for (Tree child : ancestor.getChildrenAsList()) {
					if (child.label().value().startsWith("V")) {
						return child;
					}
				}
			}
			i++;
		}
		return null;
	}
	
	public Pair<Integer,Calendar> getRoundedSentenceDate() {
		if (roundedSentenceDate != null) {
			return roundedSentenceDate;
		}
		Calendar date = new GregorianCalendar(getSentenceDate().getSecond().get(Calendar.YEAR),
				getSentenceDate().getSecond().get(Calendar.MONTH),
				getSentenceDate().getSecond().get(Calendar.DAY_OF_MONTH),0,0,0);
		roundedSentenceDate = new Pair<Integer,Calendar>(getSentenceDate().getFirst(),date);
		return roundedSentenceDate;
	}
	
	public Calendar getRoundedArticleDate() {
		Calendar date = new GregorianCalendar(getArticleDate().get(Calendar.YEAR),
				getArticleDate().get(Calendar.MONTH),
				getArticleDate().get(Calendar.DAY_OF_MONTH),0,0,0);
		return date;
	}
	
	private boolean validSentenceDate(String dateStr, int dateIndex, boolean debug) {
		// Get the previous pos, the previous string
		String prevPos = "";
		String prevLemma = "";
		int prevIndex = dateIndex-1;
		while (prevIndex > -1 && getNer(prevIndex).equals("DATE")) {
			prevIndex--;
		}
		if (prevIndex > -1) {
			prevPos = getPos(prevIndex);
			prevLemma = getLemma(prevIndex);
		}
		
		// Get the the next lemma
		String nextLemma = "";
		if (dateIndex < getLength()-1) {
			nextLemma = getLemma(dateIndex+1);
		}
		if ((isDefinitiveDate(dateStr) || isAmbiguousDate(dateStr)) && !nextLemma.equals("'s") &&
				(prevPos.equals("") ||  prevPos.equals("JJ") || prevPos.equals("NN") || 
				prevPos.equals("NNS") || prevPos.equals("NNP") || prevPos.equals("NNPS") ||
				prevPos.equals("PRP") || prevPos.equals("RB") || prevPos.equals("RP") || 
				prevPos.equals("VBD") || prevPos.equals("VBG") || prevPos.equals("VBN") || prevPos.equals("EX") ||
				(prevPos.equals("IN") && (prevLemma.equals("of") || prevLemma.equals("in") || prevLemma.equals("on"))))) {
			if (prevPos.equals("VBN") && prevIndex > 0 && getLemma(prevIndex-1).equals("the")) {
				return false;
			}
			if (getSentenceDateSub(dateIndex, dateStr, debug)) {
				if (isAmbiguousDate(dateStr)) {
					setValid(false);
				}
				return true;
			}
		} 
		return false;
	}
	
	public Pair<Integer,Calendar> getSentenceDate() {
		if (sentenceDate != null) {
			return sentenceDate;
		}
		ArrayList<Integer> checkedIndices = new ArrayList<Integer>();
		if (getSentenceDates() != null && getSentenceDates().size() > 0) {
			for (int i = getSentenceDates().size()-1; i > -1; i--) {
				Pair<Integer, Calendar> pair =  getSentenceDates().get(i);
				Calendar date = pair.getSecond();
				if (date.before(getArticleDateOrig())) {
					int dateIndex = pair.getFirst();
					checkedIndices.add(dateIndex);
					
					// Get the full date string
					String dateStr = getLemma(dateIndex);
					int last = i;
					for (int j = i-1; j > -1; j--) {
						if (getSentenceDates().get(j).getSecond().getTimeInMillis() == date.getTimeInMillis()) {
							int tempIndex = getSentenceDates().get(j).getFirst();
							checkedIndices.add(tempIndex);
							dateStr = getLemma(tempIndex)+" "+dateStr;
							last = j;
						} else {
							break;
						}
					}
					i = last;

					// Check if it's a valid sentence date
					if (validSentenceDate(dateStr, dateIndex, false)) {
						sentenceDate = new Pair<Integer,Calendar>(dateIndex,date);
						return sentenceDate;
					}
				}
			}
		} 
		for (int i = 0; i < getLength(); i++) {
			if (getNer(i).contains("DATE") && !checkedIndices.contains(i)) {
				int dateIndex = i;
				
				// Get the date string
				int last = i;
				String dateStr = getLemma(i);
				for (int j = i+1; j < getLength(); j++) {
					if (getNer(j).contains("DATE") && !checkedIndices.contains(j)) {
						dateStr += " "+getLemma(j);
						last = j;
					} else {
						break;
					}
				}
				i = last;
				
				// Check if it's a valid sentence date
				if (validSentenceDate(dateStr, dateIndex, false)) {
					sentenceDate = new Pair<Integer,Calendar>(dateIndex,Utilities.getDate(getArticleDateOrig(), dateStr));
					if (sentenceDate.getSecond() == null) {
						valid = false;
					} else {
						return sentenceDate;
					}
				}
			}
		}
		sentenceDate = new Pair<Integer,Calendar>(-1,getArticleDateOrig());
		return sentenceDate;
	}
		
	public static boolean isAmbiguousDate(String date) {
		if (date.matches("199\\d") || date.matches("200\\d") || date.matches("201\\d")) {
			return true;
		}
		return false;
	}
	
	public static boolean isDefinitiveDate(String date) {
		String[] week = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
		for (String weekStr : week) {
			if (date.contains(weekStr)) {
				return true;
			}
		}
		String[] month = {"jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"};
		for (String monthStr : month) {
			if (date.toLowerCase().startsWith(monthStr)) {
				return true;
			}
		}
		String[] other = {"yesterday", "decade", "the day", "week", "year", "month"};
		for (String otherStr : other) {
			if (date.contains(otherStr)) {
				return true;
			}
		}
		return false;
	}

	public List<String> getTokenListNoNums() {
		if (tokenListNoNums != null) {
			return tokenListNoNums;
		}
		ArrayList<String> tokens = new ArrayList<String>();
		for (int i = 0; i < getLength(); i++) {
			if (getPos(i).equals("CD")) {
				tokens.add("CD");
			} else {
				tokens.add(getToken(i));
			}
		}
		return tokens;
	}

	public Tree getParseTree() {
		return parseTree;
	}

	public void setParseTree(Tree parseTree) {
		this.parseTree = parseTree;
	}

	public boolean wasModified() {
		return modified;
	}
	
	public void setModified(boolean modified) {
		this.modified = modified;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	public String getCluster() {
		return cluster;
	}

	public void setDocumentId(String docId) {
		this.docId = docId;
	}

	public void setSentenceId(int sentenceId) {
		this.sentenceId = sentenceId;
	}

	public void setValid(boolean value) {
		this.valid = value;
	}
	
	public boolean getValid() {
		return valid;
	}

	public String posBefore(String noun) {
		int location = getLemmaList().indexOf(noun);
		if (location > 0) {
			int prev = location-1;
			String prevPos = getPos(prev);
			while ((prevPos.equals("NNP") || prevPos.equals("NN") || prevPos.equals("CD")) && prev > 0) {
				prev--;
				prevPos = getPos(prev);
			}
			return prevPos;
		}
		return "";
	}
}
