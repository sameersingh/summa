package edu.washington.cs.knowitall.datastructures;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import edu.washington.cs.knowitall.utilities.Utilities;

public class Document {
	private String docId;
	private String headline;
	private String dateline;
	private Calendar date;
	private String text = "";
	private String type;
	private Set<String> termSet;
	private List<String> termList;
	
	public Document() {
		
	}
	
	public Document(String inputString, boolean debug) {
		init(inputString, debug);
	}
	
	private void init(String inputString, boolean debug) {
		docId = inputString.split("<DOC id=\"")[1].split("\"")[0];
		type = inputString.split(" type=\"")[1].split("\"")[0];
		if (inputString.contains("<DATETIME>")) {
			String dateString = inputString.split("DATETIME>")[1].replaceAll("</", "").trim();
			//2013-5-10 18:40:36
			int year = Integer.parseInt(dateString.split("-")[0]);
			int month = Integer.parseInt(dateString.split("-")[1])-1; //Calendar is 0-indexed for months
			int day = Integer.parseInt(dateString.split("-")[2].split(" ")[0]);
			int hour = Integer.parseInt(dateString.split(":")[0].split(" ")[1]);
			int minutes = Integer.parseInt(dateString.split(":")[1]);
			int seconds = Integer.parseInt(dateString.split(":")[2]);
			date = new GregorianCalendar();
			date.set(year, month, day, hour, minutes, seconds);
		} else {
			date = parseDate(docId);
		}

		if (inputString.contains("HEADLINE> ")) {
			String tempHeadline = inputString.split("HEADLINE> ")[1].replaceAll("</", "").trim();
			if (!docId.startsWith("NYT_ENG") || 
					!(Pattern.matches("[a-z]+.*", tempHeadline) || tempHeadline.endsWith(".") || tempHeadline.endsWith(".''") || tempHeadline.endsWith(")"))) {
				headline = tempHeadline;
			} 
		} if (inputString.contains("DATELINE> ")) {
			dateline = inputString.split("DATELINE> ")[1].replaceAll("</", "").trim();
		}
	
		String tempText = inputString.split("TEXT")[1];
		if (tempText.length() > 10) {
			tempText = tempText.substring(2, tempText.length()-2).replaceAll("<P>", "\n\n").replaceAll("</P>", "").replaceAll("(MORE)","").replaceAll("\\(.*?\\)","").trim();
			
			
			String[] lines = tempText.split("\n");
			String cleanText = "";
			for (String line : lines) {
				line = line.trim();
				if (Pattern.matches("With [0-9A-Z;\\-]+ .*",line) || line.matches("By [A-Z\\. ]*") || line.matches("c.\\d+ Cox News Service") || line.matches("EDs: Optional.*")) {
				} else if (line.matches(".*[\\.\\?:\\!]") || line.matches(".*''") || line.matches(".*'")) {
					cleanText += line+" \n";
				} else if (!line.trim().matches("")){
	
				}
			}		
	
			lines = cleanText.split("\n");
			lines[0] = lines[0].trim();
			if (lines[0].matches("[A-Z]+ ([A-Z]+ )* *[-�]+ *\\w+.*")) {
				lines[0] = lines[0].split(" [-�]+ *",2)[1].trim();
			} else if (lines[0].matches("[A-Z]+ [A-Z]+�[-]+ .*")) {
				lines[0] = lines[0].split("- ",2)[1].trim();
			} else if (lines[0].matches("([A-Z,a-z]*-)?([A-Z,-]+ )*[A-Z,-]+, ([A-Z,a-z,-]+ )*[A-Z,a-z,-]+\\.? +- +.*")) {
				lines[0] = lines[0].split(" - ",2)[1].trim();
			} else if (lines[0].matches("[A-Z]+,?( [A-Z,a-z,\\.]+)?[ ]*_.*")) {
				lines[0] = lines[0].split("_",2)[1].trim();
			} else if (lines[0].matches("[A-Z]+,?( [A-Z,a-z,\\.]+)? +[-�] +.*")) {
				lines[0] = lines[0].split("[-�]",2)[1].trim();
			} else if (lines[0].matches("[A-Z]+[A-Z]+[A-Z]+[A-Z]+[-]+[ ]*[A-Z][a-z]+.*")) {
				lines[0] = lines[0].split("[-]+",2)[1].trim();
			} else if (lines[0].matches("[A-Z]+, [A-Z][a-z]+ \\d+ [ ]?-.*")) {
				lines[0] = lines[0].split("[-]+",2)[1].trim();
			} else if (lines[0].matches("[A-Z]+( [A-Z]+)*, [A-Z][a-z]+ \\d+[,]?  -.*")) {
				lines[0] = lines[0].split("-",2)[1].trim();
			} else if (lines[0].matches("[A-Z]+( [A-Z]+)*, ([A-Z][a-z]+,? )*[A-Z][a-z]+ \\d+[,]?  -.*")) {
				lines[0] = lines[0].split("-",2)[1].trim();
			} else if (docId.startsWith("NYT") && lines[0].matches("[A-Z]+( [A-Z]+)* .*? [A-Z]+ -- [A-Z][a-z].*")) {
				String title = lines[0].split(" [A-Z][a-z]+ ",2)[0].trim();
				headline = title.split("\\(")[0].split("--")[0];
				lines[0] = lines[0].split(" -- ",2)[1].trim();
			} else if (lines[0].matches("(Rome|Vatican City) [-]+ .*")) {
				lines[0] = lines[0].split(" [-]+",2)[1].trim();
			} else if (lines[0].matches("[A-Z]+( [A-Z]+)*, [A-Z][a-z]+ [ ]*-.*")) {
				lines[0] = lines[0].split(" [-]+",2)[1].trim();
			} else if ((headline == null || (!headline.startsWith("WORLD AT") && !headline.startsWith("WORLD SPORTS"))) && lines[0].matches(".*[A-Z]+,?( [A-Z]+)* _ [A-Z][ a-z].*")) {
				lines[0] = lines[0].split("[A-Z]+,?( [A-Z]+)* _ ",2)[1].trim();
			} else if (lines[0].matches(".*[A-Z]+,?( [A-Z]+,?)* ([A-Z][a-z]* )*_ [A-Z][ a-z].*")) {
				lines[0] = lines[0].split(" _ ",2)[1].trim();
			} else if (docId.startsWith("NYT") && !lines[0].startsWith("The following ") && !lines[0].contains("today's top news stories") && 
					!lines[0].contains("stories for Page 1") && lines[0].matches("[A-Z][A-Z]+ .* [A-Z][A-Z]+(, [A-Z][a-z]+)? -[-]? .*")) {
				lines[0] = lines[0].split(" [A-Z][A-Z]+(, [A-Z][a-z]+)? -[-]? ")[1];
			}
		
			if (lines[0].startsWith("_")) {
				lines[0] = lines[0].split("_",2)[1].trim();
			}
			if (lines[0].startsWith("-- ")) {
				lines[0] = lines[0].split("--",2)[1].trim();
			}
			if (lines[0].startsWith("- ")) {
				lines[0] = lines[0].split("-",2)[1].trim();
			}

			if (lines[0].endsWith(":")) {
				lines[0] = lines[0].substring(0, lines[0].length()-1)+".";
			}

			lines[0] += "\n\n";
	
			text = lines[0];
			for (int i = 1; i < lines.length; i++) {
				String line = lines[i].trim();
				if (line.startsWith("This article has been revised")) {
					break;
				}
				if (i == lines.length-1 && (line.contains("contributed reporting") ||
						line.contains("reported from") ||
						line.contains("contributed to this report"))) {
					break;
				}
				if (!line.equals("") && !line.startsWith("ALSO") && !line.toUpperCase().equals(line) && !line.startsWith("PHOTOS")) {
					text += line+"\n\n";
				} 
			}
		} 
	}
	
	public String getRevisedHeadline() {
		String newHeadline = headline;
		if (headline != null && !Pattern.matches(".* [a-z]+ .*", headline)) {
	
			String tempText = text.replaceAll("\n",  " ");
			String[] textTokens = tempText.split(" ");
			String[] titleTokens = headline.split(" ");
			ArrayList<String> newTokens = new ArrayList<String>();
			int i = 0;
			for (String token : titleTokens) {
				HashMap<String,Integer> tokenMap = new HashMap<String,Integer>();
				Pair<String,Pair<String,String>> splitOnPunc = trim(token, false);
				String tokenLower = splitOnPunc.getFirst().toLowerCase();
				String punc1 = splitOnPunc.getSecond().getFirst().toLowerCase();
				String punc2 = splitOnPunc.getSecond().getSecond().toLowerCase();
				for (String textToken : textTokens) {
					textToken = trim(textToken, false).getFirst();
					String textTokenLower = textToken.toLowerCase();
					if (tokenLower.equals(textTokenLower)) {
						if (!tokenMap.containsKey(textToken)) {
							tokenMap.put(textToken,  0);
						}
						tokenMap.put(textToken,tokenMap.get(textToken)+1);
					}
				}
				int largestVote = 0;
				String newToken = tokenLower;
				for (String tempToken : tokenMap.keySet()) {
					if (tokenMap.get(tempToken) > largestVote || 
							(tokenMap.get(tempToken) == largestVote && tempToken.toLowerCase().equals(tempToken))) {
						newToken = tempToken;
						largestVote = tokenMap.get(tempToken);
					}
				}
				newToken = punc1+newToken+punc2;
				if (i == 0 && newToken.length() > 1) {
					newToken = newToken.substring(0,1).toUpperCase()+newToken.substring(1);
				} else if (i == 0 && newToken.length() > 0) {
					newToken = newToken.substring(0,1).toUpperCase();
				}
				newTokens.add(newToken);
				i++;
			}
			newHeadline = Utilities.join(newTokens, " ");
		}
		if (newHeadline != null) {
			newHeadline = newHeadline.split(" =")[0];
			String newHeadline2 = "";
			String[] tokens = newHeadline.split("\\(.*?\\)");
			for (String token : tokens) {
				newHeadline2 += token.trim()+" ";
			}
			newHeadline2 = newHeadline2.trim();
			if (newHeadline2.contains("GRAPHIC")) {
				newHeadline2 = newHeadline2.replaceAll(" \\+ GRAPHIC\\)","");
				newHeadline2 = newHeadline2.replaceAll(" \\+ GRAPHICS\\)","");
			}
			if (getDocId().startsWith("AFP") && Pattern.matches(".* by [A-Z][a-z]+ ([A-Z]\\. )?(M(a)?c)?[A-Z][a-z]+", newHeadline2)) {
				Pattern p = Pattern.compile(" by [A-Z][a-z]+ ([A-Z]\\. )?(M(a)?c)?[A-Z][a-z]+");
				newHeadline2 = p.split(newHeadline2)[0];
			}
			newHeadline = newHeadline2;
			newHeadline = newHeadline.replaceAll(" URGENT ", "");
			newHeadline = newHeadline.replaceAll("URGENT ", "");
			newHeadline = newHeadline.replaceAll(" URGENT", "");
			newHeadline = newHeadline.split(" ATTENTION")[0];
			newHeadline = newHeadline.split(" Eds:")[0];
			newHeadline = newHeadline.split(" EDS:")[0];
			newHeadline = newHeadline.split(" EDs:")[0];
			newHeadline = newHeadline.split(" AP Photo")[0];
			if (newHeadline.matches(".* B[Yy] [A-Z]+.*")) {
				newHeadline = newHeadline.split(" B[Yy] ",2)[0];
			}
			// Precede BAGHDAD
			if (newHeadline.matches("Precede [A-Z]+ .*")) {
				newHeadline = newHeadline.split("Precede [A-Z]+ ",2)[1];
			}
			// With BC-Libya-Lockerbie
			if (getDocId().startsWith("APW") && newHeadline.matches(".* With [A-Z,a-z]+([- ][A-Z,a-z]+)*")) {
				newHeadline = newHeadline.split(" With [A-Z,a-z]+",2)[0];
			}
			if (getDocId().startsWith("APW") && newHeadline.matches(".* With")) {
				newHeadline = newHeadline.split(" With",2)[0];
			}
			newHeadline = newHeadline.trim();
		}
		return newHeadline;
	}
	private static Pair<String,Pair<String,String>> trim(String token, boolean debug) {
		String punc1 = "";
		String punc2 = "";
		String newToken = token;

		if (Pattern.matches("[`].*", token)) {
			punc1 = token.substring(0,1);
			newToken = token.substring(1);
		}
		
		if (Pattern.matches(".*[\\:]", token)) {
			punc2 = token.substring(token.length()-1);
			newToken = token.substring(0,token.length()-1);
		}

		if (Pattern.matches(".*''", newToken) || Pattern.matches(".*'S", newToken)) {
			punc2 = newToken.substring(newToken.length()-2)+punc2;
			newToken = newToken.substring(0,newToken.length()-2);
		} else if (Pattern.matches(".*[\\.,\\?\\!']", newToken)) {
			punc2 = newToken.substring(newToken.length()-1)+punc2;
			newToken = newToken.substring(0,newToken.length()-1);
		}

		return new Pair<String,Pair<String,String>>(newToken,new Pair<String,String>(punc1,punc2));
	}
	
	public static Calendar parseDate(String docId) {
		Calendar date = new GregorianCalendar();
		int year = Integer.parseInt(docId.substring(8, 12));
		int month = Integer.parseInt(docId.substring(12, 14))-1;
		int day = Integer.parseInt(docId.substring(14, 16));
		date.set(Calendar.YEAR, year);
		date.set(Calendar.MONTH, month);
		date.set(Calendar.DAY_OF_MONTH, day);
		date.set(Calendar.HOUR, 12);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		return date;
	}
	
	public String getDocId() {
		return docId;
	}
	
	public String getType() {
		return type;
	}

	public String getHeadline() {
		return headline;
	}

	public String getDateline() {
		return dateline;
	}

	public Calendar getDate() {
		return date;
	}

	public String getText() {
		return text;
	}
	
	public Set<String> getTermSet() {
		if (termSet == null) {
			termSet = new HashSet<String>();
			String[] elements = text.split(" ");
			for (String element : elements) {
				element = element.replaceAll(",","").replaceAll("\\.","").replaceAll("\\?","").replaceAll("\\!","");
				element = element.replaceAll("`","").replaceAll("'","").replaceAll("-","").replaceAll("_","");
				element = element.replaceAll("\\(","").replaceAll("\\)","");
				element = element.replaceAll(":","").replaceAll(";","");
				element = element.toLowerCase().trim();
				if (!element.equals("") && !element.contains("$") && !Pattern.matches(".*\\d.*",element)) {
					termSet.add(element);
				}
			}
		}
		return termSet;
	}
	
	public List<String> getTermList() {
		if (termList == null) {
			termList = new ArrayList<String>();
			String title = getRevisedHeadline();
			if (title != null) {
				String[] titleElements = getRevisedHeadline().toLowerCase().split(" ");
				for (String element : titleElements) {
					element = element.replaceAll(",","").replaceAll("\\.","").replaceAll("\\?","").replaceAll("\\!","");
					element = element.replaceAll("`","").replaceAll("'","").replaceAll("-","").replaceAll("_","");
					element = element.replaceAll("\\(","").replaceAll("\\)","");
					element = element.replaceAll(":","").replaceAll(";","");
					element = element.trim();
					if (!element.equals("") && !element.contains("$") && !Pattern.matches(".*\\d.*",element)) {
						termList.add(element);
						if (termList.size() > 100) {
							break;
						}
					}
				}
			}
			
			String[] elements = text.toLowerCase().split(" ");
			for (String element : elements) {
				element = element.replaceAll(",","").replaceAll("\\.","").replaceAll("\\?","").replaceAll("\\!","");
				element = element.replaceAll("`","").replaceAll("'","").replaceAll("-","").replaceAll("_","");
				element = element.replaceAll("\\(","").replaceAll("\\)","");
				element = element.replaceAll(":","").replaceAll(";","");
				element = element.trim();
				if (!element.equals("") && !element.contains("$") && !Pattern.matches(".*\\d.*",element)) {
					termList.add(element);
					if (termList.size() > 100) {
						break;
					}
				}
			}
		}
		return termList;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public void setDocId(String docId) {
		this.docId = docId;
	}
	
	public void setType(String type) {
		this.type = type;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public void setDateline(String dateline) {
		this.dateline = dateline;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}
	
	public Calendar getRoundedDate() {
		Calendar roundedDate = new GregorianCalendar(date.get(Calendar.YEAR), 
													date.get(Calendar.MONTH), 
													date.get(Calendar.DAY_OF_MONTH), 
													0,
													0,
													0);
		return roundedDate;
	}
}
