package edu.washington.cs.knowitall.datastructures;

import java.util.ArrayList;
import java.util.List;

public class Coreference {
	private String docId;
	private ArrayList<Mention> mentions;
	
	public Coreference(String docId, ArrayList<Mention> mentions) {
		this.setDocId(docId);
		this.setMentions(mentions);
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public ArrayList<Mention> getMentions() {
		return mentions;
	}

	public void setMentions(ArrayList<Mention> mentions) {
		this.mentions = mentions;
	}

	public String toString() {
		String string = "Coreference for: "+docId+"\n";
		for (Mention mention : mentions) {
			string += mention.toString()+"\n";
		}
		return string;
	}
	
	public List<Mention> getMention(Sentence sentence) {
		ArrayList<Mention> inSentence = new ArrayList<Mention>();
		for (Mention mention : mentions) {
			if (sentence.getDocId().equals(mention.getDocId()) && 
					sentence.getSentenceId() == mention.getSentenceId()) {
				inSentence.add(mention);
			}
		}
		return inSentence;
	}
	
	public boolean contains(int sentenceId, int startIndex, int endIndex) {
		for (Mention mention : mentions) {
			if (mention.getSentenceId() == sentenceId && mention.getStart() == startIndex && mention.getEnd() == endIndex) {
				return true;
			}
		}
		return false;
	}
	
	public boolean matchingNonPronoun(int sentenceId, int startIndex, int endIndex) {
		if (!contains(sentenceId, startIndex, endIndex)) {
			return false;
		}
		for (Mention mention : mentions) {
			if (mention.getSentenceId() == sentenceId) {
				Sentence sentence = mention.getSentence();
				if (sentence != null) {
					List<String> posList = sentence.getPosList();
					for (int i = mention.getStart(); i < mention.getEnd(); i++) {
						if (!posList.get(i).startsWith("PRP")) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public boolean equals(Coreference coreference2) {
		if (docId.equals(coreference2.getDocId())) {
			return false;
		}
		for (Mention mention : mentions) {
			for (Mention mention2 : coreference2.getMentions()) {
				if (mention.isPerson() && mention2.isPerson() && 
						!mention.isPronoun() && !mention2.isPronoun() && 
						mention.getTokens().equals(mention2.getTokens())) {
					return true;
				}
			}
		}
		return false;
	}
}
