package edu.washington.cs.knowitall.datastructures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.washington.cs.knowitall.datastructures.SentenceEdge.EDGE_TYPE;
import edu.washington.cs.knowitall.summarization.ActionResponseGenerator;
import edu.washington.cs.knowitall.summarization.Parameters;
import edu.washington.cs.knowitall.summarization.UnderspecifiedClassifier;
import edu.washington.cs.knowitall.utilities.Utilities;

public class CoherenceGraph {
	private HashMap<String,List<SentenceEdge>> edges = new HashMap<String,List<SentenceEdge>>();

	private HashMap<String,HashMap<Sentence,List<SentenceEdge>>> inEdgesPerson = new HashMap<String,HashMap<Sentence,List<SentenceEdge>>>();
	private HashMap<String,HashMap<Sentence,List<SentenceEdge>>> inEdgesNoun = new HashMap<String,HashMap<Sentence,List<SentenceEdge>>>();
	private HashMap<String,HashMap<Sentence,List<SentenceEdge>>> inEdgesNounVerb = new HashMap<String,HashMap<Sentence,List<SentenceEdge>>>();
	private HashMap<String,HashMap<Sentence,List<SentenceEdge>>> inEdgesTransition = new HashMap<String,HashMap<Sentence,List<SentenceEdge>>>();
	private HashMap<String,HashMap<Sentence,List<SentenceEdge>>> inEdgesNecNounVerb = new HashMap<String,HashMap<Sentence,List<SentenceEdge>>>();
	private HashMap<String,List<SentenceEdge>> inEdges = new HashMap<String,List<SentenceEdge>>();
	
	private HashMap<String,HashSet<Sentence>> outSentences = new HashMap<String,HashSet<Sentence>>();
	private HashMap<String,Set<String>> sentenceToNecessaryWordMap = new HashMap<String,Set<String>>();
	
	private UnderspecifiedClassifier underspecifiedClassifier = new UnderspecifiedClassifier();
	private ActionResponseGenerator actionResponseGenerator = new ActionResponseGenerator();
	private static double DEFAULT_WEIGHT = 1.0;
	
	public void setup(HashMap<String, Sentence> sentences) {
		edges = new HashMap<String, List<SentenceEdge>>();
		HashMap<String, ArrayList<Sentence>> sortedSentences = sortByDocument(sentences.values());
		generatePronounEdges(sortedSentences);
		generateDiscourseQueEdges(sortedSentences);
		generateNonspecificPersonEdges(sentences);
		generateNounEdges(sentences.values(), sortedSentences);
		
		inEdges = new HashMap<String,List<SentenceEdge>>();
		for (List<SentenceEdge> curEdges : edges.values()) {
			for (SentenceEdge edge : curEdges) {
				Sentence destSentence = edge.getDestination();
				Sentence sourceSentence = edge.getSource();
				if (!inEdges.containsKey(destSentence.getKey())) {
					inEdges.put(destSentence.getKey(), new ArrayList<SentenceEdge>());
				}
				inEdges.get(destSentence.getKey()).add(edge);

				HashMap<String,HashMap<Sentence,List<SentenceEdge>>> temp = null;
				if (edge.getType() == SentenceEdge.EDGE_TYPE.PERSON) {
					temp = inEdgesPerson;
				} else if (edge.getType() == SentenceEdge.EDGE_TYPE.NOUN) {
					temp = inEdgesNoun;
				} else if (edge.getType() == SentenceEdge.EDGE_TYPE.NOUNVERB) {
					temp = inEdgesNounVerb;
				} else if (edge.getType() == SentenceEdge.EDGE_TYPE.TRANSITION) {
					temp = inEdgesTransition;
				} else if (edge.getType() == SentenceEdge.EDGE_TYPE.NECNOUNVERB) {
					temp = inEdgesNecNounVerb;
				}
				if (temp != null && sourceSentence != null) {
					if (!temp.containsKey(destSentence.getKey())) {
						temp.put(destSentence.getKey(), new HashMap<Sentence,List<SentenceEdge>>());
					}
					if (!temp.get(destSentence.getKey()).containsKey(sourceSentence)) {
						temp.get(destSentence.getKey()).put(sourceSentence, new ArrayList<SentenceEdge>());
					}			
					temp.get(destSentence.getKey()).get(sourceSentence).add(edge);
				} else if (edge != null && sourceSentence == null && edge.getType() == SentenceEdge.EDGE_TYPE.PERSON) {
					sourceSentence = new Sentence();
					
					if (!temp.containsKey(destSentence.getKey())) {
						temp.put(destSentence.getKey(), new HashMap<Sentence,List<SentenceEdge>>());
					}
					if (!temp.get(destSentence.getKey()).containsKey(sourceSentence)) {
						temp.get(destSentence.getKey()).put(sourceSentence, new ArrayList<SentenceEdge>());
					}
					temp.get(destSentence.getKey()).get(sourceSentence).add(edge);
				}
			}
		}
		outSentences = new HashMap<String,HashSet<Sentence>>();
		for (List<SentenceEdge> curEdges : edges.values()) {
			for (SentenceEdge edge : curEdges) {
				Sentence sourceSentence = edge.getSource();
				if (sourceSentence != null) {
					if (!outSentences.containsKey(sourceSentence.getKey())) {
						outSentences.put(sourceSentence.getKey(), new HashSet<Sentence>());
					}
					outSentences.get(sourceSentence.getKey()).add(edge.getDestination());
				}
			}
		}
	}
	
	private void addEdge(Sentence source, 
			Sentence dest,
			String reason,
			EDGE_TYPE type,
			HashSet<String> synonyms) {
		String key = SentenceEdge.getKey(source, dest);
		if (source == null || 
			((source != dest || type == EDGE_TYPE.NECNOUNVERB) && 
			 (!source.getSentenceStr().equals(dest.getSentenceStr()) || type == EDGE_TYPE.NECNOUNVERB))) {
			if (!edges.containsKey(key)) {
				edges.put(key, new ArrayList<SentenceEdge>());
			}
			for (SentenceEdge edge : edges.get(key)) {
				if (edge.getReason().equals(reason) && edge.getType() == type) {
					return;
				}
			}
			edges.get(key).add(new SentenceEdge(source, dest, DEFAULT_WEIGHT, reason, type, synonyms));
		}
	}

	public Set<String> getNecessaryWords(Sentence destSentence, boolean debug) {
		if (!debug && sentenceToNecessaryWordMap.containsKey(destSentence.getKey())) {
			Set<String> tempWords = new HashSet<String>();
			Set<String> words = sentenceToNecessaryWordMap.get(destSentence.getKey());;
			for (String word : words) {
				tempWords.add(word);
			}
			return tempWords;
		}
		HashSet<String> necessaryWords = new HashSet<String>();
		List<SentenceEdge> curInEdges = getInEdges(destSentence);
		List<String> fulfilledReasons = new ArrayList<String>();
		for (SentenceEdge edge : curInEdges) {
			if (edge.getSource()!=destSentence && edge.getType() != EDGE_TYPE.INFERTRANSITION && edge.getType() != EDGE_TYPE.NOUNVERB) {
				necessaryWords.add(edge.getReason());
			} else if (edge.getSource() == destSentence) {
				fulfilledReasons.add(edge.getReason());
			}
		}
		for (String reason : fulfilledReasons) {
			for (String word : necessaryWords) {
				if (word.equals(reason)) {
					necessaryWords.remove(reason);
					break;
				}
			}
		}
		sentenceToNecessaryWordMap.put(destSentence.getKey(), necessaryWords);
		return necessaryWords;
	}
	
	private static HashMap<String, ArrayList<Sentence>> sortByDocument(Collection<Sentence> sentences) {
		HashMap<String, ArrayList<Sentence>> sortedSentences = new HashMap<String, ArrayList<Sentence>>();
		for (Sentence sentence : sentences) {
			String docId = sentence.getDocId();
			if (!sortedSentences.containsKey(docId)) {
				sortedSentences.put(docId, new ArrayList<Sentence>());
			}
			sortedSentences.get(docId).add(sentence);
		}
		for (ArrayList<Sentence> sentenceGroup : sortedSentences.values()) {
			Collections.sort(sentenceGroup, new Comparator<Sentence>(){
				@Override
				public int compare(Sentence sentence1, Sentence sentence2) {
					int sentenceId1 = sentence1.getSentenceId();
		            int sentenceId2 = sentence2.getSentenceId();
					if (sentenceId1 < sentenceId2) {
		            	return -1;
		            } else if (sentenceId1 > sentenceId2) {
		            	return 1;
		            }
		            return 0;
				}
			});
		}
		return sortedSentences;
	}
	
	/********* noun edges ************/
	
	private boolean coreferenceInSentence(Sentence sentence, Pair<Integer,Integer> nounPair) {
		int start = nounPair.getFirst();
		int end = nounPair.getSecond();
		List<Coreference> coreferences = sentence.getCoreferences(start, end);
		for (Coreference coref : coreferences) {
			for (Mention mention : coref.getMentions()) {
				if (mention.getSentenceId() == sentence.getSentenceId() && mention.getStart() < start) {
					return true;
				}
			}
		}
		return false;
	}
	
	private List<Sentence> coreference(Sentence sentence, Pair<Integer,Integer> nounPair) {
		int start = nounPair.getFirst();
		int end = nounPair.getSecond();
		List<Coreference> coreferences = sentence.getCoreferences(start, end);
		List<Sentence> corefSentences = new ArrayList<Sentence>();
		for (Coreference coref : coreferences) {
			for (Mention mention : coref.getMentions()) {
				if (mention.getSentenceId() < sentence.getSentenceId()) {
					corefSentences.add(sentence);
				}
			}
		}
		return corefSentences;
	}
	
	private boolean underspecifiedLoose(Sentence sentence, int index) {
		List<String> posList = sentence.getPosList();
		List<String> lemmaList = sentence.getLemmaList();
		if (index > posList.size()-1 || index == 0 || sentence.getNer(index).equals("DATE") || 
				sentence.getNer(index).equals("DURATION") || !sentence.endOfNounPhrase(index)) {
			return false;
		}
		String pos = posList.get(index);
		int prevIndex = index-1;
		String prevLemma = lemmaList.get(prevIndex);
		String prevPos = posList.get(prevIndex);
		while (prevIndex > 0 && (prevPos.startsWith("N") || prevPos.equals("CD") || prevLemma.equals("of") || 
				(prevPos.equals("VBN") && lemmaList.get(prevIndex-1).equals("the")))) {
			prevIndex--;
			prevLemma = lemmaList.get(prevIndex);
			prevPos = posList.get(prevIndex);
			if (prevPos.startsWith("NNP")) {
				break;
			}
		}
		if ((pos.startsWith("NN") && prevPos.startsWith("JJ")) || 
				(pos.startsWith("NN") && (prevLemma.equals("the") || prevLemma.equals("this") || prevLemma.equals("those") || prevPos.startsWith("NNP")))) {
			return true;
		}
		return false;
	}
	
	private HashSet<String> generateNounSet(Collection<Sentence> sentences) {
		HashSet<String> nounList = new HashSet<String>();
		for (Sentence sentence : sentences) {
			for (Pair<Integer,String> nounPair : sentence.getNouns()) {
				int nounLocation = nounPair.getFirst();
				if (underspecifiedLoose(sentence, nounLocation)) {
					String noun = sentence.getLemma(nounLocation);
					boolean validResponse = ActionResponseGenerator.validResponse(sentence, nounLocation);
					if (validResponse) {
						nounList.add(noun);
					}
				}
			}
		}
		return nounList;
	}
	
	private HashMap<String,ArrayList<Sentence>> generateVerbToSentenceMapping(Collection<Sentence> sentences) {
		HashMap<String,ArrayList<Sentence>> verbsToSentenceMapping = new HashMap<String,ArrayList<Sentence>>();
		for (Sentence sentenceSource : sentences) {
			List<String> verbs = sentenceSource.getVerbs();
			for (String verb : verbs) {
				if (!Parameters.STOP_VERBS.contains(verb) && !sentenceSource.getTokenList().contains("not")) {
					if (!verbsToSentenceMapping.containsKey(verb)) {
						verbsToSentenceMapping.put(verb, new ArrayList<Sentence>());
					}
					verbsToSentenceMapping.get(verb).add(sentenceSource);
				}
			}
		}
		return verbsToSentenceMapping;
	}
	
	private HashMap<String, HashSet<String>> generateNounVerbMapping(Collection<Sentence> sentences, HashMap<String,ArrayList<Sentence>> verbsToSentenceMapping ) {
		HashMap<String, HashSet<String>> nounVerbMap = new HashMap<String, HashSet<String>>();
		HashSet<String> nounSet = generateNounSet(sentences);
		for (String noun : nounSet) {
			for (String verb : verbsToSentenceMapping.keySet()) {
				double tmpConnection = actionResponseGenerator.related(noun, verb);
				if (tmpConnection > 0 || (noun.endsWith("ing") && (noun.substring(0, noun.length()-3).startsWith(verb) || noun.substring(0, noun.length()-4).startsWith(verb)))
						 || (noun.endsWith("er") && (noun.substring(0, noun.length()-2).startsWith(verb) ||  noun.substring(0, noun.length()-3).startsWith(verb))))  {
					if (!nounVerbMap.containsKey(noun)) {
						nounVerbMap.put(noun, new HashSet<String>());
					}
					nounVerbMap.get(noun).add(verb);
				}
			}
		}
		return nounVerbMap;
	}
	
	private List<Sentence> earlierMentions(ArrayList<Sentence> documentSentences, Sentence sentence1, String noun) {
		ArrayList<Sentence> earlierSentences = new ArrayList<Sentence>();
		for (Sentence sentence2 : documentSentences) {
			if (sentence2.getSentenceId() < sentence1.getSentenceId()) {
				if (sentence2.getLemmaList().contains(noun)) {
					earlierSentences.add(sentence2);
				}
			}
		}
		return earlierSentences;
	}
	
	private boolean shouldLinkToPreviousSentence(ArrayList<Sentence> documentSentences, Sentence destSentence, int underspecifiedIndex) {
		boolean proceededBySpecifier = UnderspecifiedClassifier.proceededBySpecifier(destSentence, underspecifiedIndex);
		// Subject
		boolean subject = underspecifiedClassifier.isSubject(destSentence, underspecifiedIndex);
		// Date possessive
		boolean datePos = underspecifiedClassifier.datePossessive(destSentence, underspecifiedIndex);
		// Within clause
		boolean withinClauseStrict = underspecifiedClassifier.withinClauseStrict(destSentence, underspecifiedIndex);
		if (!withinClauseStrict && !datePos && proceededBySpecifier && documentSentences.indexOf(destSentence) > 0) {
			if (subject) {
				// choose the previous sentence
				return true;
			} else if (!subject) {
				boolean thatNext = false;
				if (destSentence.getPos(underspecifiedIndex+1).equals("WDT") || 
						destSentence.getLemma(underspecifiedIndex+1).equals("that") ||
						destSentence.getLemma(underspecifiedIndex+1).equals("which") ||
						destSentence.getLemma(underspecifiedIndex+1).equals("who")) thatNext = true;
				boolean ppNext = false;
				if (destSentence.getLemma(underspecifiedIndex+1).equals("on") || 
						destSentence.getLemma(underspecifiedIndex+1).equals("against") || 
						destSentence.getLemma(underspecifiedIndex+1).equals("in") || 
						destSentence.getLemma(underspecifiedIndex+1).equals("at")) ppNext = true;
				boolean detPrev = false;
				if (destSentence.getPos(underspecifiedIndex-1).equals("DT")) detPrev = true;

				if (detPrev && !thatNext && !ppNext) {
					// choose the previous sentence
					return true;
				}
			}
		}
		return false;
	}
	
	private static boolean proceededBySpecifier(Sentence sentence, int index, boolean debug) {
		boolean proceededBySpecifier = false;
		int tempIndex = index-1;
		while (tempIndex > -1) {
			String pos = sentence.getPos(tempIndex);
			String lemma = sentence.getLemma(tempIndex);
			if ((lemma.equals("that") && pos.equals("DT")) || lemma.equals("this") || lemma.equals("both") || 
					lemma.equals("such") || lemma.equals("those") || lemma.equals("these") || 
					(pos.equals("CD") && !sentence.getNer(tempIndex).equals("DATE") && 
					tempIndex > 0 && sentence.getLemma(tempIndex-1).equals("the") &&
					!lemma.equals("one") && (index >= sentence.getLength()-1 || 
					!(sentence.getPos(index+1).equals("IN") || sentence.getPos(index+1).equals("PP"))))) {
				if (!pos.equals("WDT") && (!lemma.equals("both") || 
						(!UnderspecifiedClassifier.followedByAnd(sentence,tempIndex)))) {
					proceededBySpecifier = true;
				}
				return proceededBySpecifier;
			}
			if (!pos.startsWith("J") && !pos.startsWith("N") && !pos.startsWith("CD")) {
				break;
			}
			tempIndex--;
		}
		return proceededBySpecifier;
	}
	
	private void generateNounEdgeSpecifier(ArrayList<Sentence> documentSentences, Sentence destSentence, int index, boolean debug) {
		String noun = destSentence.getLemma(index);
		// If it's a coreference  -  add a necessary edge
		if (!coreferenceInSentence(destSentence, new Pair<Integer,Integer>(index,index+1))) {
			List<Sentence> corefSentences = coreference(destSentence, new Pair<Integer,Integer>(index,index+1));
			for (Sentence corefSentence : corefSentences) {
				// add necessary edge between corefSentence and sentence
				addEdge(corefSentence, destSentence, noun, EDGE_TYPE.NOUN, null);
			}
			if (corefSentences.size() == 0) {
				List<Sentence> earlierMentions = earlierMentions(documentSentences, destSentence, noun);
				for (Sentence earlierMention : earlierMentions) {
					addEdge(earlierMention, destSentence, noun, EDGE_TYPE.NOUN, null);
				}
				if (earlierMentions(documentSentences, destSentence, noun).size() == 0 &&
						documentSentences.indexOf(destSentence) > 0) {
					Sentence sourceSentence = documentSentences.get(documentSentences.indexOf(destSentence)-1);
					addEdge(sourceSentence, destSentence, noun, EDGE_TYPE.NOUN, null);
				}
			}
		}
	}
	
	private void generateNounEdgeUnderspecified(HashMap<String,ArrayList<Sentence>> verbsToSentenceMapping,
												HashMap<String, HashSet<String>> nounVerbMapping, 
												ArrayList<Sentence> documentSentences, Sentence destSentence, int index) {
		String noun = destSentence.getLemma(index);
		
		// Edges to verb->nouns
		int verbReferencesInDoc = 0;
		if (nounVerbMapping.containsKey(noun)) {
			for (String verb : nounVerbMapping.get(noun)) {
				for (Sentence sourceSentence : verbsToSentenceMapping.get(verb)) {
					if (sourceSentence.getDocId().equals(destSentence.getDocId()) &&
							sourceSentence.getSentenceId() <= destSentence.getSentenceId()) {
						verbReferencesInDoc++;
					}
					// add necessary edge between verb sentence and sentence
					addEdge(sourceSentence, destSentence, noun, EDGE_TYPE.NECNOUNVERB, null);
				}
			}
		} 
		
		List<Sentence> corefSentences = coreference(destSentence, new Pair<Integer,Integer>(index,index+1));
		for (Sentence corefSentence : corefSentences) {
			// add necessary edge between corefSentence and sentence
			addEdge(corefSentence, destSentence, noun, EDGE_TYPE.NOUN, null);
		}
		
		if (verbReferencesInDoc == 0 && corefSentences.size() == 0) {
			List<Sentence> earlierMentions = earlierMentions(documentSentences, destSentence, noun);
			for (Sentence earlierMention : earlierMentions) {
				addEdge(earlierMention, destSentence, noun, EDGE_TYPE.NOUN, null);
			}
			if ( earlierMentions(documentSentences, destSentence, noun).size() == 0) {
				if (shouldLinkToPreviousSentence(documentSentences, destSentence, index)) {
					Sentence sourceSentence = documentSentences.get(documentSentences.indexOf(destSentence)-1);
					addEdge(sourceSentence, destSentence, noun, EDGE_TYPE.NOUN, null);
				}
			}
		}
	}
	
	private void generateNounEdges(Collection<Sentence> sentences, HashMap<String, ArrayList<Sentence>> sortedSentences) {
		HashMap<String,ArrayList<Sentence>> verbsToSentenceMapping = generateVerbToSentenceMapping(sentences);
		HashMap<String, HashSet<String>> nounVerbMapping = generateNounVerbMapping(sentences, verbsToSentenceMapping);
		
		for (ArrayList<Sentence> documentSentences : sortedSentences.values()) {
			for (Sentence destSentence : documentSentences) {
				for (Pair<Integer,String> nounPair : destSentence.getNouns()) {
					int index = nounPair.getFirst();
					String noun = destSentence.getLemma(index);
					// If the noun is preceded by this/that/those
					if (proceededBySpecifier(destSentence, index, false) && documentSentences.indexOf(destSentence) > 0) {
						generateNounEdgeSpecifier(documentSentences, destSentence, index, false);
					// If the noun is underspecified
					} else if (underspecifiedClassifier.classifyInstance(sentences, destSentence, index) < .5) {
						generateNounEdgeUnderspecified(verbsToSentenceMapping, nounVerbMapping, documentSentences, destSentence, index);
					// Non necessary noun->verbs
					} else {
						// Edges to verb->nouns
						if (nounVerbMapping.containsKey(noun)) {
							// add nonnecesary edge between verb sentence and sentence
							for (String verb : nounVerbMapping.get(noun)) {
								for (Sentence sourceSentence : verbsToSentenceMapping.get(verb)) {
									addEdge(sourceSentence, destSentence, noun, EDGE_TYPE.NOUNVERB, null);
								}
							}
						} 
					}
				}
			}
		}
	}
	
	/********** person to person edges ************/
	
	private HashMap<String,List<Sentence>> generateNameToSentenceMapping(Collection<Sentence> sentences) {
		HashMap<String,List<Sentence>> nameToSentenceMapping = new HashMap<String,List<Sentence>>();
		for (Sentence sentence : sentences) {
			HashSet<String> peopleStrings = sentence.getPeopleStringsFull();
			for (String name : peopleStrings) {
				if (!nameToSentenceMapping.containsKey(name)) {
					nameToSentenceMapping.put(name,  new ArrayList<Sentence>());
				}
				nameToSentenceMapping.get(name).add(sentence);
			}
		}
		return nameToSentenceMapping;
	}
	
	private static HashMap<String, HashSet<String>> generateSynonymMapping(Collection<Sentence> sentences) {
		HashMap<String, HashSet<String>> nameToNameMap = sortNamesIntoMaps(sentences);
		
		// for each sentence
		for (Sentence sentence : sentences) {
			// for each coreference 
			for (Coreference coreference :  sentence.getCoreferences()) {
				// for each mention
				HashSet<String> names = new HashSet<String>();
				for (Mention mention : coreference.getMentions()) {
					// get the full name and add to the mapping
					String name = fullName(mention);
					if (!mention.isPronoun() && name != null) {
						names.add(name);
					}
				}
				HashSet<HashSet<String>> nameClusters = getValidNameClusters(names);
				for (HashSet<String> cluster : nameClusters) {
					if (cluster.size() > 0) {
						for (String name : cluster) {
							if (!nameToNameMap.containsKey(name)) {
								nameToNameMap.put(name, new HashSet<String>());
							}
							nameToNameMap.get(name).addAll(cluster);
						}
					}
				}
			}
		}
		
		return nameToNameMap;
	}
	
	private void generateNonspecificPersonEdgesSub(HashMap<String,Sentence> sentences, 
			HashMap<String, HashSet<String>> sentenceToNameMapping, 
			HashMap<String, List<Sentence>> nameToSentenceMapping, 
			HashMap<String, HashSet<String>> synonyms) {
		for (Sentence nodeDest : sentences.values()) {
			HashSet<String> peopleStrings = nodeDest.getPeopleStringsFull();
			for (String name : peopleStrings) {
				boolean specific = specificName(sentences, sentenceToNameMapping, nodeDest, name, synonyms.get(name), false);
				if (!specific) {
					addEdge(null, nodeDest, name, EDGE_TYPE.PERSON, synonyms.get(name));
				}
			}
		}
	}
	
	private void generateNonspecificPersonEdges(HashMap<String,Sentence> sentences) {
		HashMap<String, HashSet<String>> sentenceToNameMapping = generateSentenceToNameMapping(sentences.values());
		HashMap<String,List<Sentence>> nameToSentenceMapping = generateNameToSentenceMapping(sentences.values());
		HashMap<String, HashSet<String>> synonyms = generateSynonymMapping(sentences.values());
		generateNonspecificPersonEdgesSub(sentences, sentenceToNameMapping, nameToSentenceMapping, synonyms);
	}
		
	private static HashMap<String, HashSet<String>> generateSentenceToNameMapping(Collection<Sentence> sentences) {
		HashMap<String, HashSet<String>> sentenceToNameMap = new HashMap<String, HashSet<String>>();

		// for each sentence
		for (Sentence sentence : sentences) {
			HashSet<String> peopleStrings = sentence.getPeopleStringsFull();
			if (peopleStrings.size() > 0) {
				sentenceToNameMap.put(sentence.getKey(), new HashSet<String>());
				sentenceToNameMap.get(sentence.getKey()).addAll(peopleStrings);
			}
		}
		return sentenceToNameMap;
	}
	
	private static HashMap<String, HashSet<String>> sortNamesIntoMaps(Collection<Sentence> sentences) {
		HashMap<String, HashSet<String>> lastNameSets = new HashMap<String, HashSet<String>>();
		for (Sentence sentence : sentences) {
			for (String name : sentence.getPeopleStringsFull()) {
				String lastName = name.split(" ")[name.split(" ").length-1].toLowerCase();
				if (!lastNameSets.containsKey(lastName)) {
					lastNameSets.put(lastName,  new HashSet<String>());
				}
				lastNameSets.get(lastName).add(name);
			}
		}
		
		HashMap<String, HashSet<String>> nameToNameMap = new HashMap<String, HashSet<String>>();
		for (HashSet<String> cluster : lastNameSets.values()) {
			if (cluster.size() > 0) {
				for (String name : cluster) {
					if (!nameToNameMap.containsKey(name)) {
						nameToNameMap.put(name, new HashSet<String>());
					}
					nameToNameMap.get(name).addAll(cluster);
				}
			}
		}
		return nameToNameMap;
	}
	
	private static String fullName(Mention mention) {
		Sentence sentence = mention.getSentence();
		List<String> tokens = sentence.getTokenList();
		List<String> ner = sentence.getNerList();
		int start = mention.getStart();
		int end = mention.getEnd();
		String name = "";
		for (int i = start; i < end; i++) {
			if (ner.get(i).equals("PERSON")) {
				if (i > 0 && name.equals("") && Parameters.TITLES.contains(tokens.get(i-1))) {
					name += tokens.get(i-1)+" ";
				}
				name += tokens.get(i)+" ";
			} else {
				if (!name.equals("")) {
					break;
				}
			}
		}
		if (!name.equals("")) {
			return name.trim();
		}
		return null;
	}
	
	private static HashSet<HashSet<String>> getValidNameClusters(HashSet<String> names) {
		if (names.size() > 0) {
			HashSet<HashSet<String>> clusters = sortIntoInitialClusters(names);
			clusters = refineClusters(clusters);
			return clusters;
		}
		return new HashSet<HashSet<String>>();
	}
	
	private static HashSet<HashSet<String>> sortIntoInitialClusters(HashSet<String> names) {
		HashSet<HashSet<String>> clusters = new HashSet<HashSet<String>>();
		for (String name1 : names) {
			boolean located = false;
			for (HashSet<String> cluster : clusters) {
				for (String name2 : cluster) {
					if (Utilities.overlap(name1.split(" "), name2.split(" "))>0) {
						cluster.add(name1);
						located = true;
						break;
					}
				}
				if (located) {
					break;
				}
			}
			if (!located) {
				HashSet<String> cluster = new HashSet<String>();
				cluster.add(name1);
				clusters.add(cluster);
			}
		}
		return clusters;
	}
	
	private static HashSet<HashSet<String>> refineClusters(HashSet<HashSet<String>> clusters) {
		boolean changed = true;
		while (changed) {
			changed = false;
			HashSet<HashSet<String>> newClusters = new HashSet<HashSet<String>>();
			HashSet<HashSet<String>> skipList = new HashSet<HashSet<String>>();
			Pair<HashSet<String>, HashSet<String>> mergePair = null;
			for (HashSet<String> cluster1 : clusters) {
				for (HashSet<String> cluster2 : clusters) {
					if (cluster1 != cluster2) {
						for (String name1 : cluster1) {
							for (String name2 : cluster2) {
								if (Utilities.overlap(name1.split(" "), name2.split(" "))>0) {
									mergePair = new Pair<HashSet<String>,HashSet<String>>(cluster1,cluster2);
									cluster1.addAll(cluster2);
									skipList.add(cluster2);
									break;
								}
							}
							if (mergePair != null) {
								break;
							}
						}
						if (mergePair != null) {
							break;
						}
					}
				}
				if (!skipList.contains(cluster1)) {
					newClusters.add(cluster1);
				}
				if (mergePair != null) {
					break;
				}
			}
			if (mergePair != null) {
				clusters = newClusters;
			}
		}
		return clusters;
	}
	
	private static boolean specificName(HashMap<String,Sentence> sentences, 
			HashMap<String, HashSet<String>> sentenceToNameMap, 
			Sentence sentence, String name, HashSet<String> names, boolean debug) {
		String[] tokens = name.split(" ");
		int capCount = 0;
		if (tokens.length > 1) {
			for (int i = 0; i < tokens.length; i++)
			if (Utilities.firstLetterCapitalized(tokens[i])) {
				capCount++;
				break;
			}
		}
		if (capCount > 0 && (names==null) || !longerSynonymExistsWithinDoc(sentences, sentenceToNameMap, sentence, name, names, debug)) {
			return true;
		}
		
		return false;
	}
	
	private static boolean longerSynonymExistsWithinDoc(HashMap<String,Sentence> sentences, 
			HashMap<String, HashSet<String>> sentenceToNameMap, 
			Sentence sentence, String name, HashSet<String> names, boolean debug) {
		String docId = sentence.getDocId();
		int sentenceId = sentence.getSentenceId();
		for (int id = 0; id < sentenceId; id++) {
			String key = Sentence.getKey(docId, id);
			if (sentenceToNameMap.containsKey(key)) {
				if (sentenceToNameMap.get(key).contains(name) && name.split(" ").length < 2) {
					return true;
				}
				for (String synonym : names) {
					if (sentenceToNameMap.get(key).contains(synonym)) {
						String tempSynonym = synonym.toLowerCase();
						for (String title : Parameters.TITLES){
							if (synonym.startsWith(title)) {
								tempSynonym = tempSynonym.replaceFirst(title, "").trim();
							}
						}
						if (!tempSynonym.equals(name.toLowerCase()) && tempSynonym.contains(name.toLowerCase())) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	/********** pronoun edges **************/
	
	private void generatePronounEdges(HashMap<String, ArrayList<Sentence>> sortedSentences) {
		for (String docId : sortedSentences.keySet()) {
			for (int i = 1; i < sortedSentences.get(docId).size(); i++) {
				Sentence nodeSource = sortedSentences.get(docId).get(i-1);
				Sentence nodeDest = sortedSentences.get(docId).get(i);
				if (nodeDest.pronounsNotInSentence().size() > 0) {
					addEdge(nodeSource, nodeDest, "*pronoun*", EDGE_TYPE.PERSON, null);
				}
			}
		}

	}
	
	/********** transition edges ************/
	
	private void generateDiscourseQueEdges(HashMap<String, ArrayList<Sentence>> sortedSentences) {
		for (ArrayList<Sentence> sentenceGroup : sortedSentences.values()) {
			for (int i = 1; i < sentenceGroup.size(); i++) {
				Sentence sentenceDest = sentenceGroup.get(i);
				Sentence sentenceSource = sentenceGroup.get(i-1);
				String transition = sentenceDest.transitionSentence(false);
				if (!transition.equals("") && sentenceSource.getSentenceId() == sentenceDest.getSentenceId()-1) {
					addEdge(sentenceSource, sentenceDest, transition, EDGE_TYPE.TRANSITION, null);
				} else if (!transition.equals("")) {
					addEdge(null, sentenceDest, transition, EDGE_TYPE.TRANSITION, null);
				}
			}
		}
	}
	
	/********** in edges ****************/
	public List<SentenceEdge> getInEdges(Sentence destSentence) {
		String key = destSentence.getKey();
		if (inEdges.containsKey(key)) {
			return inEdges.get(key);
		}
		return new ArrayList<SentenceEdge>();
	}
	
	public HashMap<Sentence,List<SentenceEdge>> getInEdgesTransition(Sentence destSentence) {
		String key = destSentence.getKey();
		if (inEdgesTransition.containsKey(key)) {
			return inEdgesTransition.get(key);
		}
		return new HashMap<Sentence,List<SentenceEdge>>();
	}
	
	public HashMap<Sentence,List<SentenceEdge>> getInEdgesNoun(Sentence destSentence) {
		String key = destSentence.getKey();
		if (inEdgesNoun.containsKey(key)) {
			return inEdgesNoun.get(key);
		}
		return new HashMap<Sentence,List<SentenceEdge>>();
	}
	
	public HashMap<Sentence,List<SentenceEdge>> getInEdgesNounVerb(Sentence destSentence) {
		String key = destSentence.getKey();
		if (inEdgesNounVerb.containsKey(key)) {
			return inEdgesNounVerb.get(key);
		}
		return new HashMap<Sentence,List<SentenceEdge>>();
	}
	
	public HashMap<Sentence,List<SentenceEdge>> getInEdgesNecNounVerb(Sentence destSentence) {
		String key = destSentence.getKey();
		if (inEdgesNecNounVerb.containsKey(key)) {
			return inEdgesNecNounVerb.get(key);
		}
		return new HashMap<Sentence,List<SentenceEdge>>();
	}
	
	public HashMap<Sentence,List<SentenceEdge>> getInEdgesPerson(Sentence destSentence) {
		String key = destSentence.getKey();
		if (inEdgesPerson.containsKey(key)) {
			return inEdgesPerson.get(key);
		}
		return new HashMap<Sentence,List<SentenceEdge>>();
	}

}
