package edu.washington.cs.knowitall.summarization;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;

import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.utilities.Utilities;


public class ActionResponseGenerator {
	private HashMap<String, Double> actionResponseMapping;
	private HashSet<String> events = new HashSet<String>();
	private static final String ACTION_RESPONSE_MAPPING_FILE = "data/deverbalNounPairs.txt";
	private static final String ACTION_RESPONSE_EVENT_FILE = "data/events.txt";
	
	public ActionResponseGenerator() {
		this.actionResponseMapping = initFromFile();
	}
	
	private HashMap<String, Double> initFromFile() {
		String[] eventlines = Utilities.readInFile(new File(ACTION_RESPONSE_EVENT_FILE));
		for (String line : eventlines) {
			 String[] tokens = line.split("\t");
			if (tokens.length == 2) {
				String event = tokens[0];
				events.add(event);
			}
		}
		HashMap<String, Double> actionResponseMapping = new HashMap<String, Double>();
		String[] lines = Utilities.readInFile(new File(ACTION_RESPONSE_MAPPING_FILE));
		for (String line : lines) {
			String[] tokens = line.split("\t");
			if (tokens.length == 3) {
				String noun = tokens[0];
				String verb = tokens[1];
				double value = Double.parseDouble(tokens[2]);
				String key = noun+"\t"+verb;
				if (events.contains(noun)) {
					actionResponseMapping.put(key, value);
				}
			}
		}
		return actionResponseMapping;
	}
	
	public double related(String noun, String baseVerb) {
		String verb = baseVerb;
		String key = noun+"\t"+verb;
		if (actionResponseMapping.containsKey(key)) {
			double score = actionResponseMapping.get(key);
			return score;
		}
		return 0.0;
	}
	
	public static boolean validResponse(Sentence sentence, int location) {
		if ((sentence.getNer(location).equals("LOCATION") ||
				sentence.getNer(location).equals("ORGANIZATION") ||
				sentence.getNer(location).equals("NUMBER") ||
				sentence.getNer(location).equals("MISC"))) {
			return false;
		}
		if (Parameters.NON_EVENTS.contains(sentence.getLemma(location))) {
			return false;
		}
		if (location > 0) {
			int prev = location-1;
			String prevLemma = sentence.getLemma(prev);
			String prevPos = sentence.getPos(prev);
			while (//(prevPos.equals("JJ") || prevPos.equals("JJS"))
					(prevPos.equals("NNP") || prevPos.equals("NN") || prevPos.equals("CD")) && prev > 0) {
				prev--;
				prevLemma = sentence.getLemma(prev);
				prevPos = sentence.getPos(prev);
			}
			if (sentence.getSentenceId() == 0 || Parameters.ACTION_INDIC.contains(prevLemma) || Parameters.ACTION_INDIC.contains(prevPos)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isEvent(String lemma) {
		return events.contains(lemma);
	}
	
}
