package edu.washington.cs.knowitall.summarization;

import java.util.Collection;
import java.util.HashMap;

import edu.washington.cs.knowitall.datastructures.Sentence;

public class SalienceCalculator {
	private SalienceClassifier classifier;
	private HashMap<Sentence,Double> salienceScores = new HashMap<Sentence,Double>();
	private boolean hierarchical;
	
	public SalienceCalculator( WordScorer wordScorer, boolean hierarchical) {
		this.classifier = new SalienceClassifier(wordScorer);
		this.hierarchical = hierarchical;
	}
	
	public void setup(Collection<Sentence> sentences) {
		classifier.setup(sentences);
		this.salienceScores = classifier.classifySentences(hierarchical);
	}
	
	public double getSalience(Sentence sentence) {
		if (sentence != null) {
			return salienceScores.get(sentence);
		} else {
			return 0.0;
		}
	}

	public double getMaxSalienceScore() {
		double max = -100000000;
		
		for (Sentence sentence : salienceScores.keySet()) {
			if (getSalience(sentence) > max) {
				max = getSalience(sentence);
			}
		}
		return max;
	}

}
