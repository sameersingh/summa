package edu.washington.cs.knowitall.summarization;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import weka.classifiers.functions.Logistic;
import weka.core.Instances;

import edu.stanford.nlp.trees.Tree;
import edu.washington.cs.knowitall.datastructures.Sentence;

public class UnderspecifiedClassifier {
	private ActionResponseGenerator actionResponseGenerator = new ActionResponseGenerator();
//	private HashMap<String, Double> labelMap = new HashMap<String, Double>();
	private HashMap<String, Double> predictionMap = new HashMap<String, Double>();
	private HashMap<String, Integer> detMap = new HashMap<String, Integer>();
	private HashMap<String, Integer> aMap = new HashMap<String, Integer>();
    private Logistic classifier;

	private static String trainingFilename = "data/underspecified.arff";
	
	public UnderspecifiedClassifier() {
		setupClassifier();
	//	readInLabels(); 
	}
	

	/**
	 * Set up the classifier by reading in the training data.
	 */
	private void setupClassifier() {
		// Set up the classifier from the training data.
		InputStreamReader trainingReader = null;
		classifier = new Logistic();
        try {
        	trainingReader = new InputStreamReader(new FileInputStream(trainingFilename));
        	Instances trainingInstances = setupInstances(trainingReader);
        	classifier.buildClassifier(trainingInstances);
        }
    	catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
    	}
		        
	}
	
	/**
	 * Set up the instances from the reader
	 * @param instanceReader the source of the instances
	 * @return the instances object
	 */
	private Instances setupInstances(java.io.Reader instanceReader) {
		Instances instances = null;
		try {
			instances = new Instances(instanceReader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		instances.setClassIndex(instances.numAttributes() - 1);
		try {
			instanceReader.close();
		} catch (IOException e) {
			System.err.println("could not close reader");
			e.printStackTrace();
			System.exit(1);
		}
		return instances;
	}

	/**
	 * Classify an instance using the features provided
	 * @param features
	 * @return
	 */
	public double classifyInstance(String features) {
		StringReader testReader = new StringReader(features+"\n");
		Instances testingInstances = setupInstances(testReader);
		try {
			classifier.classifyInstance(testingInstances.firstInstance());
			return classifier.distributionForInstance(testingInstances.firstInstance())[0];
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.5;
	}
	
	public double classifyInstance(Collection<Sentence> sentences, Sentence sentence, int nounIndex) {
		String key = sentence.getKey()+"::"+nounIndex;
		if (predictionMap.containsKey(key)) {
			return predictionMap.get(key);
		}
		String features = getHeader()+"\n"+getFeatures(sentences, sentence, nounIndex)+",0.0";
		double predictedLabel = classifyInstance(features);
		predictionMap.put(key, predictedLabel);
		return predictedLabel;
	}
		
	/*** FEATURES ***/

	private static String getLocations(Sentence sentence, int index) {
		String location = "";
		int tempIndex = index-1;
		while (tempIndex > 0) {
			if (sentence.getNer(tempIndex).equals("LOCATION")) {
				location += sentence.getToken(tempIndex)+" ";
				tempIndex--;
			} else if (sentence.getPos(tempIndex).startsWith("N") ||
						sentence.getPos(tempIndex).equals("DT") || 
						sentence.getPos(tempIndex).equals("IN")) {
				tempIndex--;
			} else {
				break;
			}
		}
		
		tempIndex = index+1;
		while (tempIndex < sentence.getLength()) {
			if (sentence.getNer(tempIndex).equals("LOCATION")) {
				location += sentence.getToken(tempIndex)+" ";
				tempIndex++;
			} else if (sentence.getPos(tempIndex).startsWith("N") ||
						sentence.getPos(tempIndex).equals("DT") || 
						sentence.getPos(tempIndex).equals("IN") ||
						sentence.getPos(tempIndex).equals("OF")) {
				tempIndex++;
			} else {
				break;
			}
		}
		return location;
	}
		
	private int numberOfTimesMentionedDet(Collection<Sentence> sentences, String noun) {
		if (detMap.containsKey(noun)) {
			return detMap.get(noun);
		}
		int mentioned = 0;
		for (Sentence tempSentence : sentences) {
			if (tempSentence.getLemmaList().contains(noun)) {
				int index = tempSentence.getLemmaList().indexOf(noun);
				if (proceededBySpecifier(tempSentence, index)) {
					mentioned++;
				}
			}
		}
		detMap.put(noun, mentioned);
		return mentioned;
	}
	
	
	private int numberOfTimesMentionedA(Collection<Sentence> sentences, String noun) {
		if (aMap.containsKey(noun)) {
			return aMap.get(noun);
		}
		int mentioned = 0;
		for (Sentence tempSentence : sentences) {
			if (tempSentence.getLemmaList().contains(noun)) {
				int index = tempSentence.getLemmaList().indexOf(noun);
				int prevIndex = getPrevIndexExcludeExtras(tempSentence, index);
				if (prevIndex > -1 && (tempSentence.getLemma(prevIndex).equals("a") || tempSentence.getLemma(prevIndex).equals("an"))) {
					mentioned++;
				}
			}
		}
		aMap.put(noun, mentioned);
		return mentioned;
	}
	
	
	private boolean mentionedInDoc(Collection<Sentence> sentences, Sentence sentence, String noun) {
		boolean mentioned = false;
		for (Sentence tempSentence : sentences) {
			if (tempSentence.getDocId().equals(sentence.getDocId()) && 
					tempSentence.getSentenceId() < sentence.getSentenceId() && 
					tempSentence.getLemmaList().contains(noun)) {
				int index = tempSentence.getLemmaList().indexOf(noun);
				if (proceededBySpecifier(tempSentence, index)) {
					mentioned = true;
				}
			}
		}
		return mentioned;
	}
	
	public static boolean followedByAnd(Sentence sentence, int index) {
		index++;
		while (index < sentence.getLength()) {
			String pos = sentence.getPos(index);
			String lemma = sentence.getLemma(index);
			if (lemma.equals("and") && index < sentence.getLength()-1 && sentence.getPos(index+1).startsWith("N")) {
				return true;
			}
			if (!pos.startsWith("J") && !pos.startsWith("N") && !pos.startsWith("CD") && !pos.startsWith("DT")) {
				return false;
			}
			index++;
		}
		return false;
	}
	
	public static boolean proceededBySpecifier(Sentence sentence, int index) {
		boolean proceededBySpecifier = false;
		int tempIndex = index-1;
		while (tempIndex > -1) {
			String pos = sentence.getPos(tempIndex);
			String lemma = sentence.getLemma(tempIndex);
			if (lemma.equals("the") || lemma.equals("that") || lemma.equals("this") || 
					lemma.equals("both") || lemma.equals("such") || lemma.equals("those") ||
					lemma.equals("these") || lemma.equals("other")) {
				if (!pos.equals("WDT") && !pos.equals("IN") && (!lemma.equals("both") || (followedByAnd(sentence,tempIndex)))) {
					proceededBySpecifier = true;
				}
				return proceededBySpecifier;
			}
			if (!pos.startsWith("J") && !pos.startsWith("N") && !pos.startsWith("CD")) {
				break;
			}
			tempIndex--;
		}
		return proceededBySpecifier;
	}
	
	public static boolean proceededByRefSpecifier(Sentence sentence, int index) {
		boolean proceededBySpecifier = false;
		int tempIndex = index-1;
		while (tempIndex > -1) {
			String pos = sentence.getPos(tempIndex);
			String lemma = sentence.getLemma(tempIndex);
			if (lemma.equals("that") || lemma.equals("this") || 
					lemma.equals("both") || lemma.equals("such") || lemma.equals("those") ||
					lemma.equals("these") || lemma.equals("other")) {
				if (!pos.equals("WDT") && !pos.equals("IN") && (!lemma.equals("both") || (!followedByAnd(sentence,tempIndex)))) {
					proceededBySpecifier = true;
				}
				return proceededBySpecifier;
			}
			if (!pos.startsWith("J") && !pos.startsWith("N") && !pos.startsWith("CD")) {
				break;
			}
			tempIndex--;
		}
		return proceededBySpecifier;
	}
	
	private int getPrevIndexExcludeExtras(Sentence sentence, int index) {
		if (index <= 0) {
			return index-1;
		}
		int tempIndex = index-1;
		String pos = sentence.getPos(tempIndex);
		while (tempIndex > 0 && (pos.startsWith("J") || pos.startsWith("N") || pos.startsWith("CD"))) {
			tempIndex--;
			pos = sentence.getPos(tempIndex);
		}
		return tempIndex;
	}
	
	public boolean isSubject(Sentence sentence, int index) {
		boolean subject = false;
		Tree root = sentence.getParseTree();
		Tree leaf = root.getLeaves().get(index);
		List<Tree> path = root.dominationPath(leaf);
		if (path.size() > 2 && path.get(0).label().value().equals("ROOT") && 
				path.get(1).label().value().equals("S") && 
				path.get(2).label().value().equals("NP")) {
			boolean okay = true;
			for (Tree element : path) {
				if (element.label().value().startsWith("PP") || element.label().value().startsWith("SBAR")) {
					okay = false;
				}
			}
			subject = okay;
		}
		return subject;
	}
	
	public boolean withinClause(Sentence sentence, int index) {
		Tree root = sentence.getParseTree();
		Tree leaf = root.getLeaves().get(index);
		List<Tree> path = root.dominationPath(leaf);
		for (Tree tree : path) {
			if (tree.label().value().equals("SBAR")) {
				String firstWord = tree.getLeaves().get(0).label().value();
				if (firstWord.equals("when") || firstWord.equals("after") || firstWord.equals("before") || firstWord.equals("while")) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean withinClauseStrict(Sentence sentence, int index) {
		Tree root = sentence.getParseTree();
		Tree leaf = root.getLeaves().get(index);
		List<Tree> path = root.dominationPath(leaf);
		for (Tree tree : path) {
			if (tree.label().value().equals("SBAR")) {
				String firstWord = tree.getLeaves().get(0).label().value();
				if (firstWord.equals("when") || firstWord.equals("after") || firstWord.equals("before") || firstWord.equals("while")) {
					if (tree.getChildrenAsList().size() > 1 && tree.getChild(1).label().value().equals("S")) {
						if (tree.getChild(1).getChild(0).label().value().equals("NP") && tree.getChild(1).getChild(0).getLeaves().contains(leaf)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public boolean withinPossessive(Sentence sentence, int index) {
		if (index  < 2) {
			return false;
		}
		int prevIndexExclude = getPrevIndexExcludeExtras(sentence, index);
		String prevPosExclude = sentence.getPos(prevIndexExclude);
		if (prevPosExclude.equals("POS") || prevPosExclude.equals("PRP$")) {
			return true;
		}
		return false;
	}
	
	public boolean datePossessive(Sentence sentence, int index) {
		if (withinPossessive(sentence, index)) {
			int prevIndexExclude = getPrevIndexExcludeExtras(sentence, index);
			if (prevIndexExclude > 0 && sentence.getNer(prevIndexExclude-1).equals("DATE")) {
				return true;
			}
		}
		return false;
	}
	
	public boolean personPossessive(Sentence sentence, int index) {
		if (withinPossessive(sentence, index)) {
			int prevIndexExclude = getPrevIndexExcludeExtras(sentence, index);
			if (prevIndexExclude > 0 && sentence.getNer(prevIndexExclude-1).equals("PERSON")) {
				return true;
			}
		}
		return false;
	}
	
	private String getFeatures(Collection<Sentence> sentenceMap, Sentence sentence, int index) {
		String prevToken = "";
		String prevPos = "";
		if (index > 0) {
			prevToken = sentence.getToken(index-1);
			prevPos = sentence.getPos(index-1);
		}
		
		// Proceeded by specifier
		int proceededBySpecifier = 0;
		if (proceededBySpecifier(sentence, index)) proceededBySpecifier = 1;
		
		// Subject
		int subject = 0;
		if (isSubject(sentence, index)) subject = 1;
		
		// within clause (before, after, while, when)
		int withinClause = 0;
		if (withinClause(sentence, index)) withinClause = 1;
		
		int withinClauseStrict = 0;
		if (withinClauseStrict(sentence, index)) withinClauseStrict = 1;
		
		String nextPos = "";
		String nextLemma = "";
		if (index < sentence.getLength()-1) {
			nextPos = sentence.getPos(index+1);
			nextLemma = sentence.getLemma(index+1);
		}
		
		int otherNer = 0;
		if (sentence.getNer(index).equals("O")) otherNer = 1;
		
		double nextIsOf = 0.0;
		if (nextLemma.equals("of")) nextIsOf = 1.0;
		
		double nextIsComma = 0.0;
		if (nextLemma.equals(",")) nextIsComma = 1.0;
		
		double nextIsCC = 0.0;
		if (nextPos.equals("CC")) nextIsCC = 1.0;
		
		double nextIsMD = 0.0;
		if (nextPos.equals("MD")) nextIsMD = 1.0;
		
		double nextIsTO = 0.0;
		if (nextPos.equals("TO")) nextIsTO = 1.0;
		
		double nextIsVBD = 0.0;
		if (nextPos.equals("VBD")) nextIsVBD = 1.0;
		
		double nextIsVBZ = 0.0;
		if (nextPos.equals("VBZ")) nextIsVBZ = 1.0;
		
		double otherVB = 0.0;
		if (nextPos.equals("VBN") || nextPos.equals("VBP") || nextPos.equals("VBG")) otherVB = 1.0;
		
		double nextIsWDT = 0.0;
		if (nextPos.equals("WDT")) nextIsWDT = 1.0;
		
		double prevIsPos = 0.0;
		if (withinPossessive(sentence, index)) prevIsPos = 1.0;
		
		double datePos = 0.0;
		if (datePossessive(sentence, index)) datePos = 1.0; 
		
		double personPos = 0.0;
		if (personPossessive(sentence, index)) personPos = 1.0; 
		
		double numberOfTimesMentionedDet = numberOfTimesMentionedDet(sentenceMap, sentence.getLemma(index));
		
		double numberOfTimesMentionedA = numberOfTimesMentionedA(sentenceMap, sentence.getLemma(index));
		
		double location = 0.0;
		if (!getLocations(sentence, index).equals("")) location = 1.0;
		
		int sentenceDateSize = sentence.getSentenceDates().size();
		
		double properNounAdj = 0.0;
		if (prevPos.startsWith("JJ") & !prevToken.toLowerCase().equals(prevToken)) properNounAdj = 1.0;
		
		double mentionedInDoc = 0.0;
		if (mentionedInDoc(sentenceMap, sentence, sentence.getLemma(index))) mentionedInDoc = 1.0;
		
		double subjectSpecifier = 0.0;
		if (subject == 1.0 && proceededBySpecifier == 1.0) subjectSpecifier = 1.0;
		
		double event = 0.0;
		if (actionResponseGenerator.isEvent(sentence.getLemma(index))) event = 1.0;
		
		double wdtVbd = 0.0;
		if (nextIsWDT > .5 && sentence.getPos(index+2).equals("VBD")) {
			wdtVbd = 1.0;
		}
		
		double incorrectWord = 0.0;
		
		if (Parameters.NON_AMBIGUOUS.contains(sentence.getLemma(index))) incorrectWord = 1.0;

		// Proceeded by specifier
		int proceededByRefSpecifier = 0;
		if (proceededByRefSpecifier(sentence, index)) proceededByRefSpecifier = 1;
		
		
		String features = otherNer+","+
							proceededBySpecifier+","+
							subject+","+
							index+","+
							nextIsOf+","+
							nextIsComma+","+
							nextIsCC+","+
						    nextIsMD+","+
							nextIsTO+","+
						    nextIsVBD+","+
							nextIsVBZ+","+
						    otherVB+","+
							nextIsWDT+","+
							prevIsPos+","+
							numberOfTimesMentionedDet+","+
							location+","+
							sentenceDateSize+","+
							properNounAdj+","+
							mentionedInDoc+","+
							subjectSpecifier+","+
							datePos+","+
							withinClause+","+
							personPos+","+
							numberOfTimesMentionedA+","+
							event+","+
							wdtVbd+","+
							withinClauseStrict+","+
							incorrectWord+","+
							proceededByRefSpecifier;
		return features;
	}
	
	/**** END FEATURES ***/
	
	/**
	 * Get the header.
	 * @return the header
	 */
	private static String getHeader() {
		String header = "@RELATION underspecified\n"+
						"@ATTRIBUTE otherNer NUMERIC\n"+
						"@ATTRIBUTE proceededBySpecifier NUMERIC\n"+
						"@ATTRIBUTE subject NUMERIC\n"+
						"@ATTRIBUTE index NUMERIC\n"+
						"@ATTRIBUTE nextIsOf NUMERIC\n"+
						"@ATTRIBUTE nextIsComma NUMERIC\n"+
						"@ATTRIBUTE nextIsCC NUMERIC\n"+
						"@ATTRIBUTE nextIsMD NUMERIC\n"+
						"@ATTRIBUTE nextIsTO NUMERIC\n"+
						"@ATTRIBUTE nextIsVBD NUMERIC\n"+
						"@ATTRIBUTE nextIsVBZ NUMERIC\n"+
						"@ATTRIBUTE otherVB NUMERIC\n"+
						"@ATTRIBUTE nextIsWDT NUMERIC\n"+
						"@ATTRIBUTE prevIsPos NUMERIC\n"+
						"@ATTRIBUTE numberOfTimesMentionedDet NUMERIC\n"+
						"@ATTRIBUTE location NUMERIC\n"+
						"@ATTRIBUTE sentenceDateSize NUMERIC\n"+
						"@ATTRIBUTE properNounAdj NUMERIC\n"+
						"@ATTRIBUTE mentionedInDoc NUMERIC\n"+
						"@ATTRIBUTE subjectSpecifier NUMERIC\n"+
						"@ATTRIBUTE datePos NUMERIC\n"+
						"@ATTRIBUTE withinClause NUMERIC\n"+
						"@ATTRIBUTE personPos NUMERIC\n"+
						"@ATTRIBUTE numberOfTimesMentionedA NUMERIC\n"+
						"@ATTRIBUTE event NUMERIC\n"+
						"@ATTRIBUTE wdtVbd NUMERIC\n"+
						"@ATTRIBUTE withinClauseStrict NUMERIC\n"+
						"@ATTRIBUTE incorrectWord NUMERIC\n"+
						"@ATTRIBUTE refSpecifier NUMERIC\n"+
						"@ATTRIBUTE class {0.0,1.0}\n"+
						"@DATA";
		return header;
	}



}
