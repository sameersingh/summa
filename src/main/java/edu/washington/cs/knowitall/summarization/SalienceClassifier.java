package edu.washington.cs.knowitall.summarization;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;


import edu.washington.cs.knowitall.datastructures.Pair;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.utilities.Classifier;
import edu.washington.cs.knowitall.utilities.Utilities;


public class SalienceClassifier extends Classifier {
	
	private static String trainingFilenameTitles = "data/salienceTitles.arff";
	private static String trainingFilenameNoTitles = "data/salience.arff";
	private boolean usingTitles = false;
	
	private WordScorer wordScorer;
	
	public SalienceClassifier(WordScorer wordScorer) {
		super();
		this.wordScorer = wordScorer;
	}
	
	public int getScoreProperNoun(String properNoun) {
		return wordScorer.scoreProperNoun(properNoun);
	}
	
	public void setup(Collection<Sentence> sentences) {
		this.sentences = sentences;
		if (sentences.iterator().hasNext() && 
			sentences.iterator().next().getTitle() != null) {
			usingTitles = true;
		}
		if (usingTitles) {
			setupTraining(trainingFilenameTitles);
		} else {
			setupTraining(trainingFilenameNoTitles);
		}
	}
	

	public HashMap<Sentence,Double> classifySentences(boolean hierarchical) {
		HashMap<Sentence,Double> valueMap = new HashMap<Sentence,Double>();
		HashMap<Sentence,String> featureMap = generateTrainingData(hierarchical);
		for (Sentence sentence : featureMap.keySet()) {
			String features = getHeader()+"\n"+featureMap.get(sentence);
			double salience = classifyInstance(features);
			//TODO adding in negative salience
			int minScore = 0;
			for (Pair<Integer, String> properNoun : sentence.getProperNouns()) {
				int index = properNoun.getFirst();
				String noun = properNoun.getSecond();
				if (sentence.getPos(index).startsWith("NNP")) {
					if (sentence.getNer(index).equals("PERSON")) {
						noun = noun.split(" ")[noun.split(" ").length-1];
					}
					if (getScoreProperNoun(noun) < minScore || minScore == 0) {
						minScore = getScoreProperNoun(noun);
					}
				}
			}
			if (minScore > 0 && hierarchical) {
				salience -= 1.0/minScore;
			}
			valueMap.put(sentence, salience);
		}
		return valueMap;
	}
	
	/** 
	 * For each possible sentence generate features for it
	 * and return the scaled features for all sentences.
	 * @return map of sentence to features
	 */
	public HashMap<Sentence, String> generateTrainingData(boolean active) {
		ArrayList<ArrayList<Double>> features = new ArrayList<ArrayList<Double>>();
		// Generate the features for each sentence.
		for (Sentence sentence : sentences) {
			
			// get the features
			ArrayList<Double> tempFeatures = getFeatures(sentences, sentence, active);
			
			// label the sentence
			double tempLabel = labelSentence(sentence);
			tempFeatures.add(tempLabel);
			
			features.add(tempFeatures);
		}
		
		// Scale all the features
		features = scaleFeatures(features);
		
		// Join all the features into single strings.
		HashMap<Sentence, String> featureMap = new HashMap<Sentence, String>();
		int i = 0;
		for (Sentence sentence : sentences) {
			String featureStr = Utilities.join(features.get(i), ",");
			featureMap.put(sentence, featureStr);
			i++;
		}
		return featureMap;
	}
	
	private HashSet<String> getTitles(Collection<Sentence> sentences) {
		HashSet<String> titles = new HashSet<String>();
		for (Sentence sentence : sentences) {
			if (sentence.getTitle()!=null) {
				titles.add(sentence.getTitle());
			}
		}
		return titles;
	}
	
	/** 
	 * Get the features for a potential sentence.
	 * @param sentence
	 * @return
	 */
	private ArrayList<Double> getFeatures(Collection<Sentence> sentences, Sentence sentence, boolean active) {
		ArrayList<Double> features = new ArrayList<Double>();
		boolean debug = false;
		if (sentence.getSentenceStr().contains("One day soon") ||
				sentence.getSentenceStr().contains("asked the faithful")) {
			debug = true;
		}
		// sentence id
		double sentenceId = sentence.getSentenceId();
		
		// first three sentences
		double firstThreeSentences = 0;
		if (sentenceId < 3) {
			firstThreeSentences = 1;
		}
		
		// Contains nouns
		double nouns = 0;
		if (sentence.containsNouns()) {
			nouns = 1;
		}

		// Contains numbers
		double numbers = 0;
		if (sentence.containsNumbers()) {
			numbers = 1;
		}
		
		// Contains money
		double money = 0;
		if (sentence.containsMoney()) {
			money = 1;
		}
		
		double peopleCount = sentence.getPeopleStrings().size();

		double sentenceLength = sentence.getLength();
		
		double greaterThan20 = 0;
		if (sentenceLength > 20) {
			greaterThan20 = 1;
		}
		
		double scoreNoun = wordScorer.scoreSentenceNoun(sentence, active, debug);
		
		double scoreProperNoun = wordScorer.scoreSentenceProperNoun(sentence, active);
		
		double scoreVerb = wordScorer.scoreSentenceVerb(sentence, active);

		HashSet<String> titles = getTitles(sentences);
		double titleOverlap = 0;
		for (String title : titles) {
			for (String token : title.split(" ")) {
				if (!Parameters.STOP_WORDS.contains(token.toLowerCase()) && 
						(sentence.getTokenList().contains(token) ||
								sentence.getTokenList().contains(token.toLowerCase()) ||
								sentence.getLemmaList().contains(token) ||
								sentence.getLemmaList().contains(token.toLowerCase()))) {
					titleOverlap++;
				}
			}
		}
		
		features.add(sentenceId);
		features.add(firstThreeSentences);
		features.add(nouns);
		features.add(numbers);
		features.add(money);
		features.add(peopleCount);
		features.add(sentenceLength);
		features.add(greaterThan20);
		features.add(scoreNoun);
		features.add(scoreProperNoun);
		features.add(scoreVerb);
		if (usingTitles) {
			features.add(titleOverlap);
		}
		return features;
	}
	
	public double countPeopleDocument(Sentence sentence1) {
		HashSet<String> people = sentence1.getPeopleStrings();
		HashSet<String> docs = new HashSet<String>();
		if (people.size() > 0) {
			for (Sentence sentence2 : sentences) {
				if (sentence2 != sentence1) {
					for (String person : people) {
						person = person.toLowerCase();
						if (sentence2.getLemmaList().contains(person)) {
							docs.add(sentence2.getDocId());
						}
					}
				}
			}
			return docs.size()/people.size();
		} else {
			return 0;
		}
	}
	
	public double countPeopleWithinDocument(Sentence sentence1) {
		HashSet<String> people = sentence1.getPeopleStrings();
		int personCount = 0;
		if (people.size() > 0) {
			for (Sentence sentence2 : sentences) {
				if (sentence2 != sentence1 && sentence1.getDocId().equals(sentence2.getDocId())) {
					for (String person : people) {
						person = person.toLowerCase();
						if (sentence2.getLemmaList().contains(person)) {
							personCount++;
						}
					}
				}
			}
			return personCount/people.size();
		} else {
			return 1;
		}
	}
	
	public double personAppearsOnlyOnce(Sentence sentence1) {
		HashSet<String> people = sentence1.getPeopleStrings();
		if (people.size() > 0) {
			int personCount = 0;
			for (String person : people) {
				person = person.toLowerCase();
				for (Sentence sentence2 : sentences) {
					if (sentence2 != sentence1 && sentence1.getDocId().equals(sentence2.getDocId())) {
						if (sentence2.getLemmaList().contains(person)) {
							personCount++;
						}
					}
				}
				if (personCount == 0) {
					return 1;
				}
			}
		}
		return 1;
	}
	
	public String getHeader() {
		String header = ""+
				"@RELATION salience\n"+
				"@ATTRIBUTE sentenceId   NUMERIC\n"+
				"@ATTRIBUTE firstThreeSentences   NUMERIC\n"+
				"@ATTRIBUTE nouns	NUMERIC\n"+
				"@ATTRIBUTE numbers   NUMERIC\n"+
				"@ATTRIBUTE money   NUMERIC\n"+
				"@ATTRIBUTE peopleCount   NUMERIC\n"+
				"@ATTRIBUTE sentenceLength   NUMERIC\n"+
				"@ATTRIBUTE greaterThan20    NUMERIC\n"+
				"@ATTRIBUTE scoreNoun   NUMERIC\n"+
				"@ATTRIBUTE scoreProperNoun   NUMERIC\n"+
				"@ATTRIBUTE scoreVerb   NUMERIC\n";
		if (usingTitles) {
			header+="@ATTRIBUTE titleOverlap   NUMERIC\n";
		}
		header += "@ATTRIBUTE class        NUMERIC\n"+
				  "@DATA\n";
		return header;

	}
}
