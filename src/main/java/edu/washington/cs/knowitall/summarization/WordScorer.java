package edu.washington.cs.knowitall.summarization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import edu.smu.tspell.wordnet.Synset;
import edu.washington.cs.knowitall.datastructures.Pair;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.utilities.SynsetMapper;

public class WordScorer {
	private SynsetMapper synsetMapper;
	
	private HashMap<Synset, Integer> commonNounMap = new HashMap<Synset, Integer>();
	private HashMap<String, Integer> properNounMap = new HashMap<String, Integer>();
	private Collection<Sentence> sentences;
	
	private HashMap<String, ArrayList<Synset>> sentenceToNounMap = new HashMap<String, ArrayList<Synset>>();
	private HashMap<String, ArrayList<String>> sentenceToProperNounMap = new HashMap<String, ArrayList<String>>();
	private HashMap<String, ArrayList<Synset>> sentenceToVerbMap = new HashMap<String, ArrayList<Synset>>();
	public WordScorer() {
		this.synsetMapper = new SynsetMapper();
	}
	
	public void setup(Collection<Sentence> sentences) {
		// Score sentences
		this.sentences = sentences;
		generateMaps();
	}
	
	private Synset getSynset(String noun) {
		Synset[] synsets = synsetMapper.getSynsetsNoun(noun);
		if (synsets.length == 0) {
			synsets = synsetMapper.getSynsetsNoun(noun.split(" ")[noun.split(" ").length-1]);
		}
		if (synsets.length > 0) {
			Synset synset = synsets[0];
			return synset;
		}
		return null;
	}
	
	private void generateMaps() {
		properNounMap = new HashMap<String, Integer>();
		commonNounMap = new HashMap<Synset, Integer>();
		for (Sentence sentence : sentences) {
			ArrayList<String> properNouns = generateProperNounList(sentence);
			sentenceToProperNounMap.put(sentence.getKey(),  properNouns);
			for (String noun : properNouns) {
				if (!properNounMap.containsKey(noun)) {
					properNounMap.put(noun, 0);
				}
				properNounMap.put(noun, properNounMap.get(noun)+1);
			}
			
			ArrayList<Synset> nouns = generateNounList(sentence);
			sentenceToNounMap.put(sentence.getKey(),nouns);
			for (Synset synset : nouns) {
				if (!commonNounMap.containsKey(synset)) {
					commonNounMap.put(synset, 0);
				}
				commonNounMap.put(synset, commonNounMap.get(synset)+1);
			}

			ArrayList<Synset> verbs = generateVerbList(sentence);
			sentenceToVerbMap.put(sentence.getKey(), verbs);
			for (Synset synset : verbs) {
				if (!commonNounMap.containsKey(synset)) {
					commonNounMap.put(synset, 0);
				}
				commonNounMap.put(synset, commonNounMap.get(synset)+1);
			}
		}
	}
	
	private ArrayList<String> getProperNounList(Sentence sentence) {
		return sentenceToProperNounMap.get(sentence.getKey());
	}
	
	private ArrayList<Synset> getNounList(Sentence sentence) {
		return sentenceToNounMap.get(sentence.getKey());
	}
	
	private ArrayList<Synset> getVerbList(Sentence sentence) {
		return sentenceToVerbMap.get(sentence.getKey());
	}
	
	private ArrayList<String> generateProperNounList(Sentence sentence) {
		ArrayList<String> properNouns = new ArrayList<String>();
		//ArrayList<Pair<Integer, String>> nouns = sentence.getNounsAndProperNouns();
		ArrayList<Pair<Integer, String>> nouns = sentence.getProperNouns();
		for (Pair<Integer, String> nounPair : nouns) {
			int index = nounPair.getFirst();
			String noun = nounPair.getSecond();
			if (sentence.getPos(index).startsWith("NNP")) {
				if (sentence.getNer(index).equals("PERSON")) {
					noun = noun.split(" ")[noun.split(" ").length-1];
				}
				properNouns.add(noun);
			} 
		}

		return properNouns;
	}
	
	private ArrayList<String> getProperNounListActive(Sentence sentence) {
		ArrayList<String> properNouns = new ArrayList<String>();
		//ArrayList<Pair<Integer, String>> nouns = sentence.getNounsAndProperNouns();
		ArrayList<Pair<Integer, String>> nouns = sentence.getActiveProperNouns();
		for (Pair<Integer, String> nounPair : nouns) {
			int index = nounPair.getFirst();
			String noun = nounPair.getSecond();
			if (sentence.getPos(index).startsWith("NNP")) {
				if (sentence.getNer(index).equals("PERSON")) {
					noun = noun.split(" ")[noun.split(" ").length-1];
				}
				properNouns.add(noun);
			} 
		}

		return properNouns;
	}
	
	private ArrayList<Synset> generateNounList(Sentence sentence) {
		ArrayList<Synset> nounList = new ArrayList<Synset>();
		ArrayList<Pair<Integer, String>> nouns = sentence.getNouns();
		for (Pair<Integer, String> nounPair : nouns) {
			String noun = nounPair.getSecond();
			if (!Parameters.NOUN_STOP_LIST.contains(noun)) {
				Synset synset = getSynset(noun);
				if (synset != null) {
					nounList.add(synset);
				}
			}
		}
		return nounList;
	}
	
	private ArrayList<Synset> getNounListActive(Sentence sentence) {
		ArrayList<Synset> nounList = new ArrayList<Synset>();
		ArrayList<Pair<Integer, String>> nouns = sentence.getActiveNouns();
		for (Pair<Integer, String> nounPair : nouns) {
			String noun = nounPair.getSecond();
			if (!Parameters.NOUN_STOP_LIST.contains(noun)) {
				Synset synset = getSynset(noun);
				if (synset != null) {
					nounList.add(synset);
				}
			}
		}
		return nounList;
	}
		
	private ArrayList<Synset> generateVerbList(Sentence sentence) {
		ArrayList<Synset> verbList = new ArrayList<Synset>();
		List<String> verbs = sentence.getVerbs();
		for (String verb : verbs) {
			Synset[] derivationallyRelatedForms = synsetMapper.getDerivationalyRelatedForms(verb);
			if (derivationallyRelatedForms.length > 0) {
				Synset synset = derivationallyRelatedForms[0];
				verbList.add(synset);
			}
		}
		return verbList;
	}
	
	private ArrayList<Synset> getVerbListActive(Sentence sentence) {
		ArrayList<Synset> verbList = new ArrayList<Synset>();
		List<String> verbs = sentence.getActiveVerbs();
		for (String verb : verbs) {
			Synset[] derivationallyRelatedForms = synsetMapper.getDerivationalyRelatedForms(verb);
			if (derivationallyRelatedForms.length > 0) {
				Synset synset = derivationallyRelatedForms[0];
				verbList.add(synset);
			}
		}
		return verbList;
	}
	
	public int scoreProperNoun(String noun) {
		if (!properNounMap.containsKey(noun)) {
			System.err.println(noun);
			return 0;
		}
		int score = properNounMap.get(noun);
		return score;
	}
	
	public int scoreSentenceProperNoun(Sentence sentence, boolean active) {
		ArrayList<String> properNouns;
		if (active) {
			properNouns = getProperNounListActive(sentence);
		} else {
			properNouns = getProperNounList(sentence);
		}
		int properNounScore = 0;
		for (String noun : properNouns) {
			int score = scoreProperNoun(noun);
			properNounScore += score;
		}
		return properNounScore;
	}
	
	public int scoreSentenceNoun(Sentence sentence, boolean active, boolean debug) {
		ArrayList<Synset> nouns;
		if (active) {
			nouns = getNounListActive(sentence);
		} else {
			nouns = getNounList(sentence);
		}
		int nounScore = 0;
		HashSet<Synset> seen = new HashSet<Synset>();
		for (Synset synset : nouns) {
			if (!seen.contains(synset)) {
				int score = commonNounMap.get(synset);
				nounScore += score;
				seen.add(synset);
			}
		}
		return nounScore;
	}
	
	public int scoreSentenceVerb(Sentence sentence, boolean active) {
		int verbScore = 0;
		ArrayList<Synset> verbs;
		if (active) {
			verbs = getVerbListActive(sentence);
		} else {
			verbs = getVerbList(sentence);
		}
		for (Synset synset : verbs) {
			int score = commonNounMap.get(synset);
			verbScore += score;
		}
		return verbScore;
	}
					
	public double getOverlapVerbs(Sentence sentence, Sentence prevSentence) {
		int overlap = 0;
		ArrayList<Synset> verbs1 = getVerbList(prevSentence);
		ArrayList<Synset> nouns2 = getNounList(sentence);
		for (Synset synset : verbs1) {
			if (nouns2.contains(synset)) {
				overlap++;
			}
		}
		return overlap;
	}
	
	public double getOverlapNouns(Sentence sentence, Sentence prevSentence, boolean mustBeBefore, boolean debug) {
		if (mustBeBefore && prevSentence.getSentenceDate().getSecond().getTimeInMillis()-sentence.getSentenceDate().getSecond().getTimeInMillis()>0) {
			return 0;
		}
		int overlap = 0;
		ArrayList<String> properNouns1 = getProperNounList(sentence);
		ArrayList<String> properNouns2 = getProperNounList(prevSentence);
		HashSet<String> counted1 = new HashSet<String>();
		HashSet<String> days = new HashSet<String>();
		days.add("Sunday");
		days.add("Monday");
		days.add("Tuesday");
		days.add("Wednesday");
		days.add("Thursday");
		days.add("Friday");
		days.add("Saturday");
		
		for (String properNoun : properNouns1) {
			if (!counted1.contains(properNoun) && !days.contains(properNoun)) {
				if (properNouns2.contains(properNoun)) {
					overlap++;
				}
				counted1.add(properNoun);
			}
		}
		
		ArrayList<Synset> nouns1 = getNounList(sentence);		
		ArrayList<Synset> nouns2 = getNounList(prevSentence);
		HashSet<Synset> counted = new HashSet<Synset>();
		for (Synset synset : nouns1) {
			if (!counted.contains(synset)) {
				if (nouns2.contains(synset)) {
					overlap++;
				}
				counted.add(synset);
			}
		}
		return overlap;
	}
	
}
