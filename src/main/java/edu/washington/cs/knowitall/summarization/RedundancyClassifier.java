package edu.washington.cs.knowitall.summarization;

import weka.core.Instances;
import weka.classifiers.functions.Logistic;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import edu.washington.cs.knowitall.datastructures.Extraction;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.utilities.ParseTreeHandler;
import edu.washington.cs.knowitall.utilities.TfidfCalculator;
import edu.washington.cs.knowitall.utilities.Utilities;

public class RedundancyClassifier {
	
	private static String[] mortar = {"a", "A", "the", "The", "in","In", "with", "to", "of", "and", "is","has", "down","was", "for","that", "on", "an", "as", "at","by","be", "have"};
	private static Instances data;
	private static Logistic tree = new Logistic();
	private TfidfCalculator calculator = new TfidfCalculator();
	private HashMap<String,Boolean> redundantMap = new HashMap<String,Boolean>();
	private String trainingDataFilename = "data/redundancyTrainingData.arff";
		
	/**
	 * @param training set in arff format with 9 attributes incuding class
	 */
	public void trainClassifier() {
		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(trainingDataFilename));
			data = new Instances(reader);
			reader.close();
		} catch (FileNotFoundException e) {
			System.err.println("Could not find file: "+trainingDataFilename);
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ioexception for file: "+trainingDataFilename);
			e.printStackTrace();
		}
		data.setClassIndex(data.numAttributes() - 1);
		String[] options = new String[1];
		options[0] = "-U";            
		try {
			tree.setOptions(options); 		// set the options
			tree.buildClassifier(data);   	// build classifier
		} catch (Exception e) {
			e.printStackTrace();
		}     
	}
	
	private static int min(int a, int b) {
		if(a<b)
			return a;
		return b;
	}
	
	private static int max(int a, int b) {
		if(a>b)
			return a;
		return b;
	}
	
	//checks if two strings make a similar word
	private static boolean areSimilarWords(String  s1, String s2){
		if(s1.equals(s2))
			return true;
		else if(s1.length()>3&&s2.length()>3){
			if(s1.substring(0, 4).equals(s2.substring(0, 4)))
				return true;
		}
		return false;
	}
	
	//checks if two sentences have atleast one similar word
	private static boolean hasCommonWord(String s1, String s2){
		String[] array1= s1.split(" ");
		String[] array2= s2.split(" ");
		for(int i=0; i<array1.length; i++){
			for(int j=0; j< array2.length; j++){
				if(array2[j].equals(array1[i])&& !Arrays.asList(mortar).contains(array1[i].toLowerCase())&&!Arrays.asList(mortar).contains(array2[j].toLowerCase()))
					return true;
			}
		}
		return false;
	}
	
	private static boolean hasCommonWord(ArrayList<String> a1, ArrayList<String> a2){
		for (String str1 : a1) {
			for (String str2 : a2) {
				if(str1.equals(str2) && 
						!Arrays.asList(mortar).contains(str1.toLowerCase()) &&
						!Arrays.asList(mortar).contains(str2.toLowerCase()))
					return true;
			}
		}
		return false;
	}
	
	
	/**
	 * 
	 * @param s1
	 * @param s2
	 * @return int array of size 5 with count of common noun, adj, verb, pronoun, adverb respectively
	 */
	private static int[] commonPOS(Sentence s1, Sentence s2){
		
		List<String> tokenList1 = s1.getLemmaList();
		List<String> tokenList2 = s2.getLemmaList();
		List<String> posList1 = s1.getPosList();
		List<String> posList2 = s2.getPosList();
		
		int nounCount = 0;
		int adjCount = 0;
		int verbCount = 0;
		int pronounCount = 0;
		int advCount = 0;
		
		String[] nounTags = {"NN", "NNS", "NNP", "NNPS"};
		String[] adjTags = {"JJ", "JJR", "JJS"};
		String[] verbTags = {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"};
		String[] pronounTags = {"PRP", "PRP$"};
		String[] advTags = {"RB", "RBR", "RBS"};
		
		for(int i=0; i< tokenList1.size(); i++){
			
			for(int j=0; j< tokenList2.size(); j++){
				
				if(areSimilarWords(tokenList1.get(i), tokenList2.get(j))){
					
					if(Arrays.asList(nounTags).contains(posList1.get(i))&&Arrays.asList(nounTags).contains(posList2.get(j)))
						nounCount++;
					else if(Arrays.asList(adjTags).contains(posList1.get(i))&&Arrays.asList(adjTags).contains(posList2.get(j)))
						adjCount++;
					else if(Arrays.asList(verbTags).contains(posList1.get(i))&&Arrays.asList(verbTags).contains(posList2.get(j)) && 
							!(tokenList1.get(i).equals("say") || tokenList1.get(i).equals("be") || tokenList1.get(i).equals("have"))) {
						verbCount++;
					}else if(Arrays.asList(pronounTags).contains(posList1.get(i))&&Arrays.asList(pronounTags).contains(posList2.get(j)))
						pronounCount++;
					else if(Arrays.asList(advTags).contains(posList1.get(i))&&Arrays.asList(advTags).contains(posList2.get(j)))
						advCount++;						
				}
			}
		}
		int[] output= {nounCount, adjCount, verbCount, pronounCount, advCount};
		return output;
		
	}

	private static List<String> getCleanList(List<String> list) {
		//used to clean  sentences
		String[] garbage= {".", "'","\"\"","`s","'s","\'\'","``", ",", ":",
							"a", "an", "the", "those", "that", 
							"and", "or", "but",
							"be", "have", 
							"he", "she","them",
							"there",
							"who",
							"in", "with", "to", "of",  "for","that","which","by", "on", "at", "as" };
		
		List<String> tokensClean = new ArrayList<String>();
		for (String str : list) {
			if (!Arrays.asList(garbage).contains(str.toLowerCase())){
				tokensClean.add(str);
			}
		}
		return tokensClean;
	}
	
	private static int getOverlap(List<String> list1, List<String> list2) {
		//Clean token list
		List<String> tokensClean1 = getCleanList(list1);
		List<String> tokensClean2 = getCleanList(list2);
		
		int commonCount= 0;
		for (String fst: tokensClean1) {
			if (!fst.equals("")) {
				for (String snd: tokensClean2) {
					if (fst.equals(snd)) {
						commonCount++;
					}
					else if (fst.length()>3&&snd.length()>3) {
						if (fst.substring(0, 4).equals(snd.substring(0, 4))) {
							commonCount++;
						}
					}
				}
			}
		}
		return commonCount;
	}
	
	/**
	 * @param fstSentence
	 * @param sndSentence
	 * @output generates and dumps features to a temporary arff file in directory with file name "temp.arff"
	 */
	public String getFeatures(Sentence sentence1, Sentence sentence2, boolean debug, int classification) {
		int commonCount = getOverlap(sentence1.getLemmaList(), sentence2.getLemmaList());

    	int numOverlapArg = 0;
    	int numOverlapRel = 0;
    	
    	//calculates overlap for arg and relation
    	for (Extraction extr1 : sentence1.getExtractions()) {
    		String arg11 = extr1.arg1();
			String arg12 = extr1.arg2();
			ArrayList<String> rel1 = extr1.getLemmaRelationSparse();
			for (Extraction extr2 : sentence2.getExtractions()) {
				
    			String arg21 = extr2.arg1();
    			String arg22 = extr2.arg2();
    			ArrayList<String> rel2 = extr2.getLemmaRelationSparse();
    			if(hasCommonWord(rel1, rel2)) {
    				numOverlapRel++;
    			}
    			if(arg11.equals(arg21) || hasCommonWord(arg11, arg21)) {
    				numOverlapArg++;
    			}
    			if(arg12.equals(arg22) || hasCommonWord(arg12, arg22)) {
    				numOverlapArg++;		 
    			}
    		}
    	}
    	//array containing count of common POS tags
    	int[] pos = commonPOS(sentence1, sentence2);
    	int lengthClean = min(getCleanList(sentence1.getLemmaList()).size(), getCleanList(sentence1.getLemmaList()).size());
    	int length = min(sentence1.getLength(), sentence2.getLength());
    	int lengthMax = max(sentence1.getLength(), sentence2.getLength());
    	double lengthRatio = (length+0.0)/lengthMax;
    	// Return the string containing all the features;
    	int longestOverlap = longestOverlap(sentence1, sentence2);
    	double overlapPercent = overlapPercent(sentence1, sentence2);
    	int sameStart = 0;
    	if (sameFirstPart(sentence1, sentence2)) {
    		sameStart = 1;
    	}
    	int sameDoc = 0;
    	if (sentence1.getDocId().equals(sentence2.getDocId())) {
    		sameDoc = 1;
    	}
    	double tfidf = getTfidf(sentence1, sentence2);
    	
    	List<String> subj1 = getCleanList(Arrays.asList(ParseTreeHandler.getSubj(sentence1).split(" ")));
    	List<String> subj2 = getCleanList(Arrays.asList(ParseTreeHandler.getSubj(sentence2).split(" ")));
    	double subjOverlap = getOverlap(subj1, subj2);
    	int minSubjSize = min(subj1.size(), subj2.size());
    	
    	if (subjOverlap > 0 && classification == 0 && !subj1.get(0).equals("") && !subj2.get(0).equals("")) {
    		subjOverlap = subjOverlap/minSubjSize;
    	}
		String features = commonCount*1.0/lengthClean+","+pos[0]*(1.0/length)+","+pos[1]*(1.0/length)+","+pos[2]*(1.0/length)+","+
						pos[3]*(1.0/length)+","+pos[4]*(1.0/length)+","+numOverlapArg+","+numOverlapRel+","+lengthRatio+","+
				longestOverlap+","+overlapPercent+","+sameStart+","+sameDoc+","+tfidf+","+subjOverlap;
		return features;
	}
	
	public static int getArgOverlap(Sentence fstSentence, Sentence sndSentence) {
    	int numOverlapArg=0;
    	
    	//calculates overlap for arg and relation
    	for (Extraction extr1 : fstSentence.getExtractions()) {
    		String arg11 = extr1.arg1();
			String arg12 = extr1.arg2();
		
			for (Extraction extr2 : sndSentence.getExtractions()) {
    			String arg21 = extr2.arg1();
    			String arg22 = extr2.arg2();
    			if(arg11==arg21 || hasCommonWord(arg11, arg21))
    				numOverlapArg++;
    			if(arg12==arg22 || hasCommonWord(arg12, arg22))
    				numOverlapArg++;		 
    		}
    	}
    	return numOverlapArg;
	}
	
	private double getTfidf(Sentence fstSentence, Sentence sndSentence) {
		double sim = calculator.calculateCosineSimilarity(fstSentence.getLemmaList(), sndSentence.getLemmaList(), false);
		return sim;
	}

	public static String getHeader() {
    	String header = "@RELATION redundancy\n"
					+"@ATTRIBUTE wordoverlap NUMERIC\n"
					+"@ATTRIBUTE nounoverlap NUMERIC\n"
					+"@ATTRIBUTE adjoverlap NUMERIC\n"
					+"@ATTRIBUTE verboverlap NUMERIC\n"
					+"@ATTRIBUTE pronounoverlap NUMERIC\n"
					+"@ATTRIBUTE advoverlap NUMERIC\n"
					+"@ATTRIBUTE argoverlap NUMERIC\n"
					+"@ATTRIBUTE reloverlap NUMERIC\n"
					+"@ATTRIBUTE lengthRatio NUMERIC\n"
					+"@ATTRIBUTE longestOverlap NUMERIC\n"
					+"@ATTRIBUTE overlapPercent NUMERIC\n"
					+"@ATTRIBUTE sameStart NUMERIC\n"
					+"@ATTRIBUTE sameDoc NUMERIC\n"
					+"@ATTRIBUTE tfidf NUMERIC\n"
					+"@ATTRIBUTE sameSubj NUMERIC\n"
					+"@ATTRIBUTE class {1.0,0.0}\n\n"
					+"@DATA\n";
    	return header;
	}
	
	private static int longestOverlap(Sentence sentence1, Sentence sentence2) {
		int longestOverlap = Utilities.longestConsecutiveOverlap(sentence1.getTokenListNoNums(), sentence2.getTokenListNoNums());
		return longestOverlap;
	}
	
	private static double overlapPercent(Sentence sentence1, Sentence sentence2) {
		double overlapPercent1 = Utilities.percentOverlap(sentence1.getTokenList(), sentence2.getTokenList());
		return overlapPercent1;
	}
	
	private static boolean sameFirstPart(Sentence sentence1, Sentence sentence2) {
		if(sentence1.getLength() > 4 && sentence2.getLength() > 4 && 
				sentence1.getTokens(0, 5).equals(sentence2.getTokens(0, 5))) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param fstSentence
	 * @param sndSentence
	 * @return if the sentence are redundant
	 * @throws Exception
	 */
	public boolean redundant(Sentence fstSentence, Sentence sndSentence) {
		if (redundantMap.containsKey(fstSentence.getKey()+"::"+sndSentence.getKey())) {
			return redundantMap.get(fstSentence.getKey()+"::"+sndSentence.getKey());
		}
		// Set up the instances.
		String header = getHeader();
		String features = getFeatures(fstSentence, sndSentence, false, -1);
		StringReader testReader = new StringReader(header+features+",0.0"+"\n");
		Instances unlabeled = setupInstances(testReader);
		
		try{
			// label instances
			if (unlabeled.numInstances() > 0) {
				double clsLabel = tree.classifyInstance(unlabeled.instance(0));
				if(clsLabel==0.0) {
					redundantMap.put(fstSentence.getKey()+"::"+sndSentence.getKey(), true);
					redundantMap.put(sndSentence.getKey()+"::"+fstSentence.getKey(), true);
					return true;
				} else {
					redundantMap.put(fstSentence.getKey()+"::"+sndSentence.getKey(), false);
					redundantMap.put(sndSentence.getKey()+"::"+fstSentence.getKey(), false);
					return false;
				}
			}
		 }
		catch (FileNotFoundException e){
			System.err.println("File not found error");
			e.printStackTrace();
		 } catch (Exception e) {
			e.printStackTrace();
		}
		 return false;
	}
	
	public double probabilityRedundant(Sentence fstSentence, Sentence sndSentence) {
		// Set up the instances.
		String header = getHeader();
		String features = getFeatures(fstSentence, sndSentence, false, -1);
		StringReader testReader = new StringReader(header+features+",0.0"+"\n");
		Instances unlabeled = setupInstances(testReader);
		
		try {
			// label instances
			return tree.distributionForInstance(unlabeled.instance(0))[0];
		}
		catch (FileNotFoundException e){
			System.err.println("File not found error");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1.0;
	}
	
	public Instances setupInstances(StringReader testReader) {
		
		Instances instances = null;
		try {
			instances = new Instances(testReader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		instances.setClassIndex(instances.numAttributes() - 1);
		testReader.close();
		return instances;
	}
	
	
}