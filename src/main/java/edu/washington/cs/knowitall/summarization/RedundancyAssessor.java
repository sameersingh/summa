package edu.washington.cs.knowitall.summarization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import edu.washington.cs.knowitall.datastructures.Extraction;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.utilities.Utilities;

public class RedundancyAssessor {
	private HashMap<String,HashMap<String,Boolean>> redundantMap;
	private RedundancyClassifier redundancyClassifier = new RedundancyClassifier();

	public void setup() {
		redundantMap = new HashMap<String,HashMap<String,Boolean>>();
		redundancyClassifier.trainClassifier();
	}
	
	public boolean redundant(Collection<Sentence> currentSummary, Sentence sentence1) {
		if (sentence1 == null) {
			return false;
		}
		for (Sentence sentence2 : currentSummary) {
			if (sentence2 != null) {
				if (redundant(sentence1, sentence2)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean redundant(Sentence sentence1, Sentence sentence2) {
		if (redundantMap.containsKey(sentence1.getKey()) && redundantMap.get(sentence1.getKey()).containsKey(sentence2.getKey())) {
			boolean redundant = redundantMap.get(sentence1.getKey()).get(sentence2.getKey());
			return redundant;
		}
		double time1 = sentence1.getSentenceDate().getSecond().getTimeInMillis()/1000.0/60/60/24;
		double time2 = sentence2.getSentenceDate().getSecond().getTimeInMillis()/1000.0/60/60/24;
		boolean redundant = false;
		double dist = Math.abs(time1-time2);
		if (dist <= 2 && redundancyClassifier.probabilityRedundant(sentence1,  sentence2) > .97) {
			redundant = true;
		}
	
		if (!redundantMap.containsKey(sentence1.getKey())) {
			redundantMap.put(sentence1.getKey(), new HashMap<String,Boolean>());
		}
		redundantMap.get(sentence1.getKey()).put(sentence2.getKey(), redundant);
		return redundant;
	}
	
	public boolean extractionOverlap(Sentence sentence1, Sentence sentence2) {
		List<Extraction> extractions1 = sentence1.getExtractions();
		List<Extraction> extractions2 = sentence2.getExtractions();
		for (Extraction extraction1 : extractions1) {
			for (Extraction extraction2 : extractions2) {
				if (Utilities.overlap(extraction1.getLemmaRelationSparse(), extraction2.getLemmaRelationSparse()) > 0) {
					double hoursApart = (((extraction1.getDate().getTimeInMillis() - extraction2.getDate().getTimeInMillis())/1000)/60)/60;
					if (hoursApart < 48) {
						for (ArrayList<String> argument1 : extraction1.getLemmaArguments()) {
							for (ArrayList<String> argument2 : extraction2.getLemmaArguments()) {
								if (Utilities.overlap(argument1, argument2) > 0) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

	public int longestOverlap(Sentence sentence1, Sentence sentence2) {
		int longestOverlap = Utilities.longestConsecutiveOverlap(sentence1.getTokenListNoNums(), sentence2.getTokenListNoNums());
		return longestOverlap;
	}
	
	public double overlapPercent(Sentence sentence1, Sentence sentence2) {
		double overlapPercent1 = Utilities.percentOverlap(sentence1.getTokenList(), sentence2.getTokenList());
		return overlapPercent1;
	}
	
	public boolean sameFirstPart(Sentence sentence1, Sentence sentence2) {
		if(sentence1.getLength() > 4 && sentence2.getLength() > 4 && 
				sentence1.getTokens(0, 5).equals(sentence2.getTokens(0, 5))) {
			return true;
		}
		return false;
	}

	
}
