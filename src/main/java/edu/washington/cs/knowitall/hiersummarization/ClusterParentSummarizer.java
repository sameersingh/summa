package edu.washington.cs.knowitall.hiersummarization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import edu.washington.cs.knowitall.clustering.Cluster;
import edu.washington.cs.knowitall.datastructures.Pair;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.summarization.Parameters;
import edu.washington.cs.knowitall.summarization.RedundancyAssessor;
import edu.washington.cs.knowitall.utilities.Utilities;

public class ClusterParentSummarizer {
	private HierarchicalScorer scorer;
	private RedundancyAssessor redundancyAssessor = new RedundancyAssessor();
	
	public HashSet<List<String>> triedSummarySentences = new HashSet<List<String>>();
	
	private int maxSummaryLength = Parameters.DEFAULT_LENGTH;	
	private int maxIterations = 50;
	private double searchNextLevel = .25;
	
	private List<Sentence> parentSentences;
	private List<Sentence> prevSentences;
	
	private List<Sentence> sentences;

	public ClusterParentSummarizer(Cluster cluster) {
		redundancyAssessor.setup();
		scorer = new HierarchicalScorer();
		scorer.setup(cluster.getInstanceMap());
		
		this.sentences = new ArrayList<Sentence>();
	}
	
	public void setupScoring(Collection<Sentence> sentences) {
		scorer.scoreOnly(sentences);
	}
	
	public void setupRedundancy() {
		this.redundancyAssessor.setup();
	}
	
	public void setLengthBudget(int maxSummaryLength) {
		this.maxSummaryLength = maxSummaryLength;
	}
	
	public void setMaxIterations(int iterations) {
		this.maxIterations = iterations;
	}
	
	public void setPositiveCoherenceTradeoff(double weight) {
		scorer.setPositiveCoherenceTradeoff(weight);
	}
	
	public void setNegativeCoherenceTradeoff(double weight) {
		scorer.setNegativeCoherenceTradeoff(weight);
	}
	
	public void setSalienceTradeoff(double weight) {
		scorer.setSalienceTradeoff(weight);
	}
	
	private List<Sentence> getValidSortedSentences(List<Sentence> potentialSentences) {
		//  First get the valid sentences
		ArrayList<Sentence> tempSortedSentences = new ArrayList<Sentence>();
		for (Sentence sentence : potentialSentences) {
			if (potentialSentence(prevSentences, sentence)) {
				tempSortedSentences.add(sentence);
			}
		}

		// Next get the top 200 scoring sentences
		Collections.sort(tempSortedSentences, new Comparator<Sentence>(){
			@Override
			public int compare(Sentence sentence1, Sentence sentence2) {
				double score1 = scorer.getSalience(sentence1);
	            double score2 = scorer.getSalience(sentence2);
				if (score1 > score2) {
	            	return -1;
	            } else if (score1 < score2) {
	            	return 1;
	            }
	            return 0;
			}
		});
		ArrayList<Sentence> validSortedSentences = new ArrayList<Sentence>();
		if (tempSortedSentences.size() < 200) {
			validSortedSentences = tempSortedSentences;
		} else {
			for (int i = 0; i < 200; i++) {
				validSortedSentences.add(tempSortedSentences.get(i));
			}
		}
		
		// Last sort by length
		Collections.sort(validSortedSentences, new Comparator<Sentence>(){
			@Override
			public int compare(Sentence sentence1, Sentence sentence2) {
				int score1 = sentence1.getBytes();
	            int score2 = sentence2.getBytes();
				if (score1 < score2) {
	            	return -1;
	            } else if (score1 > score2) {
	            	return 1;
	            }
	            return 0;
			}
		});
		
		return validSortedSentences;
	}

	private boolean potentialSentence(Collection<Sentence> summary, Sentence sentence) {
		if (summary.contains(sentence)) {
			return false;
		}
		if (redundancyAssessor.redundant(summary,sentence)) {
			return false;
		}
		return true;
	}
	
	public boolean shortEnough(int currentSummaryLength, Sentence sentence1) {
		if (sentence1.getBytes() + currentSummaryLength > maxSummaryLength)  {
			return false;
		}
		return true;
	}
	
	public boolean shortEnough(List<Sentence> curSummary, Sentence sentence) {
		if (Utilities.calculateLength(curSummary)+sentence.getBytes() > maxSummaryLength)  {
			return false;
		}
		return true;
	}
	
	public List<Sentence> getSortedSentences(Collection<Sentence> sentences, List<Sentence> prevSentences, boolean debug) {
		return scorer.getSortedSentences(sentences, prevSentences, maxSummaryLength, debug);
	}
	
	public boolean redundant(Sentence sentence1, List<Sentence> otherSentences) {
		for (Sentence sentence2 : otherSentences) {
			if (redundancyAssessor.redundant(sentence1, sentence2)) {
				return true;
			}
		}
		return false;
	}
	
	public double scoreSummaryParentSentence(List<Sentence> summary, boolean debug) {
		return scorer.scoreSummaryParentSentence2(summary, parentSentences, prevSentences, debug);
	}
	
	
	/********** Probabilistic summary **********/
	
	private List<Sentence> getProbabilisiticSummary(Cluster cluster) {
		List<Sentence> summary = new ArrayList<Sentence>();
		
		// for each cluster
		for (Cluster childCluster : cluster.getChildren()) {
			HashMap<Sentence,Double> sentenceScoring = new HashMap<Sentence,Double>();
			for (Sentence sentence : childCluster.getValidInstances()) {
				// score adding in the sentence
				boolean potential = potentialSentence(prevSentences, sentence);
				if (potential) { 
					summary.add(sentence);
					//double score = scorer.scoreSummary(summary, false);
					double score = scorer.scoreSummaryParentSentenceSub(summary, parentSentences, prevSentences, false);
					sentenceScoring.put(sentence, score);
					summary.remove(sentence);
				}
			}
			
			Sentence sentence = Utilities.chooseProbabilistic(sentenceScoring);
			summary.add(summary.size(), sentence);
		}
		return summary;
	}
	
	private List<Sentence> getProbabilisiticSummary(List<Sentence> sentences) {
		List<Sentence> summary = new ArrayList<Sentence>();
		
		// while the summary length is less than it could be
		while (Utilities.calculateLength(summary) < maxSummaryLength && summary.size() < 4) {
			HashMap<Sentence,Double> sentenceScoring = new HashMap<Sentence,Double>();
			for (Sentence sentence : sentences) {
				boolean shortEnough = shortEnough(Utilities.calculateLength(summary),sentence);
				if (!shortEnough) {
					break;
				} else {
					// score adding in the sentence
					if (potentialSentence(summary, sentence)) {
						summary.add(sentence);
						double score = scorer.scoreSummaryParentSentenceSub(summary, parentSentences, prevSentences, false);
						sentenceScoring.put(sentence, score);
						summary.remove(sentence);
					}
				}
			}
			Sentence sentence = Utilities.chooseProbabilistic(sentenceScoring);
			if (sentence == null) {
				break;
			} 
			summary.add(summary.size(), sentence);
		}
		return summary;
	}
	
	/********* Next summary from current summary **********/
	
	private Pair<List<Sentence>,Double> compareToBest(List<Sentence> bestSummary, double bestScore, List<Sentence> tempSummary, double tempScore) {
		if (tempScore > bestScore) {
			bestScore = tempScore;
			if (bestSummary != null) {
				bestSummary.clear();
				bestSummary.addAll(tempSummary);
			} else {
				bestSummary = new ArrayList<Sentence>(tempSummary);
			}	
		}
		return new Pair<List<Sentence>,Double> (bestSummary,bestScore);
	}
	
	private List<String> getKey(List<Sentence> summary) {
		ArrayList<String> key = new ArrayList<String>();
		for (Sentence sentence : summary) {
			key.add(sentence.getKey());
		}
		return key;
	}
	
	private Pair<List<Sentence>, Double> tryAddingSentence(List<Sentence> sentences, List<Sentence> bestSummary, double bestScore, 
			List<Sentence> summary, int depth) {
		// For each replacement sentence
		for (Sentence newSentence : sentences) {
			// Check if we can add this sentence
			if (!shortEnough(Utilities.calculateLength(summary),newSentence)) {
				break;
			}
			if (potentialSentence(summary, newSentence)) {
				// Try adding the sentence in
				List<Sentence> tempSummary = new ArrayList<Sentence>(summary);
				tempSummary.add(newSentence);

					// Score summary.
					double tempScore = scorer.scoreSummaryParentSentenceSub(tempSummary, parentSentences, prevSentences, false);
					Pair<List<Sentence>,Double> bestPair = compareToBest(bestSummary, bestScore, tempSummary, tempScore);
					bestSummary = bestPair.getFirst();
					bestScore = bestPair.getSecond();

					// Possibly try adding in another sentence.
					if (Math.random() < searchNextLevel && depth < 1 && tempSummary.size() < 4) {
						bestPair = tryAddingSentence(sentences, bestSummary, bestScore, tempSummary, depth+1);
						bestSummary = bestPair.getFirst();
						bestScore = bestPair.getSecond();
					}

			} 
		}
		return new Pair<List<Sentence>, Double>(bestSummary, bestScore);
	}
	
	private Pair<List<Sentence>, Double> tryAddingSentence(List<Cluster> clusters, List<Sentence> bestSummary, double bestScore, List<Sentence> summary, int index, boolean debug) {
		int removeIndex = index;
		Sentence removedSentence = summary.remove(removeIndex);
		int currentSummaryLength = Utilities.calculateLength(summary);
		List<String> key = getKey(summary);
		
		// For each replacement sentence
		for (Sentence newSentence : clusters.get(index).getValidInstances()) {
			if (potentialSentence(prevSentences, newSentence) &&
					potentialSentence(summary, newSentence) && shortEnough(currentSummaryLength,newSentence)) {
				// Try adding it in
				List<Sentence> tempSummary = new ArrayList<Sentence>(summary);
				List<String> tempKey = new ArrayList<String>(key);
				tempSummary.add(removeIndex, newSentence);
				tempKey.add(removeIndex, newSentence.getKey());

				if (!triedSummarySentences.contains(tempKey)) {
					triedSummarySentences.add(tempKey);
					// Score summary. 
					double tempScore = scorer.scoreSummaryParentSentenceSub(tempSummary, parentSentences, prevSentences, debug);
					Pair<List<Sentence>,Double> bestPair = compareToBest(bestSummary, bestScore, tempSummary, tempScore);
					bestSummary = bestPair.getFirst();
					bestScore = bestPair.getSecond();
				} 
			}
		}
		summary.add(removeIndex, removedSentence);

		return new Pair<List<Sentence>, Double>(bestSummary, bestScore);
	}
	
	private List<Sentence> getNextSummary(List<Sentence> sentences, List<Sentence> summary, double lastScore) {
		double bestScore = lastScore;
		List<Sentence> bestSummary = null;
		
		// Get all the combinations we're going to try.
		List<List<Sentence>> summaryCombinations = Utilities.getCombinations(summary, 2, 0);
		
		// For each combination of sentences, try adding in the replacement sentence and another set
		for (List<Sentence> summaryCombination : summaryCombinations) {
			if (summaryCombination.size() < 4 && summaryCombination.size() > 1) {
				Pair<List<Sentence>,Double> bestPair = tryAddingSentence(sentences, bestSummary, bestScore, summaryCombination, 0);
				bestSummary = bestPair.getFirst();
				bestScore = bestPair.getSecond();
			}
		}
		return bestSummary;
	}
	
	private List<Sentence> getNextSummary(Cluster cluster, List<Sentence> summary, double lastScore, boolean debug) {
		double bestScore = lastScore;
		List<Sentence> bestSummary = null;
		// For each cluster, try replacing the sentence
		for (int i = 0; i < cluster.getChildren().size(); i++) {
			Pair<List<Sentence>,Double> bestPair = tryAddingSentence(cluster.getChildren(), bestSummary, bestScore, summary, i, debug);
			bestSummary = bestPair.getFirst();
			bestScore = bestPair.getSecond();
		}
		return bestSummary;
	}
	
	/******** Generate the cluster summary ***********/
	
	public List<Sentence> generateClusterSummary(Cluster cluster, List<Sentence> parentSentences, List<Sentence> prevSentences, boolean debug) {
		this.parentSentences = parentSentences;
		this.prevSentences = prevSentences;

		scorer.reset();
		triedSummarySentences = new HashSet<List<String>>();
		
		if (cluster.getChildren().size() == 0) {
			sentences = getValidSortedSentences(cluster.getValidInstances());
		}
		// Get the starting summary
		List<Sentence> summary;
		if (cluster.getChildren().size() == 0) {
			summary = getProbabilisiticSummary(sentences);
		} else {
			summary = getProbabilisiticSummary(cluster);
		}	
		if (summary.size() == 0) {
			return null;
		}

		double bestScore = scorer.scoreSummaryParentSentenceSub(summary, parentSentences, prevSentences, false);
		List<Sentence> bestSummary = summary;
		double lastScore = bestScore;
		
		// For each iteration, try to modify the current summary or generate a new summary
		for (int iteration = 0; iteration < maxIterations; iteration++) {
			if (cluster.getChildren().size() == 0) {
				summary = getNextSummary(sentences, summary, lastScore);
			} else {
				summary = getNextSummary(cluster, summary, lastScore, false);
			}
			
			if (summary == null) {
				// reset the summary
				if (cluster.getChildren().size() == 0) {
					summary = getProbabilisiticSummary(sentences);
				} else {
					summary = getProbabilisiticSummary(cluster);
				}
			}
			
			lastScore = scorer.scoreSummaryParentSentenceSub(summary, parentSentences, prevSentences, false);
			if (lastScore > bestScore) {
				bestScore = lastScore;
				bestSummary = summary;
				
			}	
			if (triedSummarySentences.size() > 10000000) {
				break;
			}
		}
		return bestSummary;
	}

	public double getMaxLength() {
		return maxSummaryLength;
	}

	public double getSalience(Sentence sentence) {
		return scorer.getSalience(sentence);
	}

	
	
}
