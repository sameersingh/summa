package edu.washington.cs.knowitall.hiersummarization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import edu.washington.cs.knowitall.clustering.Cluster;
import edu.washington.cs.knowitall.datastructures.Pair;
import edu.washington.cs.knowitall.datastructures.Sentence;

public class HierParentSummarizer {
	
	private List<Summary> sortSummaries(ArrayList<Summary> summaries) {
		Collections.sort(summaries, new Comparator<Summary>(){
			@Override
			public int compare(Summary summary1, Summary summary2) {
				Double score1 = summary1.getScore();
				Double score2 = summary2.getScore();
				if (score1 < score2) {
	            	return 1;
	            } else if (score1 > score2) {
	            	return -1;
	            }
	            return 0;
			}
		});
		return summaries;
	}
	
	private static Pair<Double,List<Sentence>> recursiveSummarizeByParent(ClusterParentSummarizer summarizer, Cluster cluster, List<Sentence> parentSentences, List<Sentence> prevSentences, boolean debug) {
		
		double distance = (0.0+cluster.getMaxDate().getTimeInMillis()-cluster.getMinDate().getTimeInMillis())/1000/60/60/24;
		if (distance < 2) {
			distance = 2;
		}
		if (distance > 20) {
			distance = 20;
		}
		summarizer.setPositiveCoherenceTradeoff((1/distance));
		List<Sentence> childSummary = summarizer.generateClusterSummary(cluster, parentSentences, prevSentences, debug);
		if (childSummary == null) {
			return null;
		}
		cluster.setSummary(childSummary);
		double score = summarizer.scoreSummaryParentSentence(childSummary, false);
		return new Pair<Double,List<Sentence>>(score,childSummary);
	}
	
	private boolean validSummarySentence(ClusterParentSummarizer parentSummarizer, Cluster root, int index, 
			Sentence parentSentence, List<Sentence> prevSentences, List<Sentence> curRootSummary) {
		// (! dateRequirement || (parentSentence.containsDate() || parentSentence.getSentenceId() == 0)
		boolean valid = false;
		Cluster cluster = root.getChildren().get(index);
		if (parentSentence.getBytes() <= parentSummarizer.getMaxLength()*(1.0/root.getChildren().size())*1.5 &&
				parentSummarizer.shortEnough(curRootSummary, parentSentence) &&
				(cluster.getChildren().size() == 0 || cluster.getChildren().get(0).getInstances().contains(parentSentence)) && 
				!parentSummarizer.redundant(parentSentence, prevSentences) ) {
				valid = true;
		}

		return valid;
	}
	
	private List<Pair<Sentence,Double>> identifyBestSentence(ClusterParentSummarizer parentSummarizer, 
			Cluster root, int index, List<Sentence> parentSentences, List<Sentence> prevSentences, 
			List<Sentence> curRootSummary, int limit, boolean debug) {
		// Set up the summarizer.
		parentSummarizer.setupRedundancy();
		Cluster cluster = root.getChildren().get(index);
		List<Sentence> sortedSentences = parentSummarizer.getSortedSentences(cluster.getValidInstances(), prevSentences, debug);
		List<Pair<Sentence,Double>> scores = new ArrayList<Pair<Sentence,Double>>();
		int tried = 0;
		int i = 0;
		boolean allTooShort = true;
		while ((i < sortedSentences.size() && tried < limit)) {
			Sentence parentSentence = sortedSentences.get(i);
			boolean valid = validSummarySentence(parentSummarizer, root, index, parentSentence, prevSentences, curRootSummary);
			if (valid) {
				// recursive summarize
				allTooShort = false;
				prevSentences.add(parentSentence);
				parentSentences.add(parentSentence);
				Pair<Double,List<Sentence>> scoreAndSummary = recursiveSummarizeByParent(parentSummarizer, cluster,  parentSentences, prevSentences,false);
				prevSentences.remove(parentSentence);
				parentSentences.remove(parentSentence);
				if (scoreAndSummary != null) {
					double score = scoreAndSummary.getFirst();
					scores.add(new Pair<Sentence,Double>(parentSentence, score));
					tried++;
				}
			}
			i++;
			if (i >= sortedSentences.size() && allTooShort) {
				break;
			}
		}
		return scores;
	}
	
	private List<Summary> generateSummarySub(ClusterParentSummarizer parentSummarizer, 
			Cluster root, Summary curSummaryObj, List<Sentence> parentSentences, int index, int limit, boolean debug) {
		List<Sentence> curSummary = curSummaryObj.getSummary();
		double curScore = curSummaryObj.getScore();

		ArrayList<Sentence> prevSentences = new ArrayList<Sentence>();
		prevSentences.addAll(parentSentences);
		prevSentences.addAll(curSummary);
		List<Pair<Sentence,Double>> sentencesAndScores = identifyBestSentence(parentSummarizer, root, index, parentSentences, prevSentences, curSummary, limit, debug);

		ArrayList<Summary> summaries = new ArrayList<Summary>();
		for (int j = 0; j < sentencesAndScores.size(); j++) {
			Pair<Sentence,Double> sentenceAndScore = sentencesAndScores.get(j);
			Sentence sentence = sentenceAndScore.getFirst();
			double tempScore = sentenceAndScore.getSecond()+curScore;
			List<Sentence> tempSummary = new ArrayList<Sentence>();
			tempSummary.addAll(curSummary);
			tempSummary.add(sentence);
			summaries.add(new Summary(tempSummary, tempScore));
		}
		return summaries;
	}
	
	public Cluster generateFirstLevel(ClusterParentSummarizer parentSummarizer, Cluster root) {

		
		// Choose the right sentences for the first level.
		List<Summary> summariesToTest = new ArrayList<Summary>();
		summariesToTest.add(new Summary(new ArrayList<Sentence>(),0));
		ArrayList<Sentence> parentSentences = new ArrayList<Sentence>();
		
		// For each child of the root
		for (int i = 0; i < root.getChildren().size(); i++) {
			parentSummarizer.setupScoring(root.getChildren().get(i).getValidInstances());
			
			int count = 0;
			ArrayList<Summary> tempList = new ArrayList<Summary>();
			
			// For each of the current summaries that should be tested
			for (int j = 0; j < summariesToTest.size(); j++) {
				Summary curSummaryObj = summariesToTest.get(j);
				int limit = 10;
				if (i == 0) {
					limit = 20;
				}
				List<Summary> newSummaries = generateSummarySub(parentSummarizer, root, curSummaryObj, parentSentences, i, limit, (i == 1));
				tempList.addAll(newSummaries);
				if (newSummaries.size() > 0) {
					count++;
				}
				if (count > 4) {
					break;
				}
			}
			summariesToTest = sortSummaries(tempList);
		}
		
		if (summariesToTest.size() > 0) {
			root.setSummary(summariesToTest.get(0).getSummary());
		}
		return root;
	}
	
	public Cluster generateSecondLevel(ClusterParentSummarizer parentSummarizer, Cluster root, int i) {
		Cluster cluster = root.getChildren().get(i);
		parentSummarizer.setupScoring(cluster.getInstances());
		
		// Choose the right sentence for each subcluster.
		List<Summary> summariesToTest = new ArrayList<Summary>();
		summariesToTest.add(new Summary(new ArrayList<Sentence>(),0));
		ArrayList<Sentence> parentSentences = new ArrayList<Sentence>();
		parentSentences.add(root.getSummary().get(i));
		for (int j = 0; j < cluster.getChildren().size(); j++) {
			ArrayList<Summary> tempList = new ArrayList<Summary>();
			int count = 0;
			for (int k = 0; k < summariesToTest.size(); k++) {
				Summary curSummaryObj = summariesToTest.get(k);
				List<Summary> newSummaries = generateSummarySub(parentSummarizer, cluster, curSummaryObj, parentSentences, j, 10, false);
				tempList.addAll(newSummaries);
				if (newSummaries.size() > 0) {
					count++;
				}
				if (count > 4) {
					break;
				}
			}
			summariesToTest = sortSummaries(tempList);
		}
		if (summariesToTest.size() > 0) {
			cluster.setSummary(summariesToTest.get(0).getSummary());
			generateThirdLevel(parentSummarizer, cluster, parentSentences);
		} 
		return root;
	}
	
	public Cluster generateThirdLevel(ClusterParentSummarizer parentSummarizer, Cluster cluster, ArrayList<Sentence> parentSentences ) {
		parentSummarizer.setMaxIterations(30);
		// Choose the right summaries for the third level
		for (int j = 0; j < cluster.getChildren().size(); j++) {	
			Cluster childCluster = cluster.getChildren().get(j);
			parentSentences.add(cluster.getSummary().get(j));
			ArrayList<Sentence> prevSentences = new ArrayList<Sentence>();
			for (int k = 0; k < j; k++) {
				prevSentences.add(cluster.getSummary().get(k));
			}
			prevSentences.addAll(parentSentences);

			Pair<Double,List<Sentence>> scoreAndSummary = recursiveSummarizeByParent(parentSummarizer, childCluster,  parentSentences, prevSentences,true);
			parentSentences.remove(cluster.getSummary().get(j));
			cluster.getChildren().get(j).setSummary(scoreAndSummary.getSecond());
		}
		return cluster;
	}
	
	public Cluster generateSecondAndThirdLevel(ClusterParentSummarizer parentSummarizer, Cluster root) {
		// Choose the right sentences for the second level.
		for (int i = 0; i < root.getChildren().size(); i++) {		
			parentSummarizer.setMaxIterations(10);
			root = generateSecondLevel(parentSummarizer, root, i);
		}
		return root;
	}

	public Cluster generateSummary(Cluster root) {
		ClusterParentSummarizer parentSummarizer = new ClusterParentSummarizer(root);
		parentSummarizer.setPositiveCoherenceTradeoff(.1);
		parentSummarizer.setNegativeCoherenceTradeoff(1.0);
		parentSummarizer.setupScoring(root.getValidInstances());
		
		// Generate the first level
		Cluster revisedCluster = generateFirstLevel(parentSummarizer, root);
		revisedCluster = generateSecondAndThirdLevel(parentSummarizer, revisedCluster);
		return revisedCluster;
	}
	
    private class Summary {
        private List<Sentence> summary;
        private double score;
        
        public Summary(List<Sentence> summary, double score) {
        	this.summary = summary;
        	this.score = score;
        }
        
        public List<Sentence> getSummary() {
        	return summary;
        }
        public double getScore() {
        	return score;
        }
    }
}
