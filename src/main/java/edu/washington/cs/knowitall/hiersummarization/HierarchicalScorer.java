package edu.washington.cs.knowitall.hiersummarization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import edu.washington.cs.knowitall.datastructures.CoherenceGraph;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.datastructures.SentenceEdge;
import edu.washington.cs.knowitall.summarization.Parameters;
import edu.washington.cs.knowitall.summarization.SalienceCalculator;
import edu.washington.cs.knowitall.summarization.WordScorer;
import edu.washington.cs.knowitall.utilities.Compressor;
import edu.washington.cs.knowitall.utilities.Utilities;

public class HierarchicalScorer {
	private double salTradeoff = 1.0;
	private double posCohTradeoff = Parameters.POSCOH_TRADEOFF;
	private double negCohTradeoff = Parameters.NEGCOH_TRADEOFF;
	private double lengthTradeoff = Parameters.LENGTH_TRADEOFF;
	private double firstSentenceSalTradeoff = 2.0;
	private double firstSentenceNegCohTradeoff = -1000;
	
	private SalienceCalculator salienceCalculator;
	private CoherenceGraph coherenceGraph;
	private WordScorer wordScorer;
	
	HashMap<String, Double> scoreMap = new HashMap<String, Double>();
	HashMap<String, Double> firstSentenceScoreMap = new HashMap<String, Double>();
	HashMap<String, Double> negCoherenceMap = new HashMap<String, Double>();
	public HashMap<String, Double> posCoherenceMap = new HashMap<String, Double>();
	Compressor compressor = new Compressor();
	
	double maxSalienceScore = -1;
	
	public HierarchicalScorer() {
		wordScorer = new WordScorer();
		salienceCalculator = new SalienceCalculator(wordScorer, true);
		coherenceGraph = new CoherenceGraph();
	}
	
	public void setup(HashMap<String,Sentence> sentences) {
		coherenceGraph.setup(sentences);
		wordScorer.setup(sentences.values());
		salienceCalculator.setup(sentences.values());
		scoreMap = new HashMap<String, Double>();
		firstSentenceScoreMap = new HashMap<String, Double>();
		maxSalienceScore = salienceCalculator.getMaxSalienceScore();
	}
	
	public void reset() {
		posCoherenceMap.clear();
		negCoherenceMap.clear();
	}
	
	public void scoreOnly(Collection<Sentence> sentences) {
		wordScorer.setup(sentences);
		salienceCalculator.setup(sentences);
		scoreMap = new HashMap<String, Double>();
		firstSentenceScoreMap = new HashMap<String, Double>();
		maxSalienceScore = salienceCalculator.getMaxSalienceScore();
	}
		
	public List<Sentence> getSortedSentences(Collection<Sentence> validSentences, List<Sentence> prevSentences, int maxSummaryLength, boolean debug) {
		List<Sentence> sortedSentences = new ArrayList<Sentence>();
		for (Sentence sentence : validSentences) {
			debug = false;
			coherenceGraph.getInEdges(sentence);
			Set<String> necessaryWords = getNecessaryWords(prevSentences, sentence, debug);
			if (necessaryWords.size() == 0  && sentence.getBytes() < .5*maxSummaryLength) {
				sortedSentences.add(sentence);
			}
		}
		
		Collections.sort(sortedSentences,  new Comparator<Sentence>() {
			public int compare(Sentence sentence1, Sentence sentence2) {
				double score1 = salienceCalculator.getSalience(sentence1);
				double score2 = salienceCalculator.getSalience(sentence2);
				if (score1 > score2) {
					return -1;
				} else if (score2 > score1) {
					return 1;
				}
				return 0;
			}
			
		});
		return sortedSentences;
	}
	
	public Sentence getFirstSentence(Collection<Sentence> validSentences, int maxSummaryLength) {
		Sentence firstSentence = null;
		double maxScore = Integer.MIN_VALUE;
		for (Sentence sentence : validSentences) {
			double score = salienceCalculator.getSalience(sentence);
			Set<String> necessaryWords = coherenceGraph.getNecessaryWords(sentence, false);
			if ((necessaryWords.size() == 0 || negCohTradeoff == 0) && (score > maxScore || salTradeoff == 0) && sentence.getBytes() < .5*maxSummaryLength) {
				firstSentence = sentence;
				maxScore = score;
			}
		}
		return firstSentence;
	}
	
	public Set<String> inLinks(Sentence sentence) {
		Set<String> necessaryWords = coherenceGraph.getNecessaryWords(sentence, false);
		return necessaryWords;
	}
	
	/****************** Salience ******************/
	
	public double getSalience(Sentence sentence) {
		return salienceCalculator.getSalience(sentence);
	}
	
	
	/****************** Negative Coherence ******************/
	
	private Set<String> removeNecessaryWords(HashMap<Sentence,List<SentenceEdge>> tempEdges, 
			List<Sentence> prevSentences, Set<String> necessaryWords) {
		for (Sentence necessarySentence : tempEdges.keySet()) {
			if (prevSentences.contains(necessarySentence)) {
				for (SentenceEdge inEdge : tempEdges.get(necessarySentence)) {
					necessaryWords.remove(inEdge.getReason());
					break;
				}
			}
			if (necessaryWords.size() == 0) {
				 break;
			 }
		}
		return necessaryWords;
	}
	
	private Set<String> removeNecessaryWords2(HashMap<Sentence,List<SentenceEdge>> tempEdges, 
			List<Sentence> prevSentences, Set<String> necessaryWords, boolean debug) {
		for (Sentence necessarySentence : tempEdges.keySet()) {
			for (SentenceEdge inEdge : tempEdges.get(necessarySentence)) {
				if (necessarySentence != null) {
					for (Sentence prevSentence : prevSentences) {
						if (prevSentence == necessarySentence || prevSentence.getLemmaList().contains(inEdge.getReason())) {
							necessaryWords.remove(inEdge.getReason());
							break;
						} 
					}
				}
				if (necessaryWords.size() == 0) {
					 break;
				 }
			}
		}
		return necessaryWords;
	}
	
	private Set<String> removeNecessaryWords3(HashMap<Sentence,List<SentenceEdge>> tempEdges, 
			List<Sentence> prevSentences, Set<String> necessaryWords) {
		for (Sentence prevSentence : prevSentences) {
			for (Sentence sentence2 : tempEdges.keySet()) {
				for (SentenceEdge edge1 : tempEdges.get(sentence2)) {
					if (edge1.getSynonyms() != null) {
						for (String synonym : edge1.getSynonyms()) {
							if (Utilities.containsString(prevSentence.getSentenceStr(), synonym)) {
								necessaryWords.remove(edge1.getReason());
								if (necessaryWords.size() == 0) {
									return necessaryWords;
								}
							}
						}
					}
				}
			}
		}
		return necessaryWords;

	}
	
	private Set<String> getNecessaryWords(List<Sentence> prevSentences, Sentence sentence, boolean debug) {
		Set<String> necessaryWords = coherenceGraph.getNecessaryWords(sentence, debug);
		if (prevSentences.size() == 0) {
			return necessaryWords;
		}
		
		HashMap<Sentence,List<SentenceEdge>> tempEdges = coherenceGraph.getInEdgesTransition(sentence);
		necessaryWords = removeNecessaryWords(tempEdges, prevSentences, necessaryWords);
		
		if (necessaryWords.size() != 0) {
			tempEdges = coherenceGraph.getInEdgesNounVerb(sentence);
			necessaryWords = removeNecessaryWords(tempEdges, prevSentences, necessaryWords);
		}
		
		if (necessaryWords.size() != 0) {	
			tempEdges = coherenceGraph.getInEdgesNoun(sentence);
			necessaryWords = removeNecessaryWords2(tempEdges, prevSentences, necessaryWords, debug);
		}
		
		if (necessaryWords.size() != 0) {
			tempEdges = coherenceGraph.getInEdgesNecNounVerb(sentence);
			necessaryWords = removeNecessaryWords2(tempEdges, prevSentences, necessaryWords, debug);
		}
		
		if (necessaryWords.size() != 0) {
			tempEdges = coherenceGraph.getInEdgesPerson(sentence);
			necessaryWords = removeNecessaryWords(tempEdges, prevSentences, necessaryWords);
		}
		
		if (necessaryWords.size() != 0) {
			tempEdges = coherenceGraph.getInEdgesPerson(sentence);
			necessaryWords = removeNecessaryWords3(tempEdges, prevSentences, necessaryWords);
		}
		
		
			return necessaryWords;
	}
	
	private double getNegativeCoherence(List<Sentence> prevSentences, Sentence sentence, boolean debug) {
		if (negCoherenceMap.containsKey(sentence.getKey())) {
			return negCoherenceMap.get(sentence.getKey());
		}
		Set<String> necessaryWords = getNecessaryWords(prevSentences, sentence, false);
		negCoherenceMap.put(sentence.getKey(),(double) necessaryWords.size());
		return necessaryWords.size();
	}
	
	private double getNegativeCoherence(Sentence prevSentence, Sentence sentence, boolean debug) {
		if (sentence == null) {
			return 0.0;
		}
		ArrayList<Sentence> tempList = new ArrayList<Sentence>();
		if (prevSentence != null) {
			tempList.add(prevSentence);
		}
		return getNegativeCoherence(tempList, sentence, debug);
	}
	
	/****************** Positive Coherence ******************/
	
	private double getPositiveCoherence(Sentence prevSentence, Sentence sentence, boolean useMap, boolean debug) {
		if (prevSentence == null || sentence == null) {
			return 0.0;
		}
		
		if (!debug && useMap && posCoherenceMap.containsKey(sentence.getKey())) {
			double coherence = posCoherenceMap.get(sentence.getKey());
			return coherence;
		}
		
		// Coherence in edges (transition, person, nouns, noun->verb)
		double score = 0.0;
		List<SentenceEdge> inEdges = coherenceGraph.getInEdges(sentence);
		for (SentenceEdge edge : inEdges) {
			Sentence sourceSentence = edge.getSource();
			if (sourceSentence != null) {
				if (prevSentence.equals(sourceSentence)) {
					score += edge.getWeight();
				}
			}
		}
		
		// Entity continuous
		double entityContinuanceScore = 0;
		double verbScore = 0;
		//verbScore = wordScorer.getOverlapVerbs(sentence, prevSentence);
		double nounScore = wordScorer.getOverlapNouns(sentence, prevSentence, true, debug);
		entityContinuanceScore = verbScore + nounScore;
		
		if (useMap) {
			posCoherenceMap.put(sentence.getKey(), score+entityContinuanceScore);
		}
		return score+entityContinuanceScore;
	}
	
	/****************** Scoring ******************/
	
	/**
	 * Scores the adjacent pairs of sentences.
	 * @param prevSentence
	 * @param sentence
	 * @param debug
	 * @return
	 */
	public double scoreSummary(Sentence prevSentence, Sentence sentence, boolean debug) {
		String key = "";
		if (prevSentence != null) {
			key += prevSentence.getKey()+"::";
		} else {
			key += "null::";
		}
		if (sentence != null) {
			key += sentence.getKey();
		} else {
			key += "null";
		}
		
		boolean containsKey = scoreMap.containsKey(key);
		if (containsKey && !debug) {
			return scoreMap.get(key);
		} 
		
		// calculate salience
		double salience = salienceCalculator.getSalience(sentence);
		
		// calculate the positive coherence
		double positiveCoherence = getPositiveCoherence(prevSentence, sentence,false, debug);
		
		// calculate the negative coherence
		double negativeCoherence = getNegativeCoherence(prevSentence, sentence, debug);
		
		double score = 0;
		score = salience + posCohTradeoff*positiveCoherence - negCohTradeoff*negativeCoherence;

		scoreMap.put(key, score);
		return score;
	}
	
	/**
	 * Scores just the first sentence in the summary.
	 * @param sentence
	 * @param debug
	 * @return
	 */
	public double scoreSummaryFirstSentence(Sentence sentence, boolean debug) {
		if (sentence == null) {
			return 0.0;
		}
		String key = sentence.getKey();
		
		boolean containsKey = firstSentenceScoreMap.containsKey(key);
		if (containsKey && !debug) {
			return firstSentenceScoreMap.get(key);
		} 
		
		// calculate salience
		double salience = salienceCalculator.getSalience(sentence);
		
		// calculate the positive coherence (must be 0 because there is no proceeding sentence)
		double positiveCoherence = 0;
		
		// calculate the negative coherence
		double negativeCoherence = getNegativeCoherence(new ArrayList<Sentence>(), sentence, debug);
		
		double score = firstSentenceSalTradeoff*salience + posCohTradeoff*positiveCoherence + firstSentenceNegCohTradeoff*negativeCoherence;

		firstSentenceScoreMap.put(key, score);
		return score;
	}
	
	/**
	 * scoreSummary() scores the summary by scoring pairwise adjacent sentences.
	 * @param sentences the summary
	 * @param debug
	 * @return
	 */
	public double scoreSummary(List<Sentence> sentences, boolean debug) {
		int firstSentenceIndex = 0;
		for (int i = 0; i < sentences.size(); i++) {
			if (sentences.get(i) != null) {
				firstSentenceIndex = i;
				break;
			}
		}
		// Score the first sentence.
		double score = scoreSummaryFirstSentence(sentences.get(firstSentenceIndex), debug);
		// Score each pair of adjacent sentences.
		for (int i = firstSentenceIndex+1; i < sentences.size(); i++) {
			score += scoreSummary(sentences.get(i-1), sentences.get(i), debug);
		}
		return score;
	}
	
	public double scoreSummaryParentSentence(List<Sentence> summary, List<Sentence> parentSentences, List<Sentence> prevSentences, boolean debug) {
		// Score each pair of sentences to extraSentence
		double score = 0.0;
		for (Sentence sentence : parentSentences) {
			score += salienceCalculator.getSalience(sentence);
		}
		
		for (int i = 0; i < parentSentences.size()-1; i++) {
			score += getPositiveCoherence(parentSentences.get(i), parentSentences.get(i+1), false, false);
		}
		
		Sentence parentSentence = parentSentences.get(parentSentences.size()-1);
		for (Sentence sentence : summary) {
			if (sentence != null) {
				// calculate salience
				double salience = salienceCalculator.getSalience(sentence);

				// calculate the positive coherence
				double positiveCoherence = getPositiveCoherence(parentSentence, sentence, false, debug);
				//if (positiveCoherence > 2) positiveCoherence = 2;
				
				// calculate the negative coherence
				double negativeCoherence = getNegativeCoherence(prevSentences, sentence, debug);
				
				score += salience + posCohTradeoff*positiveCoherence - negCohTradeoff*negativeCoherence;
			}
		}
		return score;
	}
	
	public double scoreSummaryParentSentence2(List<Sentence> summary, List<Sentence> parentSentences, List<Sentence> prevSentences, boolean debug) {
		// Score each pair of sentences to extraSentence
		double score = 0.0;
		for (Sentence sentence : parentSentences) {
			score += salienceCalculator.getSalience(sentence);
		}
		
		for (int i = 0; i < parentSentences.size()-1; i++) {
			score += getPositiveCoherence(parentSentences.get(i), parentSentences.get(i+1), false, false);
		}
		
		Sentence parentSentence = parentSentences.get(parentSentences.size()-1);
		for (Sentence sentence : summary) {
			boolean nullSentence = (sentence == null);
			if (!nullSentence) {
				// calculate salience
				double salience = salienceCalculator.getSalience(sentence);

				// calculate the positive coherence
				double positiveCoherence = getPositiveCoherence(parentSentence, sentence, true, debug);

				// calculate the negative coherence
				double negativeCoherence = getNegativeCoherence(prevSentences, sentence, debug);
				
				score += salience + posCohTradeoff*positiveCoherence - negCohTradeoff*negativeCoherence;
			}
		}
		return score;
	}
	
	public double scoreSummaryParentSentenceSub(List<Sentence> summary, List<Sentence> parentSentences, List<Sentence> prevSentences, boolean debug) {
		Sentence parentSentence = parentSentences.get(parentSentences.size()-1);
		double score = 0.0;
		for (Sentence sentence : summary) {
			boolean nullSentence = (sentence == null);
			if (!nullSentence) {
				// calculate salience
				double salience = salienceCalculator.getSalience(sentence);

				// calculate the positive coherence
				double positiveCoherence = getPositiveCoherence(parentSentence, sentence, true, debug);
				
				// calculate the negative coherence
				double negativeCoherence = getNegativeCoherence(prevSentences, sentence, debug);
				
				score += salience + posCohTradeoff*positiveCoherence - negCohTradeoff*negativeCoherence;
			}
		}
		//score += floorSalience*sentences.size()*salTradeoff;
		return score;
	}
	
	
	/**
	 * scoreSummaryParts scores the first part and the last part of a summary, skipping
	 * over the given index. This allows us to add a sentence to the summary and not
	 * recalculate the full score, just the part affected by adding a sentence.
	 * 
	 * @param sentences - the summary
	 * @param index - the index that the sentence will be added at.
	 * @param debug
	 * @return
	 */
	public double scoreSummaryParts(List<Sentence> sentences, int index, boolean debug) {
		double score = 0;
		// If we don't add the sentence to the beginning, calculate the firstSentence score.
		if (index != 0) {
			score += scoreSummaryFirstSentence(sentences.get(0), debug);
		}
		// Calculate the rest of the score with pairwise adjacent calculations for example
		// given this set of sentences and index=2:
		// sentenceA
		// sentenceB
		// sentenceD
		// sentenceE
		// This gets score(sentenceA, sentenceB)+score(sentenceD, sentenceE)
		for (int i = 1; i < sentences.size(); i++) {
			if (i != index ) {
				score += scoreSummary(sentences.get(i-1), sentences.get(i), debug);
			}
		}
		return score;
	}
	
	/**
	 * scoreSubSummary calculates the new score of a summary when just a single sentence
	 * has been added. It uses a baseScore so that it doesn't have to do the full calculation
	 * but can just do the change.
	 * 
	 * @param sentences the summary
	 * @param index the index of the new sentence
	 * @param baseScore the score of the old summary
	 * @param debug
	 * @return the score
	 */
	public double scoreSubSummary(List<Sentence> sentences, int index, double baseScore, boolean debug) {
		double score = 0.0;
		// If we added the sentence at the first position, we calculate the firstSentence score.
		if (index == 0) {
			score += scoreSummaryFirstSentence(sentences.get(0), debug);
			
		// Otherwise, we score the sentence pair of the previous sentence and the current sentence.
		// For example, assume we add sentenceC at index=2 to the previous example:
		// score(sentenceB, sentenceC)
		} else {
			score += scoreSummary(sentences.get(index-1), sentences.get(index), debug);
		}
		
		// Next we score the sentence pair of the current sentence and the next sentence.
		// i.e. score(sentenceC, sentenceD)
		if (index < sentences.size()-1) {
			score += scoreSummary(sentences.get(index), sentences.get(index+1), debug);
		}
		
		// Add the length tradeoff to the score.
		score -= lengthTradeoff*sentences.size()*maxSalienceScore;
		
		// Add the baseScore to the score (i.e. the score of all the other adjacent pairs in the sentence)
		score = baseScore + score;
		return score;
	}
	
	/****************** Scoring Weights ******************/
	
	public void setSalienceTradeoff(double weight) {
		this.salTradeoff = weight;
	}
	
	public void setPositiveCoherenceTradeoff(double weight) {
		this.posCohTradeoff = weight;
	}
	
	public void setNegativeCoherenceTradeoff(double weight) {
		this.negCohTradeoff = weight;
	}
	
	public void setLengthTradeoff(double weight) {
		this.lengthTradeoff = weight;
	}
}
