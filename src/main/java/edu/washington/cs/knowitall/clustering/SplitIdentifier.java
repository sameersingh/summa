package edu.washington.cs.knowitall.clustering;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jblas.util.Random;

import edu.washington.cs.knowitall.datastructures.Pair;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.utilities.Utilities;

public class SplitIdentifier {
	private static double C = .3;
	private static int minK = 3;
	private static int maxK = 5;
	private static int B = 20;
	public long evaluateTime1 = 0;
	public long evaluateTime2 = 0;
	public long evaluateTime3 = 0;
	public long setTime = 0;
	public long referenceTime = 0;
	
	private static ArrayList<Calendar> sortSplits(ArrayList<Calendar> splitList) {
		Collections.sort(splitList, new Comparator<Calendar>(){
			@Override
			public int compare(Calendar date1, Calendar date2) {
				if (date1.getTimeInMillis() < date2.getTimeInMillis()) {
					return -1;
				} else if (date1.getTimeInMillis() > date2.getTimeInMillis()) {
					return 1;
				}
	            return 0;
			}
		});
		return splitList;
	}
	
	private double evaluateSplit(HashMap<Long,List<Sentence>> sortedSentences, ArrayList<Long> sortedDates, Collection<Calendar> splits, int size, boolean debug) {
		// Calculate the first part
		long start = System.currentTimeMillis();
		double value1Temp = 0;
		for (Calendar split : splits) {
			for (int i = 0; i < sortedDates.size(); i++) {
				if (sortedDates.get(i) == split.getTimeInMillis()) {
					int count1=0, count3=0;
					if (i > 0) {
						count1 = sortedSentences.get(sortedDates.get(i-1)).size();
					} 
					double count2 = sortedSentences.get(sortedDates.get(i)).size();
					if (i < sortedDates.size()-1) {
						count3 = sortedSentences.get(sortedDates.get(i+1)).size();
						count2 = (count2+count3)/2.0;
					}
					double tempValue = (count1)-(count2);
					value1Temp += tempValue;
				}
			}
		}
		if (value1Temp > 0) value1Temp = 0;
		double value1 = -value1Temp/(size*splits.size());
		evaluateTime1 += System.currentTimeMillis()-start;
		

		start = System.currentTimeMillis();
		// Calculate the second part
		Calendar minDate = sortedSentences.get(sortedDates.get(0)).get(0).getRoundedSentenceDate().getSecond();
		Calendar maxDate = sortedSentences.get(sortedDates.get(sortedDates.size()-1)).get(0).getRoundedSentenceDate().getSecond();
		ArrayList<Calendar> splitList = new ArrayList<Calendar>(splits);
		if (!splitList.contains(minDate)) {
			splitList.add(minDate);
		}
		splitList.add(maxDate);
		splitList = sortSplits(splitList);
		
		double value2Temp = Integer.MAX_VALUE;
		for (int i = 0; i < splitList.size()-1; i++) {
			int tempValue = (int) (splitList.get(i+1).getTimeInMillis()/1000/60/60/24-splitList.get(i).getTimeInMillis()/1000/60/60/24);
			if (tempValue < value2Temp) {
				value2Temp = tempValue;
			}
		}
		int daysDiff = (int) ((sortedDates.get(sortedDates.size()-1)-sortedDates.get(0))/1000/60/60/24);
		double value2 = C*value2Temp/daysDiff;
		//value2 = C*value2;
		//double value = value1-value2;
		double value = value1+value2;
		evaluateTime2 += System.currentTimeMillis()-start;
		
		
		start = System.currentTimeMillis();
		ArrayList<Calendar> sortedSplits = new ArrayList<Calendar>();
		for (Calendar split : splits) {
			sortedSplits.add(split);
		}
		Collections.sort(sortedSplits);
		for (int i = 0; i < sortedSplits.size(); i++) {
			Calendar split = sortedSplits.get(i);
			int totalSentences = 0;
			for (int j = 0; j < sortedDates.size(); j++) {
				long curDate = sortedDates.get(j);
				if (curDate >= split.getTimeInMillis() && (i == sortedSplits.size()-1 || curDate < sortedSplits.get(i+1).getTimeInMillis())) {
					totalSentences += sortedSentences.get(curDate).size();
				}
			}
			if (totalSentences < 10) {
				if (totalSentences < 10) {
					value = .0000000001;
				} 
			}
		}
		evaluateTime3 += System.currentTimeMillis()-start;
		// HACK
		if (value <= 0) {
			value = .0000000001;
		}
		value = value*1000;
		return value;
	}
	
	private static boolean withinX(Set<Calendar> curSet, Calendar instance, int x) {
		for (Calendar cal : curSet){
			if ((Math.abs(cal.getTimeInMillis() - instance.getTimeInMillis())/1000/60/60/24) < x) {
				return true;
			}
		}
		return false;
	}
	

	
	private static Set<Set<Calendar>> getSetsOfDatesRecursive2(ArrayList<Calendar> dateList, HashSet<Calendar> curSet, int maxSize, int i, int level, int x) {
		Set<Set<Calendar>> totalSet = new HashSet<Set<Calendar>>();
		if (curSet.size() >= maxSize ) {
			totalSet.add(curSet);
			return totalSet;
		} else if (i < dateList.size()) {
			if (!withinX(curSet, dateList.get(i), x)) {
				curSet.add(dateList.get(i));
				for (int j = i+1; j < dateList.size()+1; j++) {
					HashSet<Calendar> tempSet = new HashSet<Calendar>(curSet);
					totalSet.addAll(getSetsOfDatesRecursive2(dateList, tempSet, maxSize, j, level+1, x));
				}
			}
		}
		return totalSet;
	}
	
	private Set<Set<Calendar>> getSetsOfDates(ArrayList<Calendar> dateList, int maxSize) {
		int dateLength = (int) ((dateList.get(dateList.size()-1).getTimeInMillis()-dateList.get(0).getTimeInMillis())/1000/60/60/24);
		int x = dateLength/10;
		Set<Set<Calendar>> dateSet = new HashSet<Set<Calendar>>();
		for (int j = 0; j < dateList.size(); j++) {
			HashSet<Calendar> tempSet = new HashSet<Calendar>();
			Set<Set<Calendar>> tempSetOfSets = getSetsOfDatesRecursive2(dateList, tempSet, maxSize, j, 0, x);
			dateSet.addAll(tempSetOfSets);
			if (dateSet.size() > 1000000) {
				break;
			}
		}
		return dateSet;
	}
	
	private static ArrayList<Calendar> getListOfDates(HashMap<Long,List<Sentence>> sortedSentences) {
		// Get the potential dates

		ArrayList<Long> datesInMillis = new ArrayList<Long>(sortedSentences.keySet());
		Collections.sort(datesInMillis);
		ArrayList<Calendar> dateList = new ArrayList<Calendar>();
		for (Long date : datesInMillis) {
			dateList.add(sortedSentences.get(date).get(0).getRoundedSentenceDate().getSecond());
		}		
		return dateList;
	}
	
	public Pair<Double,ArrayList<Cluster>> cluster(Collection<Sentence> sentences, int k, Set<Set<Calendar>> splitsList, boolean debug ) {
		List<Sentence> validSentences = new ArrayList<Sentence>();
		for (Sentence sentence : sentences) {
			if (sentence.getValid()) {
				validSentences.add(sentence);
			}
		}
		
		HashMap<Long,List<Sentence>> sortedSentences = Utilities.sortSentencesByDateAndDocument(validSentences, true, false);
		HashMap<Long,List<Sentence>> sortedSentencesAll = Utilities.sortSentencesByDateAndDocument(sentences, true, false);
		ArrayList<Calendar> dateList = getListOfDates(sortedSentences);

		// Remove the earliest date from consideration as a split
		Calendar earliestDate = dateList.get(0);
		dateList.remove(earliestDate);

		if (k > sortedSentences.size()) {
			k = sortedSentences.size();
		}
		
		if (dateList.size() < k-1) {
			return null;
		}
		
		// Identify the best splits
		Set<Calendar> bestSplits;
		double value = 0;

		if (k == 1) {
			bestSplits = new HashSet<Calendar>();
			bestSplits.add(new GregorianCalendar());
		} else {
			// Generate the possible splits
			if (splitsList == null) {
				splitsList = getSetsOfDates(dateList, k-1);
			}

			ArrayList<Long> dates = new ArrayList<Long>(sortedSentences.keySet());
			Collections.sort(dates);
			
			// Choose the best split from the options
			double maxValue = -Integer.MAX_VALUE;
			bestSplits = null;
			int dateLength = (int) ((dateList.get(dateList.size()-1).getTimeInMillis()-dateList.get(0).getTimeInMillis())/1000/60/60/24);
			int x = dateLength/10;
			if (x > 3) {
				x = 3;
			}
			for (Set<Calendar> splits : splitsList) {
				if (!withinX(splits, earliestDate, x)) {
					splits.add(earliestDate);
					double totalValue = evaluateSplit(sortedSentences, dates, splits, sentences.size(), false);
					if (totalValue > maxValue) {
						maxValue = totalValue;
						bestSplits = splits;
					} 
				}
			
			}
			if (bestSplits == null) {
				value = -1;
			} else {
				value = evaluateSplit(sortedSentences, dates, bestSplits, sentences.size(), debug);
			}
		}
		if (value > 0) {
			ArrayList<Cluster> clusters = generateClustersFromSpilts(sortedSentencesAll, bestSplits);
			return new Pair<Double,ArrayList<Cluster>>(value,clusters);
		} else {
			return new Pair<Double,ArrayList<Cluster>>(value,null);
		}

	}
	
	private static ArrayList<Cluster> generateClustersFromSpilts(HashMap<Long,List<Sentence>> sortedSentences, Set<Calendar> splits) {
		ArrayList<Calendar> splitList = new ArrayList<Calendar>(splits);
		splitList = sortSplits(splitList);
		
		ArrayList<Cluster> clustersTemp = new ArrayList<Cluster>();
		for (int i = 0; i < splits.size()+1; i++) {
			clustersTemp.add(new Cluster());
		}
		for (Long date : sortedSentences.keySet()) {
			boolean added = false;
			for (int i = 0; i < splitList.size(); i++) {
				if (date < splitList.get(i).getTimeInMillis()) {
					for (Sentence sentence : sortedSentences.get(date)) {
						clustersTemp.get(i).add(sentence);
					}
					added = true;
					break;
				}
			}
			if (!added) {
				for (Sentence sentence : sortedSentences.get(date)) {
					clustersTemp.get(clustersTemp.size()-1).add(sentence);
				}
			}
		}
		
		ArrayList<Cluster> clusters = new ArrayList<Cluster>();
		for (Cluster cluster : clustersTemp) {
			if (cluster.getInstances().size() > 0) {
				clusters.add(cluster);
			}
		}
		return clusters;
	}
	
	public ArrayList<Cluster> clusterHierarchically(Collection<Sentence> sentences, Cluster parent, int k, int level) {
		ArrayList<Cluster> clusters = cluster(sentences, k, null, false).getSecond();
		if (clusters.size() == 1) {
			return clusters;
		}
		
		for (int i = 0; i < clusters.size(); i++) {
			Cluster child = clusters.get(i);
			child.setParent(parent);
			if (parent != null) {
				parent.setChild(child);
			}
			clusterHierarchically(child.getInstances(), child, k, level+1);
		}
		
		return clusters;
	}
	
	private double getStandardDeviation(ArrayList<Integer> counts) {
		// Average
		double average = 0;
		for (int count : counts) {
			average += count;
		}
		average = average/counts.size();
		
		// Difference from mean
		double averageOfDiff = 0;
		for (int count : counts) {
			averageOfDiff += (average-count)*(average-count);
		}
		averageOfDiff = averageOfDiff/counts.size();
		
		// Square root of average of differences
		double stdev = Math.sqrt(averageOfDiff);
		return stdev;
	}
	
	private List<Sentence> generateReferenceSet(Collection<Sentence> sentences) {
		int average = 0;
		ArrayList<Integer> diffs = new ArrayList<Integer>();
		HashMap<Long, List<Sentence>> dateMap = Utilities.sortSentencesByDateAndDocument(sentences, true, false);
		ArrayList<Long> sortedDates = new ArrayList<Long>(dateMap.keySet());
		Collections.sort(sortedDates);
		int countLast = 0;
		for (int i = 0; i < sortedDates.size(); i++) {
			long millis = sortedDates.get(i);
			Calendar cal = new GregorianCalendar();
			cal.setTimeInMillis(millis);
			int numSentences = dateMap.get(millis).size();
			average += numSentences;
			if (i > 0) {				
				int diff = Math.abs(numSentences-countLast);
				diffs.add(diff);	
				countLast = numSentences;
			}
		}
		double stdev = getStandardDeviation(diffs);
		average = average/dateMap.keySet().size();
	
		Calendar earliest = null;
		Calendar latest = new GregorianCalendar(1970, 0, 1);
		for (Sentence sentence : sentences) {
			Calendar curDate = sentence.getSentenceDate().getSecond();
			if (earliest == null ||
					curDate.getTimeInMillis() < earliest.getTimeInMillis()) {
				earliest = curDate;
			}
			if (latest == null ||
					curDate.getTimeInMillis() > latest.getTimeInMillis()) {
				latest.setTimeInMillis(curDate.getTimeInMillis());
			}
		}
		latest.set(Calendar.DATE, latest.get(Calendar.DATE)+1);
		
		ArrayList<Sentence> referenceSet = new ArrayList<Sentence>();

		Calendar tempDate = new GregorianCalendar(earliest.get(Calendar.YEAR), earliest.get(Calendar.MONTH), earliest.get(Calendar.DATE));
		int count = average;
		addX(referenceSet, tempDate, count, 0);
		tempDate.set(Calendar.DATE, tempDate.get(Calendar.DATE)+1);
		int last = count;
		while (tempDate.getTimeInMillis() <= latest.getTimeInMillis()) {
			int increment = (int) (Random.nextGaussian()*stdev);
			if (increment < 0 && last <= 0) {
				increment*=-1;
			}
			count = last+increment;
			referenceSet = addX(referenceSet, tempDate, count, referenceSet.size());
			tempDate.set(Calendar.DATE, tempDate.get(Calendar.DATE)+1);
			last = count;
		}
		return referenceSet;
	}
	
	private ArrayList<Sentence> addX(ArrayList<Sentence> list, Calendar date, int times, int start) {
		for (int i = 0; i < times; i++) {
			Calendar dateRounded = new GregorianCalendar(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), 0,0,0);
			Sentence temp = new Sentence();
			temp.setDocumentId("temp");
			temp.setSentenceId(i+start);
			temp.setSentenceDate(new Pair<Integer,Calendar>(0,dateRounded));
			list.add(temp);
		}
		return list;
	}
	
	public List<Cluster> chooseK(Collection<Sentence> sentences) { 
		HashMap<Long,List<Sentence>> sortedSentences = Utilities.sortSentencesByDateAndDocument(sentences, true, false);
		if (sortedSentences.size() < minK) {
			return null;
		}
		if (sortedSentences.size() < maxK) {
			maxK = sortedSentences.size();
		}
		long time1 = System.currentTimeMillis();
		
		// Cluster the observed data.
		double[] withinDisp = new double[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {
			Pair<Double,ArrayList<Cluster>> clusterPair = cluster(sentences, k, null, true);
			if (clusterPair.getFirst() < 0) {
				maxK = k-1;
			} else {
				withinDisp[k-minK] = clusterPair.getFirst();
			}
 		}
		Calendar minDate = null;
		Calendar maxDate = null;
		for (Sentence sentence : sentences) {
			if (minDate == null || sentence.getSentenceDate().getSecond().getTimeInMillis() < minDate.getTimeInMillis()) {
				minDate = sentence.getSentenceDate().getSecond();
			}
			if (maxDate == null || sentence.getSentenceDate().getSecond().getTimeInMillis() > maxDate.getTimeInMillis()) {
				maxDate = sentence.getSentenceDate().getSecond();
			}
		}		

		// Generate B reference data sets and cluster each.
		double[][] withinDispRef = new double[maxK-minK+1][B];
		for (int k = minK; k <= maxK; k++) {
			Set<Set<Calendar>> splitsList = null;
			Calendar earliestDate = null;
			for (int b = 0; b < B; b++) {
				time1 = System.currentTimeMillis();
				List<Sentence> refSet = generateReferenceSet(sentences);
				minDate = refSet.get(0).getSentenceDate().getSecond();
				maxDate = refSet.get(0).getSentenceDate().getSecond();
				for (Sentence sentence : refSet) {
					if (sentence.getSentenceDate().getSecond().getTimeInMillis() < minDate.getTimeInMillis()) {
						minDate = sentence.getSentenceDate().getSecond();
					}
					if (sentence.getSentenceDate().getSecond().getTimeInMillis() > maxDate.getTimeInMillis()) {
						maxDate = sentence.getSentenceDate().getSecond();
					}
				}
				
				referenceTime += System.currentTimeMillis()-time1;
				if (splitsList == null) {
					sortedSentences = Utilities.sortSentencesByDateAndDocument(refSet, true, false);
					ArrayList<Calendar> dateList = getListOfDates(sortedSentences);
					// Remove the earliest date from consideration as a split
					earliestDate = dateList.get(0);
					dateList.remove(earliestDate);
					time1 = System.currentTimeMillis();
					splitsList = getSetsOfDates(dateList, k-1);
					setTime+=System.currentTimeMillis()-time1;
				} else {
					for (Set<Calendar> set : splitsList) {
						set.remove(earliestDate);
					}
				}
				boolean debug = false;
				//if (b == 0) debug = true;
				
				Pair<Double,ArrayList<Cluster>> clusterPair = cluster(refSet, k, splitsList, debug);
				withinDispRef[k-minK][b] = clusterPair.getFirst();
			}
		}
		
		// Compute the estimated gap statistic.
		double[] gapStat = new double[maxK-minK+1];
		double[] l = new double[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {	
			double sumRefDisp = 0.0;
			for (int b = 0; b < B; b++) {
				sumRefDisp += Math.log(withinDispRef[k-minK][b]);
			}
			l[k-minK] = 1.0/B * sumRefDisp;
			//gapStat[k-minK] = 1.0/B * sumRefDisp - Math.log(withinDisp[k-minK]);
			gapStat[k-minK] = Math.log(withinDisp[k-minK])-1.0/B * sumRefDisp;
		}
		
		// Compute the standard deviation.
		double[] sdk = new double[maxK-minK+1];
		double[] sk = new double[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {
			double sum = 0.0;
			for (int b = 0; b < B; b++) {
				sum += Math.pow((Math.log(withinDispRef[k-minK][b]) - l[k-minK]),2);
			}
			sdk[k-minK] = Math.pow((1.0/B) * sum, .5);
			sk[k-minK] = sdk[k-minK]*Math.pow(1+(1.0/B), .5);
		}
		
		// Choose the number of clusters.
		int chosenK = maxK;
		for (int k = minK; k < maxK; k++) {
			if (gapStat[k-minK] >= (gapStat[k+1-minK]-sk[k+1-minK])) {
				chosenK = k;
				break;
			}
		}
		Pair<Double,ArrayList<Cluster>> clusterPair = cluster(sentences, chosenK, null, false);
		return clusterPair.getSecond();
	}
	
	private Cluster clusterHierarchicallyRecursive(Collection<Sentence> sentences, int maxLevel, int curLevel, Cluster cluster) {
		if (curLevel == maxLevel) {
			return cluster;
		}
		List<Cluster> children = chooseK(cluster.getInstances());
		if (children == null) {
			return cluster;
		}
		cluster(cluster.getInstances(), children.size(), null, true);
		for (Cluster child : children) {
			cluster.setChild(child);
			child.setParent(cluster);
			child = clusterHierarchicallyRecursive(sentences, maxLevel, curLevel+1, child);
		}
		return cluster;
	}
	
	public List<Cluster> clusterHierarchically(Collection<Sentence> sentences, int maxLevel) {
		List<Cluster> firstLevel = chooseK(sentences);
		if (firstLevel == null) {
			return null;
		}
		cluster(sentences, firstLevel.size(), null, true);
		
		for (Cluster cluster : firstLevel) {
			cluster = clusterHierarchicallyRecursive(sentences, maxLevel, 1, cluster);
		}
		return firstLevel;
	}
	
	public static Calendar earliestDate(List<Sentence> sentences) {
		Calendar earliest = sentences.get(0).getSentenceDate().getSecond();
		HashMap<Long, List<Sentence>> sortedSentences = Utilities.sortSentencesByDateAndDocument(sentences, true, true);
		for (Long time : sortedSentences.keySet()) {
			if (time < earliest.getTimeInMillis() && sortedSentences.get(time).size() > 5) {
				earliest = sortedSentences.get(time).get(0).getRoundedSentenceDate().getSecond();
			}
		}
		return earliest;
	}
	
	public static Calendar latestDate(List<Sentence> sentences) {
		Calendar latest = sentences.get(0).getSentenceDate().getSecond();
		HashMap<Long, List<Sentence>> sortedSentences = Utilities.sortSentencesByDateAndDocument(sentences, true, true);
		for (Long time : sortedSentences.keySet()) {
			if (time > latest.getTimeInMillis() && sortedSentences.get(time).size() > 5) {
				latest = sortedSentences.get(time).get(0).getRoundedSentenceDate().getSecond();
			}
		}
		return latest;
	}
	
	
	public static List<Sentence> removeOutliers(HashMap<String,Sentence> sentenceMap) {
		ArrayList<Sentence> sentences = new ArrayList<Sentence>();
		sentences.addAll(sentenceMap.values());
		List<Sentence> instancesToRemove = new ArrayList<Sentence>();
		Calendar earliestDate = earliestDate(sentences);
		for (Sentence instance : sentences) {
			if (instance.getRoundedSentenceDate().getSecond().before(earliestDate)) {
				instancesToRemove.add(instance);
			}
		}
		sentences.removeAll(instancesToRemove);
		
		
		// Remove sentences marked too late
		instancesToRemove = new ArrayList<Sentence>();
		Calendar latestDate = latestDate(sentences);
		for (Sentence instance : sentences) {
			if (instance.getRoundedSentenceDate().getSecond().after(latestDate)) {
				instancesToRemove.add(instance);
			}
		}
		sentences.removeAll(instancesToRemove);
		return sentences;
	}
}
