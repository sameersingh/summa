package edu.washington.cs.knowitall.clustering;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.utilities.Utilities;

public class Cluster {
	private ArrayList<Sentence> instances = new ArrayList<Sentence>();
	private Cluster parent;
	private ArrayList<Cluster> children = new ArrayList<Cluster>();
	private List<Sentence> summary;
	private HashMap<String,Sentence> instanceMap = new HashMap<String,Sentence>();
	
	public void setParent(Cluster parent) {
		this.parent = parent;
	}
	
	public Cluster getParent() {
		return parent;
	}
	
	public void setChild(Cluster child) {
		children.add(child);
	}
	
	public List<Cluster> getChildren() {
		return children;
	}
	
	public ArrayList<Sentence> getInstances() {
		return instances;
	}
	
	public HashMap<String,Sentence> getInstanceMap() {
		return instanceMap;
	}

	public void addInstances(Collection<Sentence> instances) {
		this.instances.addAll(instances);
		for (Sentence instance : instances) {
			this.instanceMap.put(instance.getKey(), instance);
		}
	}
	
	public void add(Sentence instance) {
		instances.add(instance);
		this.instanceMap.put(instance.getKey(), instance);
	}
	
	public void clear() {
		instances.clear();
	}

	public String getDates() {
		long minDate = System.currentTimeMillis();
		long maxDate = 0;
		for (Sentence sentence : instances) {
			Calendar sentenceDate = sentence.getSentenceDate().getSecond();
			if (sentenceDate.getTimeInMillis() > maxDate) {
				maxDate = sentenceDate.getTimeInMillis();
			}
			if (sentenceDate.getTimeInMillis() < minDate) {
				minDate = sentenceDate.getTimeInMillis();
			}
		}
		Calendar minCalendar = new GregorianCalendar();
		minCalendar.setTimeInMillis(minDate);
		Calendar maxCalendar = new GregorianCalendar();
		maxCalendar.setTimeInMillis(maxDate);
		
		return Utilities.printDate(minCalendar)+" - "+Utilities.printDate(maxCalendar);
	}

	public void setSummary(List<Sentence> summary) {
		this.summary = summary;
	}
	
	public List<Sentence> getSummary() {
		return summary;
	}

	public Calendar getMinDate() {
		long minDate = System.currentTimeMillis();
		for (Sentence sentence: instances) {
			if (sentence.getRoundedSentenceDate().getSecond().getTimeInMillis() < minDate) {
				minDate = sentence.getRoundedSentenceDate().getSecond().getTimeInMillis();
			}
		}
		Calendar minCalendar = new GregorianCalendar();
		minCalendar.setTimeInMillis(minDate);
		return minCalendar;
	}
	
	public Calendar getMaxDate() {
		long maxDate = 0;
		for (Sentence sentence: instances) {
			if (sentence.getRoundedSentenceDate().getSecond().getTimeInMillis() > maxDate) {
				maxDate = sentence.getRoundedSentenceDate().getSecond().getTimeInMillis();
			}
		}
		Calendar maxCalendar = new GregorianCalendar();
		maxCalendar.setTimeInMillis(maxDate);
		return maxCalendar;
	}

	public List<Sentence> getValidInstances() {
		ArrayList<Sentence> validSentences = new ArrayList<Sentence>();
		for (Sentence sentence : instances) {
			if (sentence.getValid()) {
				validSentences.add(sentence);
			}
		}
		return validSentences;
	}
	
}
