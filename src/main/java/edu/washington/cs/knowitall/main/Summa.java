package edu.washington.cs.knowitall.main;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import edu.stanford.nlp.trees.Tree;
import edu.washington.cs.knowitall.clustering.Cluster;
import edu.washington.cs.knowitall.clustering.SplitIdentifier;
import edu.washington.cs.knowitall.datastructures.Sentence;
import edu.washington.cs.knowitall.hiersummarization.HierParentSummarizer;
import edu.washington.cs.knowitall.utilities.Reader;
import edu.washington.cs.knowitall.utilities.Utilities;

public class Summa {
	
	public static List<Sentence> getValidSentences(Collection<Sentence> sentences) {
		ArrayList<Sentence> validSentences = new ArrayList<Sentence>();
		HashMap<String, ArrayList<Sentence>> sortedSentences = Utilities.sortByDocument(sentences);
		for (String docId : sortedSentences.keySet()) {
			boolean debug = false;
			ArrayList<Sentence> currentSentences = sortedSentences.get(docId);
			String all = "";
			for (int i = 0; i < currentSentences.size(); i++) {
				Sentence sentence = currentSentences.get(i);

				if (debug) System.out.println(sentence.getSentenceStr());
				String[] elements = (" "+sentence.getSentenceStr()+" ").split("''");
				int inQuoteWords = 0;
				int outQuoteWords = 0;
				for (int j = 0; j < elements.length; j++) {
					if (j < elements.length-1) {
						all += elements[j] + "''";
					} else {
						all += elements[j];
					}
					String element = elements[j].trim();
					if (all.split("''").length%2 == 0) {
						if (!element.equals("")) 
							inQuoteWords += element.split(" ").length;
					} else {
						if (!element.equals("")) 
							outQuoteWords += element.split(" ").length;
					}
				}
				if (sentence.getValid() && inQuoteWords < outQuoteWords && validSentence(sentence, false)) {
					if (getRootVerb(sentence).equals("is") || getRootVerb(sentence).equals("are")) {
						sentence.setValid(false);
					} else {
						sentence.setValid(true);
					}
				}  else {
					sentence.setValid(false);
				}
				validSentences.add(sentence);
			}
		}
		return validSentences;
	}
	
	private static boolean validSentence(Sentence sentence, boolean debug) {
		if (sentence.getTokenList().contains("we") || 
				sentence.getTokenList().contains("We") ||
				sentence.getTokenList().contains("you") || 
				sentence.getTokenList().contains("You") || 
				sentence.getTokenList().contains("I") ) {
			return false;
		}
		
		// if it's a question
		String sentenceStr = sentence.getSentenceStr();
		if (sentenceStr.charAt(sentenceStr.length()-1) == '?') {
			return false;
		}
		
		// if it's obviously not a complete sentence
		if (sentenceStr.substring(0,1).toLowerCase().equals(sentenceStr.substring(0,1)) ) {
			return false;
		} 
		
		if (sentence.getTokenList().size() < 10) {
			return false;
		}
		
		// if the first word is something that is bad to start a sentence with
		String firstWord = sentence.getLemma(0).toLowerCase();
		String[] notFirstWords = {"that", "it", "i", "so", "here"};
		for (String invalidFirst : notFirstWords) {
			if (invalidFirst.equals(firstWord)) {
				return false;
			}
		}
		
		if (firstWord.equals("there") && (sentence.getLemma(1).toLowerCase().equals("'s") || sentence.getLemma(1).toLowerCase().equals("is")) ){
			return false;
		}
		
		// no graf
		if (sentence.getLemmaList().contains("graf") || sentence.getSentenceStr().contains("___")) {
			return false;
		}

		if (Pattern.matches((".* : [A-Z]+[A-Z]+ [A-Z]+.*"), sentenceStr)) { 
			return false;
		}
		
		boolean containsVerb = false;
		for (String pos : sentence.getPosList()) {
			if (pos.startsWith("V")) {
				containsVerb = true;
			}
		}
		if (!containsVerb) {
			return false;
		}
		
		if (sentence.getSentenceStr().startsWith("Reporting on the") || 
				sentence.getSentenceStr().startsWith("Reporting was contributed") ||
				sentence.getSentenceStr().contains("contributed reporting") ||
				sentence.getSentenceStr().contains("calls seeking comment")) {
			return false;
		}
		
		if (sentence.getSentenceStr().contains(", ''")) {
			return false;
		}
		
		return true;
	}

	private static String getRootVerb(Sentence sentence) {
		boolean debug = false;
		Tree parseTree = sentence.getParseTree();
		String verb = "";
		if (parseTree.getChildrenAsList().size() > 0 && parseTree.getChild(0).label().value().equals("S")) {
			Tree tree = parseTree.getChild(0);
			for (int i = 0; i < tree.children().length; i++) {
				Tree child = tree.getChild(i);
				if (child.label().value().startsWith("V")) {
					for (int j = 0; j < child.getLeaves().size(); j++) {
						Tree childChild = child.getLeaves().get(j);
						String childChildLabel = childChild.label().value();
						String parentLabel = childChild.ancestor(1, parseTree).label().value();
						if (debug) System.out.println(parentLabel+"\t"+childChild);
						if (parentLabel.startsWith("V") || parentLabel.startsWith("TO")) {
							verb += childChildLabel+" ";
						} else if (!verb.equals("") && !(parentLabel.startsWith("ADVP") || parentLabel.startsWith("RB"))) {
							return verb.trim();
						}
					}
				}
			}
		}
		
		return verb;
	}

	private static Calendar earliestDate(List<Sentence> sentences) {
		Calendar earliest = sentences.get(0).getArticleDate();
		for (Sentence sentence : sentences) {
			Calendar current = sentence.getArticleDate();
			if (current.getTimeInMillis() < earliest.getTimeInMillis()) {
				earliest = current;
			}
		}
		return earliest;
	}
	
	private static Calendar latestDate(List<Sentence> sentences) {
		Calendar latest =  sentences.get(0).getArticleDate();
		for (Sentence sentence : sentences) {
			Calendar current = sentence.getArticleDate();
			if (current.getTimeInMillis() > latest.getTimeInMillis()) {
				latest = current;
			}
		}
		return latest;
	}

	private static Cluster getRootCluster(List<Cluster> clusters) {
		// setup root
		Cluster root = new Cluster();
		for (int i = 0; i < clusters.size(); i++) {
			Cluster clusteri = clusters.get(i);
			root.setChild(clusteri);
			root.addInstances(clusteri.getInstances());
		}
		return root;
	}
	
	public static Cluster getClusters(String directory) {
		// Read in information
		HashMap<String, Sentence> sentenceMap = Reader.readInSentenceCluster(directory);
		Reader.readInExtractionCluster(directory, sentenceMap);
		List<Sentence> instances = getValidSentences(sentenceMap.values());
			
		// Remove sentences marked too early
		List<Sentence> instancesToRemove = new ArrayList<Sentence>();
		Calendar earliestDate = earliestDate(instances);
		for (Sentence instance : instances) {
			if (instance.getRoundedSentenceDate().getSecond().before(earliestDate)) {
				instancesToRemove.add(instance);
			}
		}
		instances.removeAll(instancesToRemove);
		
		// Remove sentences marked too late
		instancesToRemove = new ArrayList<Sentence>();
		Calendar latestDate = latestDate(instances);
		for (Sentence instance : instances) {
			if (instance.getRoundedSentenceDate().getSecond().after(latestDate)) {
				instancesToRemove.add(instance);
			}
		}
		instances.removeAll(instancesToRemove);

		// Cluster sentences.
		SplitIdentifier splitIdentifier = new SplitIdentifier();
		List<Cluster> clusters = splitIdentifier.clusterHierarchically(instances,3);
		Cluster rootCluster = getRootCluster(clusters);
		return rootCluster;
	}

	private static String printSummary(Cluster cluster, int level) {
		return printSummary(cluster, level, System.out);
	}

	private static String printSummary(Cluster cluster, int level, PrintStream p) {
		String data = "";
		List<Sentence> summary = cluster.getSummary();
		List<Cluster> children = cluster.getChildren();
		if (summary != null) {
			for (int i = 0; i < summary.size(); i++) {
				Sentence sentence = summary.get(i);
				Calendar date = sentence.getSentenceDate().getSecond();
				for (int j = 0; j < level; j++) p.print("\t");
				p.print((date.get(Calendar.MONTH) + 1) + "-" + date.get(Calendar.DATE) + "-" + date.get(Calendar.YEAR) + "\t");
				p.println(sentence.getSentenceStr());
				if (children != null && children.size() > i && children.get(i).getSummary() != null && children.get(i).getSummary().size() > 0) {
					data += printSummary(children.get(i), level+1, p);
				}
			}
		}
		return data;
	}
	
	public static void runSumma(String inDirectory) throws IOException {
		Cluster cluster = getClusters(inDirectory);		
		HierParentSummarizer summarizer = new HierParentSummarizer();
		Cluster summaryCluster = summarizer.generateSummary(cluster);
		PrintStream p = new PrintStream(new FileOutputStream(inDirectory + "/summa.txt"));
		printSummary(summaryCluster, 0, p);
		p.flush();
		p.close();
		printSummary(summaryCluster, 0);
	}
	
	public static void main(String args[]) throws IOException {
		String baseDir = "data/d2d/summa";
		String inDirectory = baseDir + "/" + args[0];
        System.out.println(inDirectory);
		runSumma(inDirectory);

	}
}
